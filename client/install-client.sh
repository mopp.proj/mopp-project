#!/bin/bash

# Forces the script to quit on error
set -e
# Usage example of the script. If there is a problem with the scripts arguments, it prints this to the screen and exit the installation
usage()
{
  echo "Usage: $0 [-d <DNS name (no https://)>]"; exit 1;
}

# Install npm dependencies for client and server
installDependencies()
{
  echo "Installing client dependencies..."
  npm install --force
}

# Updating server variables with the given DNS
updateClient()
{
  echo "Updating client variables..."
  sed -i "s/{dns}/$http_dns/" "./src/Utils/Links.js"
}

# Build the client and copy it to httpd directory
buildClient()
{
  echo "Building the client..."
  npm run build
}

copyBuildDir()
{
  echo "Please enter your ec2 Public IPv4 DNS: (from AWS interface, the address that comes after the @ in ec2-user@) "
  read ec2_dns
  echo "Please enter your AWS Key location (i.e. path/to/key.pem)"
  read key_path
  sudo scp -i "$key_path" -r build/* ec2-user@$ec2_dns:/var/www/html
}


# Parsing the arguments given by the user
while getopts 'd:' OPTION; do
  case "$OPTION" in
    d)
      dns="$OPTARG"
      if [[ "$dns" == *"https://"* ]]
        then
          echo "ERROR: DNS arguemnt cannot be added with 'https://'"
          usage
      fi
      echo "The DNS given is: $OPTARG"
      http_dns="https:\/\/$dns"
      ;;
    ?)
      usage
      ;;
  esac
done
shift "$(($OPTIND-1))"

if [ $OPTIND -eq 1 ]
    then
        echo "No arguments were give"
        usage
fi

installDependencies
updateClient
buildClient
copyBuildDir
echo "-Finished installing the client and copy to ec2. You can proceed to the server side"
