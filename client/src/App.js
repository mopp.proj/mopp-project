import React from "react";
import {
    BrowserRouter as Router,
    Routes,
    Route,
    useParams,
} from "react-router-dom";
import { instanceOf } from "prop-types";
import { withCookies, Cookies } from "react-cookie";

import { AppBar, Container, Link, Toolbar, Typography } from "@mui/material";
import { ExperimentsTableURL, HomepageURL } from "./Utils/Links";

import Homepage from "./Homepage";
import ResearcherGUI from "./Components/Researcher/ResearcherGUI";
import ExperimentsTable from "./Components/Researcher/ExperimentsTable";
import Authentication from "./Components/Auth/Authentication";
import VirtualChinrest from "./Tasks/VirtualChinrest";
import MOPPSurvey from "./Tasks/MOPPSurvey";
import MobileErrorPage from "./ErrorPages/MobileErrorPage";
import VisualAcuity from "./Tasks/VisualAcuity";
import ForbiddenAccess from "./ErrorPages/ForbiddenAccess";
import PageNotFound from "./ErrorPages/PageNotFound";
import InternalServerError from "./ErrorPages/InternalServerError";
import moppLogo from "./Utils/Assets/logo.png";
import RunningStimulus from "./Components/Participant/RunningStimulus";
import RunningResponse from "./Components/Participant/RunningResponse";
import FinishPage from "./Components/Participant/FinishPage";

class App extends React.Component {
    static propTypes = {
        cookies: instanceOf(Cookies).isRequired
    };

    _isMounted = false;
    constructor(props) {
        super(props);        
        this.state = {
            showAppbar: true,
            showExperiments: false,
        };
    }

    setAppbar = (appbarStatus, expStatus) => {
        if (
            this.state.showAppbar !== appbarStatus ||
            this.state.showExperiments !== expStatus
        ) {
            this.setState({
                showAppbar: appbarStatus,
                showExperiments: expStatus,
            });
        }
    };

    /**
     * This function checks the width of the browser's window,
     * and returns "True" if the width is too small.
     */
    isMobile = () => {
        if (window.innerWidth <= 650) {
            return true;
        }
        return false;
    };

    render() {        
        const HomepageWrapper = (props) => {
            return (
                <Homepage 
                    {...props} 
                    setAppbar={this.setAppbar} 
                    cookies={this.props.cookies}
                />
            );
        };

        // const ImageUploadWrapper = (props) => {
        //     return <ImagePage />;
        // };

        const AuthenticationWrapper = (props) => {
            const params = useParams();
            return (
                <Authentication
                    {...{ ...props, match: { params } }}
                    setAppbar={this.setAppbar}
                    cookies={this.props.cookies}
                />
            );
        };       

        const RunningStimulusGuiWrapper = (props) => {
            const params = useParams();
            return (
                <RunningStimulus
                    {...{ ...props, match: { params } }}
                    setAppbar={this.setAppbar}
                    cookies={this.props.cookies}
                />
            );
        };

        const RunningResponseGuiWrapper = (props) => {
            const params = useParams();
            return (
                <RunningResponse
                    {...{ ...props, match: { params } }}
                    setAppbar={this.setAppbar}
                    cookies={this.props.cookies}
                />
            );
        };

        const FinishGuiWrapper = (props) => {
            const params = useParams();
            return (
                <FinishPage
                    {...{ ...props, match: { params } }}
                    setAppbar={this.setAppbar}
                    cookies={this.props.cookies}
                />
            );
        };

        const SurveyWrapper = (props) => {
            const params = useParams();
            return (
                <MOPPSurvey
                    {...{ ...props, match: { params } }}
                    setAppbar={this.setAppbar}
                    cookies={this.props.cookies}
                />
            );
        };

        const VirtualChinrestWrapper = (props) => {
            const params = useParams();
            return (
                <VirtualChinrest
                    {...{ ...props, match: { params } }}
                    setAppbar={this.setAppbar}
                    cookies={this.props.cookies}
                />
            );
        };

        const VisualAcuityWrapper = (props) => {
            const params = useParams();
            return (
                <VisualAcuity
                    {...{ ...props, match: { params } }}
                    setAppbar={this.setAppbar}
                    cookies={this.props.cookies}
                />
            );
        };

        const ResearcherGuiWrapper = (props) => {
            const params = useParams();
            return (
                <ResearcherGUI
                    {...{ ...props, match: { params } }}
                    setAppbar={this.setAppbar}
                    cookies={this.props.cookies}
                />
            );
        };

        const ExperimentsTableWrapper = (props) => {
            return <ExperimentsTable {...props} setAppbar={this.setAppbar} cookies={this.props.cookies}/>;
        };

        const ForbiddenAccessWrapper = (props) => {
            return <ForbiddenAccess {...props} setAppbar={this.setAppbar} />;
        };

        const PageNotFoundWrapper = (props) => {
            return (
                <PageNotFound 
                    {...props} 
                    setAppbar={this.setAppbar} 
                    cookies={this.props.cookies}
                />
            );
        };

        const InternalServerErrorWrapper = (props) => {
            return (
                <InternalServerError 
                    {...props} 
                    setAppbar={this.setAppbar} 
                    cookies={this.props.cookies}
                />
            );
        };
        
        if (this.isMobile()) {
            return <MobileErrorPage />;
        }

        return (
            <div>
                {this.state.showAppbar ? (
                    <AppBar position="static">
                        <Container maxWidth="xl">
                            <Toolbar variant="dense" disableGutters>                                
                                <Link
                                    href={HomepageURL}
                                    underline="none"                                    
                                >                                    
                                    <img
                                        alt=""
                                        src={moppLogo}
                                        height="40px"
                                    />
                                </Link>                                
                                <Typography
                                    variant="h6"
                                    color="inherit"
                                    noWrap
                                    component="div"
                                >
                                    <Link
                                        href={HomepageURL}
                                        underline="none"
                                        color="inherit"
                                    >
                                        
                                        MOPP
                                    </Link>
                                </Typography>
                                {this.state.showExperiments ? (
                                    <Typography
                                        style={{
                                            fontSize: "16px",
                                            paddingLeft: "5px",
                                        }}
                                        color="inherit"
                                        noWrap
                                        component="div"
                                    >
                                        <Link
                                            href={ExperimentsTableURL}
                                            underline="none"
                                            color="inherit"
                                        >
                                            {"> Experiments"}
                                        </Link>
                                    </Typography>
                                ) : (
                                    <></>
                                )}
                            </Toolbar>
                        </Container>
                    </AppBar>
                ) : (
                    <></>
                )}
                <Router>
                    <Routes>
                        <Route exact path="/" element={<HomepageWrapper />} />                        
                        {/* <Route exact path="/imagesUpload" element={<ImageUploadWrapper />} /> */}
                        <Route
                            exact
                            path="/auth"
                            element={<AuthenticationWrapper />}
                        />
                        <Route
                            exact
                            path="/:id/"
                            element={<RunningStimulusGuiWrapper />}
                        />
                        <Route
                            exact
                            path="/:id/auth"
                            element={<AuthenticationWrapper />}
                        />                        
                        <Route
                            exact
                            path="/:id/:participantID/sr"
                            element={<SurveyWrapper />}
                        />
                        <Route
                            exact
                            path="/:id/:participantID/vc"
                            element={<VirtualChinrestWrapper />}
                        />
                        <Route
                            exact
                            path="/:id/:participantID/va"
                            element={<VisualAcuityWrapper />}
                        />
                        <Route
                            exact
                            path="/:id/:participantID/stimulus/"
                            element={<RunningStimulusGuiWrapper />}
                        />
                        <Route
                            exact
                            path="/:id/:participantID/response/"
                            element={<RunningResponseGuiWrapper />}
                        />
                        <Route
                            exact
                            path="/:id/:participantID/finish/"
                            element={<FinishGuiWrapper />}
                        />
                        <Route
                            exact
                            path="/experiments/"
                            element={<ExperimentsTableWrapper />}
                        />
                        <Route
                            exact
                            path="/experiments/:id"
                            element={<ResearcherGuiWrapper />}
                        />
                        <Route
                            exact
                            path="/ForbiddenAccess"
                            element={<ForbiddenAccessWrapper />}
                        />
                        <Route
                            exact
                            path="/PageNotFound"
                            element={<PageNotFoundWrapper />}
                        />
                        <Route
                            exact
                            path="/InternalServerError"
                            element={<InternalServerErrorWrapper />}
                        />
                    </Routes>
                </Router>
            </div>
        );
    }
}

export default withCookies(App);
