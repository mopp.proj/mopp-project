import React from "react";
import axios from "axios";

import { Grid, InputAdornment, Typography } from "@mui/material";
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";
// adding some lines
import GoogleAuth from "./GoogleAuth";
import FacebookAuth from "./FacebookAuth";
import MicrosoftAuth from "./MicrosoftAuth";
import ReCaptchaAuth from "./ReCaptchaAuth";
import CustomFab from "../General/CustomFab";
import CustomSnackbar from "../General/CustomSnackbar";
import CustomTextField from "../General/CustomTextField";

import {
    AuthURL,
    STAGES,
    redirectCorrectPage,
    ParticipantURL,
    redirectToHomePage,
    STAGE_ITEM,
    PASS_ITEM,
    redirectToCorrectErrorPage,
} from "../../Utils/Links";

class Authentication extends React.Component {
    // CTOR of Authentication
    constructor(props) {
        super(props);
        this.state = {
            participantUrlID: this.props.match.params.id,
            experimentType: undefined,
            passcode: "",
            participantID: undefined,
            alreadyExists: false,
            reCaptchaPassed: false,
        };
    }

    /**
     * This function does several things after the component is mounted:
     * 1. Prepare the browser for a new experiment.
     * 2. Redirects to a clean url (without wilcards).
     * 3. Gets the type of the experiment.
     */
    componentDidMount() {
        if (this.state.participantUrlID !== undefined) {
            // remove participantUrlId from url, in order to be valid in Microsoft Authentication Platform
            // (which does not work with wildcard url)
            window.history.pushState(null, "", AuthURL);
            // get experiment type
            axios
                .get(`${ParticipantURL}/${this.state.participantUrlID}/get/experimentType`)
                .then((res) => {
                    this.setState(
                        {
                            experimentType: res.data[0].experimentType,
                            reCaptchaPassed: res.data[0].experimentType === "Supervised" ? true : false,
                        },
                        () => {
                            // show appbar
                            this.props.setAppbar(
                                true,
                                this.props.cookies.get(PASS_ITEM)                                
                            );
                        }
                    );
                })
                .catch((err) => {                    
                    redirectToCorrectErrorPage(err.response.status);
                });
        } else {
            redirectToHomePage();
        }
    }
    /**
     * This function sets the passcode value after pressing "Enter", and validates it.
     * @param {String} value - the value of the passcode.
     */
    setThenValidatePasscode = (value) => {
        this.setState({ passcode: value }, () => {
            this.validatePasscode();
        });
    };

    /**
     * This function validates a given passcode.
     */
    validatePasscode = () => {
        this.validateAuthentication("Passcode", this.state.passcode);
    };

    /**
     * This function validates the authentication of the participant
     * @param {String} connectionType - type of connection
     * @param {String} authID - unique ID of authentication
     */
    validateAuthentication = (connectionType, authID) => {
        if (!this.state.participantID && this.state.participantUrlID !== STAGES.AUTH) {
            // get the client IP
            axios.get("https://geolocation-db.com/json/").then((resIP) => {
                const data = {
                    experimentType: this.state.experimentType,
                    connectionType: connectionType,
                    authID: authID,
                    clientIP: resIP.data.IPv4,
                };
                // check if authentication is ok (not already exists)
                axios
                    .put(`${ParticipantURL}/${this.state.participantUrlID}`, data)
                    .then((res) => {
                        this.setState(
                            {
                                participantID: res.data.participantID,
                            },
                            () => {
                                // move to survey page
                                this.props.cookies.set(STAGE_ITEM, STAGES.SURVEY, { path: "/" });                                
                                redirectCorrectPage(
                                    STAGES.AUTH,
                                    this.state.participantUrlID,
                                    this.state.participantID,
                                    this.props.cookies,
                                );
                            }
                        );
                    })
                    .catch((err) => {
                        this.setState({
                            alreadyExists: true,
                        });
                        // stay in authentication page
                        //TODO: Ask yuval about this line
                        // this.props.cookies.set(STAGE_ITEM, STAGES.AUTH, { path: "/" });
                                   
                    });
            });
        }
    };

    /**
     * This function hides the alert when the "X" button is pressed
     */
    closeSnackBar = () => {
        this.setState({
            alreadyExists: false,
        });
    };

    /**
     * This function sets the passcode after the user stop typing it.
     * @param {String} value - the value of passcode.
     */
    setPasscode = (value) => {
        this.setState({
            passcode: value,
        });
    };

    /**
     * This function sets the reCaptch was passed successfully.
     */
    setReCaptcha = () => {
        this.setState({ reCaptchaPassed: true });
    };

    /**
     * This function sets the reCaptch was not passed successfully.
     */
    unSetReCaptcha = () => {
        this.setState({ reCaptchaPassed: false });
    };

    render() {        
        return (
            <div>
                <Typography
                    variant="h2"
                    gutterBottom
                    style={{
                        margin: "auto",
                        textAlign: "center",
                        paddingTop: "5%",
                    }}
                >
                    Registration
                </Typography>

                {this.state.experimentType === "Supervised" ? (
                    <Typography
                        variant="h6"
                        gutterBottom
                        style={{
                            margin: "auto",
                            textAlign: "center",
                            paddingTop: "2%",
                        }}
                    >
                        Please register using one of the following platforms <br/>
                        NOTE - If you are using a pop-ups blocker, please turn it off prior to entering the experiment
                        
                    </Typography>
                ) : (
                    <ReCaptchaAuth onChange={this.setReCaptcha} onExpired={this.unSetReCaptcha} />
                )}

                <Grid
                    container
                    direction="row"
                    justifyContent="center"
                    alignItems="center"
                    spacing={2}
                    style={{
                        paddingTop: "1%",
                    }}
                >
                    <Grid item>
                        <GoogleAuth 
                            validateAuthentication={this.validateAuthentication}
                            disabled={!this.state.reCaptchaPassed}
                        />
                    </Grid>
                    <Grid item>
                        <MicrosoftAuth
                            validateAuthentication={this.validateAuthentication}
                            disabled={!this.state.reCaptchaPassed}
                        />
                    </Grid>
                    <Grid item>
                        <FacebookAuth
                            validateAuthentication={this.validateAuthentication}
                            disabled={!this.state.reCaptchaPassed}
                        />
                    </Grid>
                </Grid>
                {this.state.experimentType === "Supervised" ? (
                    <div style={{'paddingTop':"3%"}}>
                        <Typography
                            variant="h6"
                            gutterBottom
                            style={{
                                margin: "auto",
                                textAlign: "center",
                            }}
                        >
                            If you received a private key from the researcher, please enter it below.
                        </Typography>

                        <Grid
                            container
                            direction="row"
                            justifyContent="center"
                            alignItems="center"
                            style={{
                                paddingTop: "1%",
                            }}
                        >
                            <CustomTextField
                                label="Passcode"
                                value={this.state.passcode}
                                onBlur={this.setPasscode}
                                onKeyDown={this.setThenValidatePasscode}
                                InputProps={{
                                    endAdornment: (
                                        <InputAdornment position="end">
                                            <CustomFab
                                                tooltip="Enter Passcode"
                                                size="small"
                                                color="primary"
                                                label="passcode"
                                                onClick={this.validatePasscode}
                                                icon={<ArrowForwardIcon />}
                                            />
                                        </InputAdornment>
                                    ),
                                }}
                            />
                        </Grid>
                    </div>
                ) : (
                    <></>
                )}
                <Typography
                    variant="h6"
                    gutterBottom
                    style={{
                        margin: "auto",
                        textAlign: "center",
                        fontSize: "16px",
                        paddingTop: "3%",
                    }}
                >
                    Don't worry, we don't save any personal data about you.
                </Typography>
                <Typography
                    variant="h6"
                    gutterBottom
                    style={{
                        margin: "auto",
                        textAlign: "center",
                        fontSize: "16px",
                    }}
                >
                    We only want to make sure you are a new participant in this experiment
                </Typography>
                <CustomSnackbar
                    open={this.state.alreadyExists}
                    severity={"error"}
                    content={`You have already participated in this experiment${
                        this.state.experimentType === "Supervised" ? " or the passcode is incorrect" : ""
                    }.`}
                    onClose={this.closeSnackBar}
                />
            </div>
        );
    }
}

export default Authentication;
