import React from "react";
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";

import "./Assets/style.css";
import FacebookLogo from "./Assets/FacebookLogo.png";

const AuthCreds = require("./Assets/AuthenticationCreds.json");

class FacebookAuth extends React.Component {
    /**
     * This function is called after the user tried to login using Facebook.
     * @param {*} res - response object.
     */
    responseFacebook = (res) => {
        // if the login was successful
        if (res.userID) {
            this.props.validateAuthentication("Facebook", res.userID);
            // logout the user after validation
            window.FB.logout(function (res) {
            });
        }
    };

    render() {
        return (
            <FacebookLogin
                appId={AuthCreds.facebook.clientID}
                autoLoad={false}
                callback={this.responseFacebook}
                cookie={false}
                render={(renderProps) => (
                    <button
                        className={this.props.disabled ? "Facebook_Disabled" : "Facebook"}
                        onClick={renderProps.onClick}
                    >
                        <img src={FacebookLogo} className="Logo" alt={"Facebook"} />
                        Sign in with Facebook
                    </button>
                )}
                isDisabled={this.props.disabled}
            />
        );
    }
}

export default FacebookAuth;
