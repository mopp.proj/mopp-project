import React from 'react';
import { GoogleOAuthProvider,GoogleLogin  } from '@react-oauth/google';
import Logo from "./Assets/GoogleLogo.png";
import "./Assets/style.css";
const AuthCreds = require("./Assets/AuthenticationCreds.json");

class GoogleAuth extends React.Component {
    /**
     * This function is called when the login was successful.
     * @param {*} res - response object.
     */
    onSuccess = (res) => {
        this.props.validateAuthentication("Google", res.credential);
        this.signOut()
    }
    
    /**
     * This function is called when the login was not successful.
     * @param {*} res 
     */
    onFailure = (res) => {        
    }
    
    /**
     * This function signs out the user from it's account.
     */
    signOut = () => {
        this.setState({                
            authID: undefined,
            connectionType: '',
        })
    }
    

    render() {
        return (
            <div>
                <GoogleOAuthProvider clientId={AuthCreds.google.clientID}>
                    {this.props.disabled ?    
                    <button
                        disabled={true} 
                        className={"Google_Disabled"}
                    >
                        <img src={Logo} className="Logo" alt={"Google"} />
                        Sign in with google
                    </button>
                    :
                    <GoogleLogin
                        onSuccess={this.onSuccess}
                        onFailure={this.onFailure}
                        context={"signin"}
                    />    
                    }
                </GoogleOAuthProvider>
            </div>
          );
    }
};

export default GoogleAuth;