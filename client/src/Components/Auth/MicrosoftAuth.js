import React from "react";
import { MsalContext, MsalProvider } from "@azure/msal-react";
import { PublicClientApplication } from "@azure/msal-browser";

import "./Assets/style.css";
import Logo from "./Assets/MicrosoftLogo.png";

const AuthCreds = require("./Assets/AuthenticationCreds.json");
const msalInstance = new PublicClientApplication({
    auth: {
        clientId: AuthCreds.microsoft.clientID,
    },
});

class MicrosoftAuth extends React.Component {
    render() {
        return (
            <MsalProvider instance={msalInstance}>
                <MicrosoftButton
                    validateAuthentication={this.props.validateAuthentication}
                    disabled={this.props.disabled}
                />
            </MsalProvider>
        );
    }
}

export default MicrosoftAuth;

class MicrosoftButton extends React.Component {
    static contextType = MsalContext;
    /**
     * This function is called when the user clicks the Microsoft login button.
     */
    onClick = () => {
        if (this.context.inProgress !== "login" && this.context.accounts.length === 0) {
            this.login();
        } else {
            this.context.instance
                .logoutPopup()
                .then((data) => {
                    this.login();
                })
                .catch((err) => {
                });
        }
    };

    /**
     * This function is called when the user tries to login.
     */
    login = () => {
        this.context.instance
            .loginPopup()
            .then((data) => {
                this.props.validateAuthentication("Microsoft", data.uniqueId);
            })
            .catch((err) => {
            });
    };

    render() {
        return (
            <button
                className={this.props.disabled ? "Microsoft_Disabled" : "Microsoft"}
                onClick={this.onClick}
                disabled={this.props.disabled}
            >
                <img src={Logo} className="Logo" alt={"Microsoft"} />
                Sign in with Microsoft
            </button>
        );
    }
}
