import React from "react";
import ReCAPTCHA from "react-google-recaptcha";
import { Grid, Typography } from "@mui/material";

const AuthCreds = require("./Assets/AuthenticationCreds.json");

class ReCaptchaAuth extends React.Component {
    render() {
        return (
            <Grid container justifyContent={"center"} alignContent={"center"}>
                <Grid item xs={12}>
                    <Typography
                        variant="h6"
                        gutterBottom
                        style={{
                            margin: "auto",
                            textAlign: "center",
                            paddingTop: "5%",
                            paddingBottom: "2%",
                        }}
                    >
                        Please click the checkbox to verify you are not a robot <br/>
                        And afterwards register using one of the following platforms
                    </Typography>
                </Grid>
                <Grid item>
                    <ReCAPTCHA 
                        sitekey={AuthCreds.recaptch.siteKey} 
                        onChange={this.props.onChange} 
                        onExpired={this.props.onExpired} 
                    />
                </Grid>
            </Grid>
        );
    }
}
export default ReCaptchaAuth;
