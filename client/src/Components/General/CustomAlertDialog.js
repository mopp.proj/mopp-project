import React from "react";
import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from "@mui/material";

class CustomAlertDialog extends React.Component {
    render() {        
        return (                            
            <Dialog
                open={this.props.isOpen}
                onClose={this.props.onDisagree}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">
                    {this.props.title}
                </DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        {this.props.content}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.props.onDisagree}>Disagree</Button>
                    <Button onClick={this.props.onAgree} autoFocus>
                        Agree
                    </Button>
                </DialogActions>
            </Dialog>            
        );
    }
}

export default CustomAlertDialog;