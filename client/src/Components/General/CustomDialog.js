import { Dialog, DialogTitle, List, ListItem, ListItemText } from "@mui/material";
import React from "react";

class CustomDialog extends React.Component {
    
    closeDialog = () => {
        this.props.onClose();
    }
    
    selectItem = (event) => {        
        this.props.onClose(event.target.innerText);
    }

    render() {
        return (
            <Dialog
                open={this.props.open}
                onClose={this.closeDialog}
            >
                <DialogTitle>
                    Which task would you like to add?
                </DialogTitle>
                <List sk={{pt: 0}}>
                    {this.props.items.map((item) => (
                        <ListItem button onClick={this.selectItem} key={item}>
                            <ListItemText primary={item} />
                        </ListItem>
                    ))}
                </List>
            </Dialog>
        );
    }
}

export default CustomDialog;