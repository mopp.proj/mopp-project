import React from "react";
import InputLabel from "@mui/material/InputLabel";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import FormHelperText from "@mui/material/FormHelperText";
import FormControl from "@mui/material/FormControl";
import { Grid } from "@mui/material";

class CustomDropdown extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: this.props.value || "",
            error: "",
            isError: false,
        };
    }

    componentDidMount = () => {
        this.props.onChange(this.props.value, this.validateField);
    };

    componentDidUpdate = (prevProps) => {
        if (prevProps.value !== this.props.value) {
            this.setState(
                {
                    value: this.props.value,
                    isError: false,
                    error: "",
                } // adding onChange as a callback ruins the validation logic
            );
        }
    };

    validateField = () => {
        if (this.state.value === "") {
            this.setState({
                isError: true,
                error: "Please select an option",
            });
            return true;
        } else {
            this.setState({
                isError: false,
                error: "",
            });
            return false;
        }
    };

    onChange = (event) => {
        if (this.state.value !== event.target.value) {
            if(this.props.onChange){
                this.props.onChange(event.target.value, this.validateField);
            }
        }
    };

    render() {
        return (
            <Grid item>
                <FormControl error={this.state.isError}>
                    <InputLabel id={this.props.label + "-label"}>{this.props.label}</InputLabel>
                    <Select
                        sx={this.props.sx || {width: 244}}
                        labelId={this.props.label + "-label"}
                        id={this.props.label + "-Select"}
                        label={this.props.label}
                        value={this.state.value}                        
                        onChange={this.onChange}
                        disabled={this.props.disabled}
                    >
                        {this.props.options.map((option, id) => {
                            return (
                                <MenuItem key={id} value={option}>
                                    {option}
                                </MenuItem>
                            );
                        })}
                    </Select>
                    <FormHelperText>{this.state.error}</FormHelperText>
                </FormControl>
            </Grid>
        );
    }
}
export default CustomDropdown;
