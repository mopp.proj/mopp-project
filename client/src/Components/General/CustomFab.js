import React from "react";
import { Fab, Tooltip } from "@mui/material";

class CustomFab extends React.Component {
    render() {
        return(
            <Tooltip title={this.props.tooltip} placement="top">
                <Fab 
                    size={this.props.size} 
                    color={this.props.color} 
                    sx={this.props.sx}
                    aria-label={this.props.label} 
                    onClick={this.props.onClick}
                >
                    {this.props.icon}
                </Fab>
            </Tooltip>
        );
    }
}

export default CustomFab;