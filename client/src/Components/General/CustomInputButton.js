import React from "react";
import CustomFab from "./CustomFab";

class CustomButton extends React.Component {
    render() {
        return (
            <label htmlFor="upload-json">
                <input
                    style={{ display: "none" }}
                    ref={(ref) => (this.upload = ref)}
                    name={this.props.name}
                    type={this.props.type}
                    onChange={this.props.onChange}
                    disabled={this.props.disable}
                />
                <CustomFab
                    tooltip={this.props.tooltip}
                    placement="top"
                    size={this.props.size}
                    color={this.props.color}
                    label={this.props.label}
                    icon={this.props.icon}
                    onClick={() => {
                        this.upload.click();
                    }}
                />
            </label>
        );
    }
}

export default CustomButton;
