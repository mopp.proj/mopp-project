import React from "react";
import { Alert, Snackbar } from "@mui/material";

class CustomSnackbar extends React.Component {
    render() {
        return (
            <Snackbar 
                open={this.props.open}
                autoHideDuration={5000}
                onClose={this.props.onClose}
            >
                <Alert                        
                    variant="filled"    
                    severity={this.props.severity} 
                    onClose={this.props.onClose}
                >
                    {this.props.content}
                </Alert>                    
            </Snackbar>
        );
    }
}

export default CustomSnackbar;