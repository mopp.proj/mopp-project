import React from "react";
import { TextField, Grid } from "@mui/material";

const isPositive = (number, type) => {
    if (number <= 0 || isNaN(number)) {
        return { error: `Must enter a positive ${type}` };
    }
    return { error: null };
};

const positiveIntegerValidation = (value) => {
    const number = Number(value);
    // check if the number is int and not float
    if (!Number.isInteger(number)) {
        return { error: "Must enter a positive Integer" };
    }
    return isPositive(number, "Integer");
};

const positiveFloatValidation = (value) => {
    const number = Number(value);
    return isPositive(number, "Decimal");
};

const positiveIntegerWithZeroValidation = (value) => {
    const number = Number(value);
    // check if the number is int and not float
    if (!Number.isInteger(number)) {
        return { error: "Must enter a positive Integer" };
    }
    if (number === 0) {
        return { error: null };
    }
    return isPositive(number, "Integer");
};

const nonEmptyStringValidation = (value) => {
    const str = String(value);
    // check if the string is empty
    if (str === "") {
        return { error: "Must enter some text" };
    }
    return { error: null };
};

const isInRange = (number, min, max, type) => {
    if ((min > number || number > max) ||
        (type === "integer" && !Number.isInteger(number))
        ) {
        return { error: `Must enter a ${type} number in range: [${min}, ${max}]` };
    }
    return { error: null };
};

const floatRange0to1Validation = (value) => {
    const number = Number(value);
    return isInRange(number, 0, 1, "float");
}

const validationFunctions = {
    positiveInteger: positiveIntegerValidation,
    positiveFloat: positiveFloatValidation,
    positiveIntegerWithZero: positiveIntegerWithZeroValidation,
    nonEmptyString: nonEmptyStringValidation,
    floatRange0to1: floatRange0to1Validation,    
};

const ENTER_KEY = 13;

class CustomTextField extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isError: false,
            error: "",
            value: this.props.value || "",
            id: this.props.id,
        };
    }

    componentDidMount() {
        if (this.props.validation) {
            this.props.onBlur(this.props.value, this.validateField);
        }
    }

    componentDidUpdate(prevProps) {
        if (
            (this.props.value !== prevProps.value &&
                this.state.value !== this.props.value) ||
            this.state.id !== this.props.id
        ) {
            this.setState(
                {
                    isError: false,
                    error: "",
                    value: this.props.value || "",
                    id: this.props.id,
                },
                () => {
                    if (this.props.setError) {
                        this.props.setError(false, " ");
                    }
                    this.props.onBlur(this.props.value, this.validateField);
                }
            );
        }
    }

    validateField = () => {
        if (this.props.validation) {
            const res = validationFunctions[this.props.validation](
                this.state.value
            );
            if (res.error) {
                this.setState({
                    isError: true,
                    error: res.error,
                });

                if (this.props.setError) {
                    this.props.setError(true);
                }

                return true;
            } else {
                this.setState({
                    isError: false,
                    error: "",
                });

                if (this.props.setError) {
                    this.props.setError(false);
                }

                return false;
            }
        }

        if (this.props.setError) {
            this.props.setError(false);
        }

        return false;
    };

    onBlur = (event) => {
        /* we used != insted of !== because value of TextField must be string,
           and we don't want to update the state of the parent if the value itself wasn't changed */
        if (this.props.value != event.target.value) {
            this.validateField();
            this.props.onBlur(event.target.value, this.validateField);
        }
    };

    onChange = (event) => {
        this.setState({
            value: event.target.value,
        },() => {
            // immediate response overrides updating TextResponse on blur, and instead updates TextResponse immediately
            if(this.props.isImmediateResponse) {
                this.validateField();
                this.props.onBlur(event.target.value, this.validateField);
            }
        });
    };

    keyPress = (event) => {
        if (event.keyCode === ENTER_KEY) {
            if (this.props.value !== event.target.value) {
                this.validateField();
                if (this.props.onKeyDown !== undefined) {
                    this.props.onKeyDown(
                        event.target.value,
                        this.validateField
                    );
                }
            }
        }
    };

    render() {
        return (
            <Grid item>
                <TextField
                    id={this.props.id}
                    label={this.props.label}
                    type={this.props.type}
                    InputProps={this.props.InputProps}
                    InputLabelProps={this.props.InputLabelProps}
                    disabled={this.props.disabled}
                    sx={
                        this.props.fullWidth
                            ? null
                            : this.props.sx || { width: 244 }
                    }
                    multiline={this.props.multiline}
                    fullWidth={this.props.fullWidth}
                    cols={this.props.cols}
                    rows={this.props.rows}
                    autoComplete="off"
                    value={this.state.value}
                    error={this.state.isError || this.props.isError}
                    helperText={this.state.error || this.props.error}
                    onBlur={this.onBlur}
                    onChange={this.onChange}
                    onKeyDown={this.keyPress}
                    autoFocus={this.props.autoFocus}
                />
            </Grid>
        );
    }
}
export default CustomTextField;
