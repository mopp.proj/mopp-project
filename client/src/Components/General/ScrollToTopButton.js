import React from "react";

import CustomFab from "./CustomFab";
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward';
import { Grid } from "@mui/material";

class ScrollToTopButton extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            visible: false
        }
    }

    componentDidMount() {
        document.addEventListener("scroll", this.onScroll)
    }

    onScroll = () => {
        this.setState({
            visible: document.documentElement.scrollTop > 20,
        });
    }

    scrollToTop = () => {
        window.scrollTo({
            top: 0,
            behavior: "smooth",
        });
    }

    componentWillUnmount() {
        document.removeEventListener("scroll", this.onScroll);
    }
    
    render() {
        return (
            <Grid
                container                  
                justifyContent="flex-end"                
                sx={{ position: "sticky", bottom: 10}}
                style={{
                    paddingRight: "10px"
                }}
            >
                {this.state.visible
                    ?
                    <CustomFab
                        tooltip="Scroll to Top"
                        size="medium"
                        color="secondary"
                        label="scroll"
                        onClick={this.scrollToTop}
                        icon={<ArrowUpwardIcon />}
                    />
                    :
                    <></>
                }
            </Grid>
        );
    }
}

export default ScrollToTopButton;