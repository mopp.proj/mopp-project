import React from "react";

import { Typography } from "@mui/material";
import { PASS_ITEM, ParticipantURL } from "../../Utils/Links";
import axios from "axios";

class FinishPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            paymentCode:undefined,
        }
    }
    async componentDidMount() {        
        this.props.setAppbar(true, this.props.cookies.get(PASS_ITEM));
        await axios.get(`${ParticipantURL}/${this.props.match.params.id}`).then((result) => {
            this.setState({
                paymentCode:result.data.paymentCode,
            })
        })
    }    
    
    render() {   
        return (
            <div
                style={{
                    margin: "auto",
                    textAlign: "center",
                }}
            >
                <Typography
                    variant="h2"
                    gutterBottom
                    style={{                        
                        paddingTop: "7%",
                    }}
                >
                    That's it!                    
                </Typography>
                <Typography
                    variant="h4"
                    gutterBottom
                    style={{                        
                        paddingTop: "5%",
                    }}
                >
                    Your response has been recorded, Thank you for participating in the experiment!
                </Typography>
                {(this.state.paymentCode && this.state.paymentCode!=="")?
                <Typography
                    variant="h5"
                    gutterBottom
                    style={{                        
                        paddingTop: "5%",
                    }}
                >
                    Copy the following code and enter in manually in the Prolfic or MTurk app when you return: {this.state.paymentCode}
                </Typography>
                : <></>}
                
            </div>
        );
    }
}

export default FinishPage;