import React from "react";
import { Grid, Typography } from "@mui/material";
class KeyboardTappingResponse extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            answer: "",
        };
    }
    componentDidMount() {
        this.props.onChange(this.state.answer); // Allow null response
        document.addEventListener("keyup", this.onKeyUp);
    }

    /**
     * The function listens to key-up events and add's the character to a string
     * @param {*} event A key-up event
     */
    onKeyUp = (event) => {
        if (event.key === "Enter") {
            return;
        }
        this.setState(
            (prevState) => {
                return {
                    answer: prevState.answer + String.fromCharCode(event.which).toLowerCase(),
                };
            },
            () => {
                this.props.onChange(this.state.answer);
            }
        );
    };

    render() {
        var hand = this.props.hand.toLowerCase();
        var beforeText = this.props.isPractice
            ? `Type sk exactly 3 times with`
            : "Press keys 's' and 'k' repeatedly with";
        if (hand !== "both") {
            beforeText += " your";
        }
        var afterText = hand !== "both" ? "hand" : "hands";
        return (
            <Grid item xs={12}>
                <Typography
                    variant="h4"
                    gutterBottom
                    style={{
                        margin: "auto",
                        textAlign: "center",
                        whiteSpace: "pre-wrap",
                    }}
                >
                    {beforeText} <b>{hand}</b> {afterText}
                </Typography>
            </Grid>
        );
    }
}
export default KeyboardTappingResponse;
