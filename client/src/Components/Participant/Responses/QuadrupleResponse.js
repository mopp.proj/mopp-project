﻿import React, { Component } from "react";
import {
    FormControl,
    RadioGroup,
    FormControlLabel,
    Radio,
} from "@mui/material";

class QuadrupleResponse extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: true,
            answer: undefined,
        };
    }

    onChange = (event) => {
        this.setState(
            {
                answer: event.target.value,
                error: false,
            },
            () => {
                this.props.onChange(this.state.answer);
                this.props.setError(this.state.error);
            }
        );
    };

    render() {
        return (
            <form>
                <FormControl
                    error={this.state.error}
                    variant="standard"
                >
                    <RadioGroup onChange={this.onChange}>
                        <FormControlLabel
                            value={this.props.opt1Value}
                            control={<Radio />}
                            label={this.props.opt1Label}
                            disabled={this.props.disabled}
                        />
                        <FormControlLabel
                            value={this.props.opt2Value}
                            control={<Radio />}
                            label={this.props.opt2Label}
                            disabled={this.props.disabled}
                        />
                        <FormControlLabel
                            value={this.props.opt3Value}
                            control={<Radio />}
                            label={this.props.opt3Label}
                            disabled={this.props.disabled}
                        />
                        <FormControlLabel
                            value={this.props.opt4Value}
                            control={<Radio />}
                            label={this.props.opt4Label}
                            disabled={this.props.disabled}
                        />
                    </RadioGroup>
                </FormControl>
            </form>
        );
    }
}
export default QuadrupleResponse;