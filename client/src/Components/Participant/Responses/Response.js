import React, { Component } from "react";

import {
    Button,
    Grid,
    Typography,
    FormControl,
    FormHelperText,
    LinearProgress,
    Box
} from "@mui/material";

import { CountdownCircleTimer } from "react-countdown-circle-timer";

import TextResponse from "./TextResponse";
import BinaryResponse from "./BinaryResponse";
import KeyboardTappingResponse from "./KeyboardTappingResponse";
import QuadrupleResponse from "./QuadrupleResponse";

// import ScaleResponse from "./ScaleResponse";

class Response extends Component {
    constructor(props) {
        super(props);
        this.state = {
            answer: undefined,
            isEmpty:true,
            error: false,
            errorMessage: " ",
            startTime: performance.now(),
        };
    }

    onChange = (val) => {
        this.setState({
            answer: val,
        });
    };

    setError = (err) => {
        this.setState({
            error: err,
            errorMessage: err ? this.state.errorMessage : " ",
        });
    };

    sendAnswer = () => {
        // On the first run, continue=true and then it sends an empty response (which in general we want but only after timer ends)
        // so isEmpty is a bypass for this case
        if(!this.state.isEmpty) {
            if (!this.state.error) {            
                const responseTime = performance.now() - this.state.startTime;
                this.props.clearTimeout(this.state.answer, responseTime);
            } else {            
                this.setState({
                    error: true,
                    errorMessage: "Please enter an answer",
                });
            }
        } else {
            this.setState({isEmpty:false})
        }
    };

    onKeyDown = (val) => {
        this.setState(
            {
                answer: val,
            },
            () => {
                this.sendAnswer();
            }
        );
    };

    setResponseType = () => {
        switch (this.props.type) {
            case "textResponse":
                return (
                    <TextResponse
                        validation={this.props.validation}
                        setError={this.setError}
                        onChange={this.onChange}
                        onKeyDown={this.onKeyDown}
                    />
                );
            case "binaryResponse":
                return (
                    <BinaryResponse
                        setError={this.setError}
                        onChange={this.onChange}
                        opt1Label={this.props.opt1Label}
                        opt2Label={this.props.opt2Label}
                        opt1Value={this.props.opt1Value}
                        opt2Value={this.props.opt2Value}
                        disabled={this.props.disabled}
                    />
                );

            // ~(Michael Levin)~ (29.01.2024) // new type
            case "quadrupleResponse":
                return (
                    <QuadrupleResponse
                        setError={this.setError}
                        onChange={this.onChange}
                        opt1Label={this.props.opt1Label}
                        opt2Label={this.props.opt2Label}
                        opt1Value={this.props.opt1Value}
                        opt2Value={this.props.opt2Value}
                        opt3Label={this.props.opt3Label}
                        opt4Label={this.props.opt4Label}
                        opt3Value={this.props.opt3Value}
                        opt4Value={this.props.opt4Value}
                        disabled={this.props.disabled}
                    />
                );

            case "keyboardTappingResponse":
                return (
                    <KeyboardTappingResponse
                        hand={this.props.hand}
                        onChange={this.onChange}
                        isPractice={this.props.isPractice}
                    />
                );
            // case "scaleResponse":
            //     return (
            //         <ScaleResponse
            //             setError={this.setError}
            //             onBlur={this.onChange}
            //             sendAnswer={this.sendAnswer}
            //             {...this.props}
            //         />
            //     );
            default:
                break;
        }
    };

    renderTime = ({ remainingTime }) => {
        return (
            <Typography variant="h4" gutterBottom>
                {remainingTime}
            </Typography>
        );
    };
    calcProgress = ()=>{
        return Math.round(((this.props.trialNum)/this.props.maxTrials)*100)
    }

    render() {   
        // Sending the answers automatically - The continue is set to true when RunningResponse finishes its timer
        // and then the response sends its answer
        if(this.props.continue) {
            this.sendAnswer();
        }   
        return (
            <div>
                <Box sx={{alignItems:'center'}}>
                    <Box sx={{ width: '400px' }}>
                        <LinearProgress sx={{'height':'5px'}} variant="determinate" value={this.calcProgress()}/>
                    </Box>
                    <Box sx={{ 'minWidth': 35 }}>
                        <Typography variant="body2" color="text.secondary">
                            Task: {this.props.taskNum} / {this.props.maxTasks}, Trial: {this.props.trialNum} / {this.props.maxTrials}
                        </Typography>
                    </Box>
                </Box>
                <Grid
                    container
                    direction="row"
                    justifyContent="center"
                    alignItems="center"
                    style={{
                        margin: "auto",
                        textAlign: "center",
                        position: "fixed",
                        left: "50%",
                        top: "50%",
                        WebkitTransform: "translate(-50%, -50%)",
                        transform: "translate(-50%, -50%)",
                    }}
                >
                    <Grid item xs={12}>
                        <Typography
                            variant="h4"
                            gutterBottom
                            style={{
                                paddingTop: "2%",
                                paddingBottom: "2%",
                            }}
                        >
                            Fill in your answer:
                        </Typography>
                    </Grid>
                    <CountdownCircleTimer
                        isPlaying={true}
                        // the -1 is for keyboard tapping to send the answer oncomplete
                        duration={this.props.timeRemaining - 1}
                        colors={["#004777", "#F7B801", "#A30000", "#A30000"]}
                        colorsTime={[10, 6, 3, 0]}
                        size={160}
                        // send keyboardtapping answer on complete
                        onComplete={
                            this.props.type === "keyboardTappingResponse"
                                ? this.sendAnswer
                                : null
                        }
                    >
                        {this.renderTime}
                    </CountdownCircleTimer>
                    <Grid
                        item
                        xs={12}
                        style={{
                            paddingTop: "2%",
                        }}
                    >
                        {this.setResponseType()}
                    </Grid>
                    {this.props.removeSubmit && this.props.isPractice === false ? (
                        <></>
                    ) : (
                        <Grid item xs={12}>
                            <FormControl
                                error={this.state.error} 
                                variant="standard"
                            >
                                <FormHelperText>
                                    {this.state.errorMessage}
                                </FormHelperText>
                                <Button
                                    color="primary"
                                    variant="contained"
                                    onClick={this.sendAnswer}
                                    disabled={this.props.disabled}
                                >
                                    Submit
                                </Button>
                            </FormControl>
                        </Grid>
                    )}
                </Grid>
            </div>
        );
    }
}
export default Response;
