import React, { Component } from "react";
import {
    FormControl,
    RadioGroup,
    FormControlLabel,
    Radio,
    FormLabel,
} from "@mui/material";

class ScaleResponse extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: false,
            answer: undefined,
        };
    }

    onChange = (event) => {
        this.setState(
            {
                answer: event.target.value,
                error: false,
            },
            () => {
                this.props.onChange(this.state.answer);
                this.props.setError(this.state.error);
            }
        );
    };

    render() {
        return (
            <form>
                <FormControl
                    component="fieldset"
                    sx={{ m: 3 }}
                    error={this.state.error}
                >
                    <FormLabel component="legend">
                        {this.props.description}
                    </FormLabel>
                    <RadioGroup row onChange={this.onChange}>
                        {this.props.options.map((value, index) => (
                            <FormControlLabel
                                key={index}
                                value={value}
                                control={<Radio />}
                                label={value}
                                labelPlacement="top" // can be selected from ["start", "end", "top", "bottom"]
                            />
                        ))}
                    </RadioGroup>
                </FormControl>
            </form>
        );
    }
}
export default ScaleResponse;
