import React, { Component } from "react";
import { Grid } from "@mui/material";
import CustomTextField from "../../General/CustomTextField";

class TextResponse extends Component {
    render() {
        return (
            <Grid container alignItems="center" spacing={2} rowSpacing={2}>
                <Grid item xs={12}>
                    <CustomTextField
                        validation={this.props.validation}
                        setError={this.props.setError}
                        onBlur={this.props.onChange}
                        isImmediateResponse={true}
                        onKeyDown={this.props.onKeyDown}
                        autoFocus={true}
                    ></CustomTextField>
                </Grid>
            </Grid>
        );
    }
}
export default TextResponse;
