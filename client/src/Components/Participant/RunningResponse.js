﻿import React from "react";
import axios from "axios";

import { Grid, Snackbar, Alert, Button } from "@mui/material";

import {
    ParticipantURL,
    STAGES,
    STAGE_ITEM,
    STIMULUS_DISPLAY_TIME,
    redirectCorrectPage,
    redirectStimulusPage,
    redirectToCorrectErrorPage,
    httpStatusCodes,
    redirectToAuthPage,
} from "../../Utils/Links";

import Response from "./Responses/Response";

class RunningStimulus extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            participantsUrlID: this.props.match.params.id,
            participantID: this.props.match.params.participantID,
            continue: true,
            trial: "",
            response: "",
            actualAnswer: "",
            responseTimer: undefined,
            practiceStatus: "error",
            message: "",
            snackbarOpen: false,
            answer: "",
            rt: "", //response time
            responseStartDisplayTime: Date.now(),
        };
    }

    async componentDidMount() {
        this.props.setAppbar(false, false);
        // if participantID is missing
        if (this.state.participantID === undefined) {
            redirectToAuthPage(this.state.participantsUrlID);
        } else {
            axios
                .get(`${ParticipantURL}/${this.state.participantsUrlID}`)
                .then((res) => {
                    if (res.status === httpStatusCodes.OK) {
                        // if participantID is valid and current stage is tasks
                        if (
                            this.state.participantID !== undefined &&
                            this.props.cookies.get(STAGE_ITEM) === STAGES.TASKS
                        ) {
                            // get trial data for the experiment of current participantID
                            axios
                                .get(
                                    `${ParticipantURL}/${this.state.participantsUrlID}/${this.state.participantID}/get/trial`
                                )
                                .then((res) => {
                                    this.setState({
                                        trial: res.data,
                                    }, () => {
                                        this.setResponse();
                                        this.setActualAnswer();
                                    });
                                })
                                .catch((err) => {
                                    redirectToCorrectErrorPage(err.response.status);
                                });
                        } else {
                            redirectCorrectPage(
                                STAGES.TASKS,
                                this.state.participantsUrlID,
                                this.state.participantID,
                                this.props.cookies,
                            );
                        }
                    } else {
                        redirectToCorrectErrorPage(res.status);
                    }
                })
                .catch((err) => {
                    redirectToCorrectErrorPage(err.response.status);
                });
        }
    }

    componentDidUpdate = () => {
        if (this.state.response && this.state.actualAnswer !== null && this.state.continue) {
            this.showResponseWithDelay();
        }
    };

    /**
     * This function delays the stimulus page with a timer,
     * and shows the response page at that time.
     */
    showResponseWithDelay = () => {
        var timer = setTimeout(() => {
            // when the timer ends
            this.setState({
                continue: true,
            });
            if (this.state.responseTimer) {
                clearInterval(this.state.responseTimer);
            }
        }, this.state.trial.generalFields.responseDuration * 1000);
        this.setState({ responseTimer: timer, continue: false });
    };

    /**
     * This function sets the proper response object which relevant for the current trial.          
     */
    setResponse = () => {
        let response = undefined;
        switch (this.state.trial.taskType) {
            case "Numerosity":
                response = (
                    <Response
                        type="textResponse"
                        validation="positiveInteger"
                    />
                );
                break;
            case "Line Length":
                response = (
                    <Response
                        type="textResponse"
                        validation="positiveInteger"
                    />
                );
                break;
            case "Biological Motion":
                response = (
                    <Response
                        type="binaryResponse"
                        opt1Label="Person"
                        opt1Value="BML"
                        opt2Label="Random moving dots"
                        opt2Value="Random"
                    />
                );
                break;
            case "Keyboard Tapping":
                response = (
                    <Response
                        type="keyboardTappingResponse"
                        hand={this.state.trial.trialData.hand}
                        removeSubmit={true}
                        isPractice={this.state.trial.trialData.isPractice}
                    />
                );
                break;
            case "Mooney Image":
                response = (
                    <Response
                        type="binaryResponse"
                        opt1Label="I see a face"
                        opt1Value={"U" || "I"}
                        opt2Label="I don't see a face"
                        opt2Value="S"
                    />
                );
                break;
            case "Recent Prior Normal":
                response = (
                    <Response
                        type="binaryResponse"
                        opt1Label="Plain gray tiles"
                        opt1Value="Tiles"
                        opt2Label="Tiles with design"
                        opt2Value="Design"
                    />
                );
                break;

            case "Recent Prior Distorted":
                response = (
                    <Response
                        type="quadrupleResponse"
                        opt1Label="I see a part of a person"
                        opt1Value="Part"
                        opt2Label="I see a whole person"
                        opt2Value="Whole"
                        opt3Label="I see a face"
                        opt3Value="Face"
                        opt4Label="I don’t see anyone"
                        opt4Value="None"
                    />
                );
                break;
            case "RDK":
                response = (
                    <Response
                        type="binaryResponse"
                        opt1Label="Right"
                        opt1Value="0"
                        opt2Label="Left"
                        opt2Value="180"
                    />
                );
                break;
            default:
                response = <></>;
                break;
        }
        this.setState({
            response: response,
        })
    };

    /**
     * This function sets the actual answer of the current trial.          
     */
    setActualAnswer = () => {
        let actualAnswer = undefined;
        switch (this.state.trial.taskType) {
            case "Numerosity":
                actualAnswer = this.state.trial.trialData.numberOfObjects;
                break;
            case "Line Length":
                actualAnswer = this.state.trial.trialData.LengthOfLine;
                break;
            case "Biological Motion":
                actualAnswer = this.state.trial.trialData.gifType;
                break;
            case "keyboardTappingResponse":
                actualAnswer = "sk";
                break;
            case "Mooney Image":
                actualAnswer = this.state.trial.trialData.imgType;
                break;
            case "Recent Prior":
                actualAnswer = this.state.trial.trialData.imgType;
                break;
            case "RDK":
                actualAnswer = this.state.trial.trialData.direction;
                break;
            default:
                break;
        }
        this.setState({
            actualAnswer: actualAnswer,
        })
    };

    /**
     * This function verifies the answer of a practice trial.
     */
    verifyPracticeTrial = () => {
        // clear the timer
        if (this.state.responseTimer) {
            clearInterval(this.state.responseTimer);
        }
        // if no answer was given
        if (this.state.answer === "None") {
            this.setState({
                practiceStatus: "error",
                message: "No answer was received, please repeat the trial",
                snackbarOpen: true,
            });
            // if the answer if verified
        } else if (this.verifyAnswer()) {
            this.setState({
                practiceStatus: "success",
                message: "You may proceed to the next trial",
                snackbarOpen: true,
            });
            // if the answer is not verified
        } else {
            this.setState({
                practiceStatus: "error",
                message:
                    "An unreasonable answer was received, please repeat the trial",
                snackbarOpen: true,
            });
        }
    };

    /**
     * This function verify the answer of a trial.
     * @returns Boolean - true if verified, false if not verified.
     */
    verifyAnswer = () => {
        let result = false;
        switch (this.state.response.props.type) {
            case "textResponse":
                if (
                    this.state.answer >= this.state.actualAnswer / 2 &&
                    this.state.answer <= this.state.actualAnswer * 1.5
                ) {
                    result = true;
                }
                break;
            case "binaryResponse":
                if (this.state.answer === this.state.actualAnswer) {
                    result = true;
                }
                break;
            case "keyboardTappingResponse":
                if (this.state.answer === "sksksk") {
                    result = true;
                }
                break;
            default:
                break;
        }
        return result;
    };

    /**
     * This function saves the answer of the participnat to the state.
     * if the trial is a practice trial - it will verify the answer.
     * else - move to the next task.
     * @param {Object} answer - the answer of the participant.
     * @param {Number} responseTime - how much time passed until the participant answered.
     */
    setAnswer = (answer, responseTime) => {
        this.setState({ answer: answer, rt: responseTime }, () => {
            this.moveNextTask();
        });
    };

    /**
     * This function prepare the state to the next task
     */
    moveNextTask = () => {
        // clear the timer
        if (this.state.responseTimer) {
            clearInterval(this.state.responseTimer);
        }
        if (this.state.trial.trialData.isPractice) {
            this.verifyPracticeTrial();
        } else {
            // send the answer to the server in order to save it in db
            this.sendTaskAnswer();
        }
    };

    /**
     * This function sends the answer of the participant to the server, in order to save it in db.     
     */
    sendTaskAnswer = () => {
        const taskAnswer = {
            taskOrder: this.state.trial.taskOrder,
            taskType: this.state.trial.taskType,
            trialNum: this.state.trial.trialNum,
            answer: this.state.answer,
            responseTime: this.state.rt,
            responseStartDisplayTime: this.state.responseStartDisplayTime,
            stimulusStartDisplayTime: this.props.cookies.get(STIMULUS_DISPLAY_TIME),
            isPractice: this.state.trial.trialData.isPractice,
        };
        axios.put(
            `${ParticipantURL}/${this.state.participantsUrlID}/${this.state.participantID}`,
            taskAnswer
        ).then((data) => {
            this.redirectStimulusOrVC();
        });
    };

    redirectStimulusOrVC = () => {
        if (!this.state.trial.isLast) {
            redirectStimulusPage(this.state.participantsUrlID, this.state.participantID);
        } else {
            this.props.cookies.set(STIMULUS_DISPLAY_TIME, "", { path: "/" });
            this.props.cookies.set(STAGE_ITEM, STAGES.VC_END, { path: "/" });
            redirectCorrectPage(
                STAGES.TASKS,
                this.state.participantsUrlID,
                this.state.participantID,
                this.props.cookies,
            );
        }
    }

    /**
     * This function is activated when the snackbar is closed
     */
    onClose = () => {
        this.setState({ snackbarOpen: false }, () => {
            if (this.state.practiceStatus === "success") {
                this.sendTaskAnswer();
            } else {
                redirectStimulusPage(this.state.participantsUrlID, this.state.participantID);
            }
        });
    };

    /**
     * When the component is unmount, clear all created timers.
     */
    componentWillUnmount() {
        if (this.state.responseTimer) {
            clearInterval(this.state.responseTimer);
        }
    }

    render() {
        return (
            <Grid
                container
                spacing={2}
                direction="row"
                justifyContent="center"
                alignItems="center"
                style={{ paddingTop: "7%", }}
            >
                {
                    this.state.response
                        ?
                        <Grid item>
                            {
                                // Get the response and add more data as props
                                React.cloneElement(this.state.response, {
                                    timeRemaining: this.state.trial.generalFields.responseDuration,
                                    trialNum: this.state.trial.trialNum + 1,
                                    maxTrials: this.state.trial.numOfTrials,
                                    taskNum: this.state.trial.taskOrder + 1,
                                    maxTasks: this.state.trial.numOfTasks,
                                    disabled: this.state.snackbarOpen,
                                    continue: this.state.continue,
                                    clearTimeout: this.setAnswer,
                                })
                            }
                        </Grid>
                        :
                        <></>

                }
                <Snackbar open={this.state.snackbarOpen}>
                    <Alert
                        action={
                            <Button
                                color="inherit"
                                size="small"
                                onClick={this.onClose}
                            >
                                {this.state.practiceStatus === "success"
                                    ? "Continue"
                                    : "Try again"}
                            </Button>
                        }
                        variant="filled"
                        severity={this.state.practiceStatus}
                    >
                        {this.state.message}
                    </Alert>
                </Snackbar>
            </Grid>
        );
    }
}
export default RunningStimulus;