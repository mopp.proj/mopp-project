import React from "react";
import axios from "axios";

import { Grid } from "@mui/material";

import TaskDescription from "./TaskDescription";
import Numerosity from "../../Tasks/Numerosity";
import LineLength from "../../Tasks/LineLength";
import BiologicalMotion from "../../Tasks/BiologicalMotion";
import KeyboardTapping from "../../Tasks/KeyboardTapping";
import MooneyImage from "../../Tasks/MooneyImage";
import RDK from "../../Tasks/RDK";
import RecentPriorNormal from "../../Tasks/RecentPriorNormal";
import RecentPriorDistorted from "../../Tasks/RecentPriorDistorted";

import {
    ParticipantURL,
    STAGES,
    STAGE_ITEM,
    STIMULUS_DISPLAY_TIME,
    redirectResponsePage,
    redirectCorrectPage,
    redirectToCorrectErrorPage,
    httpStatusCodes,
    redirectToAuthPage,
} from "../../Utils/Links";

const STIMULUS_COMPONENT = "STIMULUS";
const DESCRIPTION_COMPONENT = "DESCRIPTION";
const DESCRIPTION_TIMER = 120;

class RunningStimulus extends React.Component {
    constructor(props) {
        super(props);        
        this.state = {
            participantsUrlID: this.props.match.params.id,
            participantID: this.props.match.params.participantID,            
            trial: "",
            componentDisplayed: STIMULUS_COMPONENT,
            descriptionTimer: undefined,
            stimulusTimer: undefined,
            continue: true,
            stimulus: "",
            stimulusStartDisplayTime: ""            
        };
    }

    async componentDidMount() {
        this.props.setAppbar(false, false);
        // if participantID is missing
        if (this.state.participantID === undefined) {
            redirectToAuthPage(this.state.participantsUrlID);
        } else {
            axios
            .get(`${ParticipantURL}/${this.state.participantsUrlID}`)
            .then((res) => {
                if (res.status === httpStatusCodes.OK) {
                    // if participantID is valid and current stage is tasks
                    if (
                        this.state.participantID !== undefined &&
                        this.props.cookies.get(STAGE_ITEM) === STAGES.TASKS
                    ) {
                        // get trial data for the experiment of current participantID
                        axios
                            .get(                                
                                `${ParticipantURL}/${this.state.participantsUrlID}/${this.state.participantID}/get/trial`
                            )
                            .then((res) => {                                
                                this.setState({
                                    trial: res.data,
                                    componentDisplayed: res.data.showDescription ? DESCRIPTION_COMPONENT : STIMULUS_COMPONENT,
                                }, () => {
                                    this.setStimulus();
                                    // ~(Yuval)~   START
                                    //console.log('log right after "this.setStimulus();"');
                                    //console.log(this.state.trial.taskType);
                                    // ~(Yuval)~   END
                                    
                                    // ~(Michael Levin)~   START
                                    // Preloading of every image from "Biological Motion" experiment to reduce loading time during the trial itself
                                    // Temporaly fix, change later to something more robust
                                    if (this.state.trial.taskType == "Biological Motion" && !localStorage.getItem('BioMotionGifsArePreloaded')) {
                                        //console.log('Before PreloadBioMotionGifs()');
                                        try {
                                            // Preloads "biological motion" gifs and sets a flag to true (to not call same function again)
                                            this.PreloadBioMotionGifs();
                                            localStorage.setItem('BioMotionGifsArePreloaded', true);
                                        } catch (err) {
                                            // If there are any errors, rset flag to false and next time will try preload them again
                                            //console.log('Error in PreloadBioMotionGifs()');
                                            localStorage.setItem('BioMotionGifsArePreloaded', false);
                                        }
                                    }
                                    // ~(Michael Levin)~   STOP
                                    
                                });                                
                            })
                            .catch((err) => {
                                redirectToCorrectErrorPage(err.response.status);
                            });
                    } else {
                        redirectCorrectPage(
                            STAGES.TASKS,
                            this.state.participantsUrlID,
                            this.state.participantID,
                            this.props.cookies,
                        );
                    }
                } else {
                    redirectToCorrectErrorPage(res.status);
                }
            })
            .catch((err) => {
                redirectToCorrectErrorPage(err.response.status);
            });
        }        
    }

    // ~(Michael Levin)~   START
    PreloadBioMotionGifs = () => {
        // console.log('inside func');
        // Creates an array with every gif's full path
        //
        // example: ['https://mopp-project.com:5443/api/assets/BMLGif/Easy/easy4.gif',
        //          'https://mopp-project.com:5443/api/assets/BMLGif/Hard/hard15.gif]

        let difficulties = ["Easy", "Medium", "Hard", "Random"];
        let common_root_path = "https://mopp-project.com:5443/api/assets/BMLGif/";
        let number_of_files_in_each_folder = 15;
        let files_names = [];

        difficulties.forEach((difficulty_level) => {
            for (let i = 1; i <= number_of_files_in_each_folder; i++) {
                let file_path = common_root_path + difficulty_level + "/" + difficulty_level.toLowerCase() + i + ".gif";
                files_names.push(file_path);
            }
        });

        let images_objs_array = []

        files_names.forEach((file_full_path) => {
            let img_obj = new Image();
            img_obj.src = file_full_path;
            images_objs_array.push(img_obj);
        });
    };

    //PreloadMooneyFaceImages = () => {

    //}
    // ~(Michael Levin)~   STOP


    componentDidUpdate = () => {
        const showDescription = this.state.componentDisplayed === DESCRIPTION_COMPONENT;
        if (showDescription && this.state.continue) {
            this.showDescriptionWithDelay();
        } else if (!showDescription && this.state.continue) {
            this.showStimulusWithDelay();
        }
    };

    /**
     * This function sets a stimulus object (task which will be shown) by queried trial data.
     * @returns Object - a stimulus.
     */
     setStimulus = () => {
        let stimulus = undefined;
        
        switch (this.state.trial.taskType) {
            case "Numerosity":
                stimulus = (
                    <Numerosity 
                        circlesNum={this.state.trial.trialData.numberOfObjects}
                        virtualChinrestData={this.state.trial.virtualChinrest}
                        idx={this.state.trial.trialNum}
                    />
                );
                break;
            case "Line Length":
                stimulus = (
                    <LineLength lineLength={this.state.trial.trialData.LengthOfLine} idx={this.state.trial.trialNum} />
                );
                break;
            case "Biological Motion":
                stimulus = <BiologicalMotion trialData={this.state.trial.trialData} idx={this.state.trial.trialNum} />;
                break;
            case "Keyboard Tapping":
                stimulus = (
                    <KeyboardTapping
                        hand={this.state.trial.trialData.hand}
                        idx={this.state.trial.trialNum}
                        isPractice={this.state.trial.trialData.isPractice}
                    />
                );
                break;
            case "Mooney Image":
                stimulus = <MooneyImage trialData={this.state.trial.trialData} idx={this.state.trial.trialNum} />;
                break;
            case "RDK":
                stimulus = (
                    <RDK 
                        rdkType={this.getNumberFromType(this.state.trial.taskFields.rdkType)}
                        apertureType={this.getNumberFromType(this.state.trial.taskFields.apertureType)}
                        numOfDots={this.state.trial.taskFields.numOfDots}
                        coherence={this.state.trial.taskFields.coherence}
                        trialDuration={this.state.trial.generalFields.stimulusDuration * 1000}
                        coherentDirection={this.state.trial.trialData.direction}
                        idx={this.state.trial.trialNum}
                    />);
                break;

            case "Recent Prior Normal":
                stimulus = <RecentPriorNormal trialData={this.state.trial.trialData} idx={this.state.trial.trialNum} />;
                break;
            case "Recent Prior Distorted":
                stimulus = <RecentPriorDistorted trialData={this.state.trial.trialData} idx={this.state.trial.trialNum} />;
                break;

            default:
                stimulus = <></>;
                break;
        }        
        this.setState({
            stimulus: stimulus,
        });        
    };

    /**
     * This function extracts the number from the beginning of the string.
     * @param {String} stringType - String of Type, which contains "{Number} - {String}""
     * @returns Number - the Number from the string of Type.
     */
    getNumberFromType = (stringType) => {
        return stringType.split(" - ")[0];
    }

    /**
     * This function delays the stimulus page with a timer,
     * and shows the description page at that time.
     */
     showDescriptionWithDelay = () => {        
        var timer = setTimeout(() => {
            // when the timer ends
            this.setState({
                componentDisplayed: STIMULUS_COMPONENT,
                continue: true,
            });            
            if (this.state.descriptionTimer) {
                clearInterval(this.state.descriptionTimer);
            }            
        }, DESCRIPTION_TIMER * 1000);
        this.setState({ descriptionTimer: timer, continue: false });
    };

    /**
     * This function closes the description page,
     * and let the participant to continue to the stimulus page.
     */
    closeDescription = () => {
        if (this.state.descriptionTimer) {
            clearInterval(this.state.descriptionTimer);
        };
        this.setState({
            componentDisplayed: STIMULUS_COMPONENT,                
            continue: true,
        });
    }

    /**
     * This function delays the response page with a timer,
     * and shows the stimulus page at that time.
     */
     showStimulusWithDelay = () => {        
        var timer = setTimeout(() => {
            // when the timer ends
            this.setState({                
                continue: true,
            });
            if (this.state.stimulusTimer) {
                clearInterval(this.state.stimulusTimer);
            }            
            this.props.cookies.set(STIMULUS_DISPLAY_TIME, this.state.stimulusStartDisplayTime, { path: "/" });
            redirectResponsePage(this.state.participantsUrlID, this.state.participantID);
        }, this.state.trial.generalFields.stimulusDuration * 1000);
        if (this.state.stimulusTimer === undefined) {
            this.setState({ stimulusTimer: timer, continue: false, stimulusStartDisplayTime: Date.now() }, );
        }        
    };

    /**
     * When the component is unmount, clear all created timers.
     */
    componentWillUnmount() {        
        if (this.state.descriptionTimer) {
            clearInterval(this.state.descriptionTimer);
        }
        if (this.state.stimulusTimer) {
            clearInterval(this.state.stimulusTimer);
        }
    }

    render() {     
        return (
            <Grid
                container
                spacing={2}
                direction="row"
                justifyContent="center"
                alignItems="center"
                style={{ paddingTop: "0%", }}
            >                
                {
                    this.state.trial.showDescription && this.state.componentDisplayed === DESCRIPTION_COMPONENT
                    ?
                    // show description before the first trial of a task
                    <TaskDescription
                        description={this.state.trial.generalFields.description}
                        closeDescription={this.closeDescription}
                    />
                    :
                    (this.state.stimulus
                        ?
                        <Grid item>
                        {
                            // Get the stimulus and add virtualChinrestData as props
                            React.cloneElement(this.state.stimulus, {
                                virtualChinrestData: this.state.trial.virtualChinrest,
                            })
                        }
                        </Grid>
                        :
                        <></>
                    )                    
                }
            </Grid>
        );
    }
}
export default RunningStimulus;
