import React from "react";

import { Button, Grid, Typography } from "@mui/material";

class TaskDescription extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            description: this.props.description,            
        }
    }

    componentDidUpdate = (prevProps) => {
        if (prevProps.description !== this.props.description) {
            this.setState({
                description: this.props.description,
            });
        }
    }    

    render() {        
        return (
            <Grid
                container
                direction="row"
                justifyContent="center"
                alignItems="center"
                style={{
                    margin: "auto",
                    textAlign: "center",
                    position: "fixed",
                    left: "50%",
                    top: "50%",
                    WebkitTransform: "translate(-50%, -50%)",
                    transform: "translate(-50%, -50%)",
                }}
            >
                <Grid item xs={12}>
                    <Typography
                        variant="h4"
                        gutterBottom
                        style={{
                            margin: "auto",
                            textAlign: "center",                            
                            whiteSpace: "pre-wrap",                            
                        }}
                    >
                        {this.state.description}
                    </Typography>                        
                    
                </Grid>
                <Grid item xs={12}>
                    <Button
                        color="primary"
                        variant="contained"
                        style={{
                            marginTop: "3%"
                        }}
                        onClick={this.props.closeDescription}
                    >
                        Continue
                    </Button>
                </Grid>
            </Grid>
        );
    }
}

export default TaskDescription;