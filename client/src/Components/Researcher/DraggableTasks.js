import React from "react";

import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd";

import TaskCard from "./TaskItems/TaskCard";
import TasksList from "./TaskItems/TasksList";

class DraggableTasks extends React.Component {
    /**
     * This function sets the fields of the task, as a result of TaskItem Component.
     * @param {*} task 
     */
    setDraggableTaskFields = (task) => {
        const newTasks = Array.from(this.props.tasks);
        newTasks[task.taskId].generalFields = task.generalFields;
        newTasks[task.taskId].fields = task.fields;
        newTasks[task.taskId].generalFieldsValidationFunctions = task.generalFieldsValidationFunctions;
        newTasks[task.taskId].fieldsValidationFunctions = task.fieldsValidationFunctions;
        this.props.setTasks(newTasks);
    }

    /**
     * This function removes a task from the state by a given taskId,
     * updates the state of the component, and sends the new array of tasks to ResearcherGUI Component.
     * @param {*} taskId 
     */
    removeTaskFromDraggableTasks = (taskId) => {                
        const newTasks = Array.from(this.props.tasks);
        // remove specifid task
        newTasks.splice(taskId, 1);
        // update the order of the tasks
        newTasks.forEach((task, id) => {
            task.order = id;
        });                
        this.props.removeTaskFromResearcherGui(newTasks);        
    }

    /**
     * This function sets the new order of task after dragging a task, updates the state of the component,
     * and sends the new array of tasks to ResearcherGUI Component.
     * @param {*} result 
     * @returns 
     */
    onDragEnd = (result) => {
        // if the experiment is not already published
        if (!this.props.isPublished) {
            const { destination, source} = result;
            // out of box dragging
            if (!destination) {
                return;
            }
            // same position dragging
            if (destination.droppableId === source.droppableId && destination.index === source.index) {
                return;
            }
            // new position dragging:
            // get old order
            const newOrder = Array.from(this.props.tasks);
            // remove moved item
            var oldItem = newOrder.splice(source.index, 1)[0];
            // insert moved item in the new position
            newOrder.splice(destination.index, 0, oldItem);
            // update the order of the tasks
            newOrder.forEach((task, id) => {
                task.order = id;
            })
            this.props.setTasks(newOrder);
        }        
    };

    render() {        
        return (
            <DragDropContext onDragEnd={this.onDragEnd}>
                <Droppable droppableId="droppable">
                    {(provided1) => (
                        <TasksList provided={provided1} innerRef={provided1.innerRef}>
                            {this.props.tasks.map((task, id) => {
                                return (
                                    <Draggable 
                                        key={task.order} 
                                        draggableId={task.name + "-" + task.order} 
                                        index={id} 
                                        isDragDisabled={this.props.isPublished}
                                    >
                                        {(provided2) => {
                                            return (
                                                <TaskCard
                                                    provided={provided2}
                                                    innerRef={provided2.innerRef}
                                                    task = {task}
                                                    isPublished={this.props.isPublished}
                                                    setDraggableTaskFields={this.setDraggableTaskFields}
                                                    removeTaskFromDraggableTasks={this.removeTaskFromDraggableTasks}
                                                    expandTask={this.props.expandTask}
                                                />
                                            );
                                        }}
                                    </Draggable>
                                );
                            })}
                            {provided1.placeholder}
                        </TasksList>
                    )}
                </Droppable>                
            </DragDropContext>
        );
    }
}
export default DraggableTasks;
