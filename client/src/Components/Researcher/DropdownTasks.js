﻿import React from "react";

import AddIcon from "@mui/icons-material/Add";
import { Grid } from "@mui/material";

import CustomFab from "../General/CustomFab";
import CustomDialog from "../General/CustomDialog";



const tasks = [
    "Numerosity",
    "Line Length",
    "Biological Motion",
    "Mooney Image",
    "Keyboard Tapping",
    "RDK",
    "Recent Prior Normal",
    "Recent Prior Distorted"
];

class DropdownTasks extends React.Component {
    /**
     * CTOR of DropdownTasks
     * @param {*} props
     */
    constructor(props) {
        super(props);
        this.state = {
            openDialog: false,
        };
    }

    openDialog = () => {
        this.setState({
            openDialog: true,
        });
    };

    closeDialog = (value) => {
        if (value) {
            this.props.onButtonClick(value);
        }
        this.setState({
            openDialog: false,
        });
    };

    render() {
        return (
            <Grid item>
                <CustomFab
                    tooltip="Add Task"
                    size="medium"
                    color="primary"
                    label="add"
                    onClick={this.openDialog}
                    icon={<AddIcon />}
                />
                <CustomDialog
                    open={this.state.openDialog}
                    items={tasks}
                    onClose={this.closeDialog}
                />
            </Grid>
        );
    }
}
export default DropdownTasks;
