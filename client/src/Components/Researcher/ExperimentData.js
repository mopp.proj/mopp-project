import React from "react";
import axios from "axios";
import FileDownload from "js-file-download";

import DownloadIcon from '@mui/icons-material/Download';
import { Grid, Typography } from "@mui/material";

import CustomFab from "../General/CustomFab";
import { ExperimentURL } from "../../Utils/Links";
import CustomSnackbar from "../General/CustomSnackbar";

class ExperimentData extends React.Component {
    /**
     * CTOR of ExperimentData.
     * @param {*} props 
     */
    constructor(props) {
        super(props);
        this.state = {
            snackBar: {
                isShown: false,
                severity: "info",
                content: "",
            }
        }
    }
    
    downloadData = () => {
        axios
            .get(`${ExperimentURL}/${this.props.experimentId}/download`)
            .then((result) => {                
                FileDownload(result.data, `${this.props.experimentName.replaceAll(" ", "_")}.csv`);
            })
            .catch((err) => {
                this.setSnackBarState(true, "error", `${err.message}`);
            });
        
    }

    /**
     * Setter of SnackBar's state
     * @param {*} snackBarStatus
     * @param {*} snackBarSeverity
     * @param {*} snackBarContent
     */
     setSnackBarState = (snackBarStatus, snackBarSeverity, snackBarContent) => {
        this.setState({
            snackBar: {
                isShown: snackBarStatus,
                severity: snackBarSeverity,
                content: snackBarContent,
            },
        });
    };

    /**
     * This function hides the alert when the "X" button is pressed
     */
     closeSnackBar = () => {
        this.setSnackBarState(false, "info", "");
    };
    
    render() {
        return (
            <Grid>
                <Typography
                    variant="h5"
                    gutterBottom
                    style={{
                        margin: "auto",
                        textAlign: "center",
                        paddingTop: "1%",
                    }}
                >                   
                    {this.props.participants.numOfCompletedParticipants}/{this.props.participants.numOfParticipants} participants 
                    completed the experiment
                </Typography>
                <br/>
                <Grid 
                    style={{
                        margin: "auto",
                        textAlign: "center",
                    }}
                >
                    <CustomFab
                        tooltip="Download data"
                        size="medium"
                        color="primary"
                        label="download"
                        onClick={
                            this.downloadData
                        }
                        icon={<DownloadIcon />}
                    />    
                </Grid>            

                <CustomSnackbar
                    open={this.state.snackBar.isShown}
                    severity={this.state.snackBar.severity}
                    content={this.state.snackBar.content}
                    onClose={this.closeSnackBar}
                />
            </Grid>
        )
    }
}

export default ExperimentData;