import React from "react";

import CopyAllIcon from "@mui/icons-material/CopyAll";
import ContentCopyIcon from "@mui/icons-material/ContentCopy";
import DeleteForeverIcon from "@mui/icons-material/DeleteForever";
import {
    Card,
    CardContent,
    Grid,
    InputAdornment,
    Typography,
} from "@mui/material";
import { red } from "@mui/material/colors";

import CustomFab from "../General/CustomFab";
import CustomDropdown from "../General/CustomDropdown";
import CustomTextField from "../General/CustomTextField";

import { HomepageURL } from "../../Utils/Links";

const ExperimentTypes = ["Supervised", "Public"];

class ExperimentGeneralData extends React.Component {
    /**
     * CTOR of ExperimentGeneralData
     * @param {*} props
     */
    constructor(props) {
        super(props);
        this.state = {
            name: this.props.name,
            startDate: this.props.startDate,
            endDate: this.props.endDate,
            experimentType: this.props.experimentType,
            generalDataValidationFunctions:
                this.props.generalDataValidationFunctions || {},
            datesError: false,
            datesErrorText: "",
            paymentCode:this.props.paymentCode,
        };
    }

    // This function updates the state of the component after the component is updated
    componentDidUpdate = (prevProps) => {
        if (this.state.name !== this.props.name) {
            this.setState({
                name: this.props.name,
                startDate: this.props.startDate,
                endDate: this.props.endDate,
                experimentType: this.props.experimentType,
                generalDataValidationFunctions:
                    this.props.generalDataValidationFunctions || {},
                paymentCode:this.props.paymentCode
            });
        }
    };

    /**
     * Setter of Experiment's name field
     * @param {*} obj
     */
    setExperimentName = (value, func) => {
        this.setState(
            (prevState) => ({
                name: value,
                generalDataValidationFunctions: {
                    ...prevState.generalDataValidationFunctions,
                    name: func,
                },
            }),
            () => {
                this.props.setResearcherGuiGeneralData(this.state);
            }
        );
    };

    /**
     * Setter of Experiment Type field
     * @param {*} value
     */
    setExperimentType = (value, func) => {
        this.setState(
            (prevState) => ({
                experimentType: value,
                generalDataValidationFunctions: {
                    ...prevState.generalDataValidationFunctions,
                    experimentType: func,
                },
            }),
            () => {
                this.props.setResearcherGuiGeneralData(this.state);
            }
        );
    };

    /**
     * Setter of Experiment's start date field
     * @param {*} value
     */
    setExperimentStartDate = (value, func) => {
        this.setState(
            (prevState) => ({
                startDate: value,
                generalDataValidationFunctions: {
                    ...prevState.generalDataValidationFunctions,
                    startDate: func,
                },
            }),
            () => {
                this.compareDates();
            }
        );
    };

    /**
     * Setter of Experiment's end date field
     * @param {*} value
     */
    setExperimentEndDate = (value, func) => {
        this.setState(
            (prevState) => ({
                endDate: value,
                generalDataValidationFunctions: {
                    ...prevState.generalDataValidationFunctions,
                    endDate: func,
                },
            }),
            () => {
                this.compareDates();
            }
        );
    };

    /**
     * Setter of Experiment's payment code
     * @param {*} obj
     */
     setPaymentCode = (value, func) => {
        this.setState(
            (prevState) => ({
                paymentCode: value,
                generalDataValidationFunctions: {
                    ...prevState.generalDataValidationFunctions,
                    paymentCode: func,
                },
            }),
            () => {
                this.props.setResearcherGuiGeneralData(this.state);
            }
        );
    };

    compareDates = () => {
        if (this.startDate !== "" && this.endDate !== "") {
            let startDate = new Date(this.state.startDate);
            let endDate = new Date(this.state.endDate);
            if (startDate.getTime() > endDate.getTime()) {
                this.setState(
                    {
                        datesError: true,
                        datesErrorText:
                            "Start date must be smaller than end date",
                    },
                    () => {
                        this.props.setResearcherGuiGeneralData(this.state);
                    }
                );
            } else {
                this.setState(
                    {
                        datesError: false,
                        datesErrorText: "",
                    },
                    () => {
                        this.props.setResearcherGuiGeneralData(this.state);
                    }
                );
            }
        } else {
            this.setState(
                {
                    datesError: false,
                    datesErrorText: "",
                },
                () => {
                    this.props.setResearcherGuiGeneralData(this.state);
                }
            );
        }
    };

    copyURL = () => {
        navigator.clipboard.writeText(
            `${HomepageURL}/${this.props.participantsUrlId}`
        );
        this.props.setCopyAlert(true, "success", "Copied Experiment's URL");
    };

    copyPasscode = () => {
        navigator.clipboard.writeText(this.props.passcode);
        this.props.setCopyAlert(
            true,
            "success",
            "Copied Experiment's Passcode"
        );
    };

    render() {
        const fabErrorStyle = {
            color: "common.white",
            bgcolor: red[500],
            "&:hover": {
                bgcolor: red[600],
            },
        };

        return (
            <Grid>
                <Grid
                    container
                    direction="row"
                    justifyContent="space-between"
                    alignItems="center"
                >
                    <Grid item xs={10}>
                        <Typography variant="h5" gutterBottom>
                            Experiment General Data
                        </Typography>
                    </Grid>
                    <Grid item>
                        <CustomFab
                            tooltip="Clone Experiment"
                            size="medium"
                            color="secondary"
                            label="clone"
                            onClick={this.props.cloneExperiment}
                            icon={<CopyAllIcon />}
                        />
                    </Grid>
                    <Grid item>
                        <CustomFab
                            tooltip="Delete Experiment"
                            size="medium"
                            color="inherit"
                            sx={{ ...fabErrorStyle }}
                            label="delete"
                            onClick={this.props.deleteExperiment}
                            icon={<DeleteForeverIcon />}
                        />
                    </Grid>
                </Grid>
                <Card style={{ margin: "10px" }}>
                    <CardContent>
                        <Grid
                            container
                            direction="row"
                            justifyContent="space-between"
                            alignItems="center"
                            spacing={2}
                        >
                            <Grid item>
                                <CustomTextField
                                    label="Experiment name"
                                    validation="nonEmptyString"
                                    onBlur={this.setExperimentName}
                                    value={this.props.name}
                                    disabled={this.props.isPublished}
                                />
                            </Grid>
                            <Grid item>
                                <CustomDropdown
                                    options={ExperimentTypes}
                                    label="Experiment Type"
                                    onChange={this.setExperimentType}
                                    value={this.state.experimentType}
                                    disabled={this.props.isPublished}
                                />
                            </Grid>
                            <Grid item>
                                <CustomTextField
                                    id="date"
                                    label="Start Date"
                                    type="date"
                                    validation="nonEmptyString"
                                    onBlur={this.setExperimentStartDate}
                                    value={this.props.startDate}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    disabled={this.props.isPublished}
                                    isError={this.state.datesError}
                                    error={this.state.datesErrorText}
                                />
                            </Grid>
                            <Grid item>
                                <CustomTextField
                                    label="End Date"
                                    type="date"
                                    validation="nonEmptyString"
                                    onBlur={this.setExperimentEndDate}
                                    value={this.props.endDate}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    disabled={this.props.isPublished}
                                />
                            </Grid>
                            {this.props.isPublished ? (
                                <Grid item>
                                    <CustomTextField
                                        label="Experiment's URL"
                                        sx={{ width: 450 }}
                                        value={`${HomepageURL}/${this.props.participantsUrlId}`}
                                        disabled={true}
                                        InputProps={{
                                            endAdornment: (
                                                <InputAdornment position="end">
                                                    <CustomFab
                                                        tooltip="Copy URL"
                                                        size="small"
                                                        color="default"
                                                        label="copy"
                                                        onClick={this.copyURL}
                                                        icon={
                                                            <ContentCopyIcon />
                                                        }
                                                    />
                                                </InputAdornment>
                                            ),
                                        }}
                                    />
                                </Grid>
                            ) : (
                                <></>
                            )}
                            {this.props.isPublished &&
                            this.props.experimentType === "Supervised" ? (
                                <Grid item>
                                    <CustomTextField
                                        label="Experiment's Passcode"
                                        value={this.props.passcode}
                                        disabled={true}
                                        InputProps={{
                                            endAdornment: (
                                                <InputAdornment position="end">
                                                    <CustomFab
                                                        tooltip="Copy Passcode"
                                                        size="small"
                                                        color="default"
                                                        label="copy"
                                                        onClick={
                                                            this.copyPasscode
                                                        }
                                                        icon={
                                                            <ContentCopyIcon />
                                                        }
                                                    />
                                                </InputAdornment>
                                            ),
                                        }}
                                    />
                                </Grid>
                            ) : (
                                <></>
                            )}
                        </Grid>
                        <Grid container marginTop={1} spacing={2} direction="row"
                    alignItems="center">
                            <Grid item>
                                <CustomTextField
                                label="Payment Code"
                                onBlur={this.setPaymentCode}
                                value={this.props.paymentCode}
                                disabled={this.props.isPublished}
                                />
                            </Grid>
                            <Grid item>
                                <Typography>
                                This is an optional field. Use it in case you plan to distribute the experiment through recruitment platforms.
                                <br/>In such cases, this code will be presented only when the participant has fully completed the experiment.
                                </Typography>
                            </Grid>
                        </Grid>
                    </CardContent>
                </Card>
            </Grid>
        );
    }
}

export default ExperimentGeneralData;
