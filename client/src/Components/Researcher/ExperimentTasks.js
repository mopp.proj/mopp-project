import React from "react";

import { FormControlLabel, FormGroup, Grid, Switch } from "@mui/material";
import DraggableTasks from "./DraggableTasks";

class ExperimentTasks extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            switchOn: true,
        };
    }

    changeSwitch = () => {
        this.setState({
            switchOn: !this.state.switchOn,
        });
    };

    render() {        
        return (
            <Grid>
                <FormGroup style={{ marginLeft: "10px" }}>
                    <FormControlLabel
                        control={<Switch checked={this.state.switchOn} />}
                        onClick={this.changeSwitch}
                        label="Expand all tasks"
                    />
                </FormGroup>
                <DraggableTasks
                    tasks={this.props.tasks}
                    isPublished={this.props.isPublished}
                    setTasks={this.props.setTasks}
                    removeTaskFromResearcherGui={this.props.removeTaskFromResearcherGui}
                    expandTask={this.state.switchOn}
                />                
            </Grid>
        );
    }
}

export default ExperimentTasks;
