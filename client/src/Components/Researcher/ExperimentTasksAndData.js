import React from "react";

import {Grid, ToggleButton, ToggleButtonGroup, Typography } from "@mui/material";
import ExperimentTasks from "./ExperimentTasks";
import ExperimentData from "./ExperimentData";

class ExperimentTasksAndData extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            toggleValue: "tasks",
        }
    }

    /**
     * This function changes the toggle
     * @param {*} event
     * @param {*} newValue
     */
     toggleSwitch = (event, newValue) => {
        this.setState({
            toggleValue: newValue,
        }, () => {
            this.props.setToggleValue(newValue);
        });
    };

    render() {        
        return (            
            <Grid>
                <Grid item xs={12}>
                    <ToggleButtonGroup
                        color="primary"
                        value={this.state.toggleValue}
                        exclusive
                        onChange={this.toggleSwitch}
                        fullWidth={true}
                    >
                        <ToggleButton value="tasks" style={{ textTransform: "none" }}>
                            <Typography variant="h6" gutterBottom>
                                Experiment's Tasks
                            </Typography>
                        </ToggleButton>
                        <ToggleButton value="data" style={{ textTransform: "none" }}>
                            <Typography variant="h6" gutterBottom>
                                Experiment's Data
                            </Typography>
                        </ToggleButton>
                    </ToggleButtonGroup>
                </Grid>                                
                {this.state.toggleValue === "tasks" 
                ?                
                <Grid item xs={12}>
                    <ExperimentTasks
                        tasks={this.props.tasks}
                        isPublished={this.props.isPublished}
                        setTasks={this.props.setTasks}                            
                        removeTaskFromResearcherGui={this.props.removeTaskFromResearcherGui}                            
                    />
                </Grid>
                : 
                <Grid item xs={12}>
                    <ExperimentData
                        experimentId={this.props.experimentId}
                        experimentName={this.props.experimentName}
                        participants={this.props.participants}
                    />
                </Grid>
                }
                
            </Grid>
        );
    }
}

export default ExperimentTasksAndData;