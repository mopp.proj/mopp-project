import React from "react";
import axios from "axios";

import AddIcon from "@mui/icons-material/Add";
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";
import { Card, CardContent, Container, Grid, Table, TableContainer, Paper, TableHead, 
    TableRow, TableCell, TableBody, Typography, TableSortLabel } from "@mui/material";

import CustomFab from "../General/CustomFab";
import { formatDate } from "../../Utils/Utils";
import { 
    ExperimentURL, 
    PASS_ITEM,
    redirectToCorrectErrorPage,
    redirectToExperimentPage, 
    redirectToForbiddenAccess 
} from "../../Utils/Links";

const headCells = [
    {
        id: 'name',
        numeric: false,                    
        label: "Experiment's Name"
    },
    {
        id: 'startDate',
        numeric: false,                    
        label: "Start Date"
    },
    {
        id: 'endDate',
        numeric: false,
        label: "End Date"
    },
    {
        id: 'isPublished',
        numeric: false,                    
        label: "Published"
    },
    {
        id: 'tasksNumber',
        numeric: true,                    
        label: "No. of Tasks"
    },
    {
        id: 'numOfParticipants',
        numeric: true,
        label: "No. of Participants"
    },
    {
        id: 'numOfCompletedParticipants',
        numeric: true,
        label: "No. of Completed"
    }
]

class ExperimentsTable extends React.Component {
    /**
     * CTOR of ExperimentsTable
     * @param {*} props 
     */
    constructor(props) {
        super(props);
        this.state = {
            experiments: [],
            order: 'asc',
            orderBy: 'name',

        }
    }
    
    /**
     * This function gets all experiments from the db when the component is created,
     * and saves it to the state of the component.
     */
    async componentDidMount() {        
        if (!this.props.cookies.get(PASS_ITEM)) {
            redirectToForbiddenAccess();
        } else {
            await axios.get(`${ExperimentURL}/`)
            .then(res => {
                let exps = res.data;
                exps.forEach(exp => {
                    switch(exp.isPublished) {
                        case true: exp.isPublished = 'Yes'; break;
                        case false: exp.isPublished = 'Not yet'; break;
                        default: break;
                    }
                });                
                this.setState({
                    experiments: exps,
                }, () => {                    
                    this.props.setAppbar(true, this.props.cookies.get(PASS_ITEM));
                });
            })
        }        
    }    

    /**
     * This function changes the state of orderBy and the order direction (asc, desc);
     * @param {*} headCellID 
     */
    sortHandler = (headCellID) => {
        this.setState({
            orderBy: headCellID,
            order: this.state.orderBy === headCellID ? (this.state.order === 'asc' ? 'desc' : 'asc') : 'asc',
        });        
    }

    /**
     * This function sorts the data in the table, as the result of the state of orderBy and order.
     * @param {*} a 
     * @param {*} b 
     * @param {*} orderBy 
     * @returns 
     */
    descendingComparator = (a, b, orderBy) => {                
        if (headCells.find(x=> x.id === orderBy).numeric) {
          return a[orderBy] - b[orderBy];
        }
      
        if (b[orderBy] < a[orderBy]) {
          return -1;
        }
        if (b[orderBy] > a[orderBy]) {
          return 1;
        }
        return 0;
      }
      
    /**
     * This function returns the order of the data, as a result of the state of orderBy and order.
     * @returns 
     */
    getComparator = () => {
    return this.state.order === 'desc'
        ? (a, b) => this.descendingComparator(a, b, this.state.orderBy)
        : (a, b) => -this.descendingComparator(a, b, this.state.orderBy);
    }
    
    /**
     * This function returns the sorted data to the table.
     * @param {*} array 
     * @param {*} comparator 
     * @returns 
     */
    stableSort = (array, comparator) => {
    const stabilizedThis = array.map((el, index) => [el, index]);  
    stabilizedThis.sort((a, b) => {
        const order = comparator(a[0], b[0]);
        if (order !== 0) return order;
        return a[0] - b[0];
    });  
        return stabilizedThis.map((el) => el[0]);
    }

    /**
     * This function sends a POST request, in order to create a new experiment, 
     * and navigates to the page of the new experiment.
     */
    addExperiment = () => {        
        const now = new Date();
        const nextWeek = new Date();
        nextWeek.setDate(now.getDate() + 7)        
        const nextNextWeek = new Date();
        nextNextWeek.setDate(now.getDate() + 14)

        const req = {            
            name: 'new_experiment',
            startDate: nextWeek,
            endDate: nextNextWeek,
        }        
        axios.post(`${ExperimentURL}/`, req)
            .then(res => {
                redirectToExperimentPage(res.data._id);                
            })
            .catch(err => {                
                redirectToCorrectErrorPage(err.response.status);
            })
    }

    render() {        
        if(!this.props.cookies.get(PASS_ITEM)) {        
            return (<div></div>);
        }
        return (
            <Container>
                <Card style={{ margin: "5px" }}>                                       
                    <CardContent>
                        <Grid container direction="row" justifyContent="space-between" alignItems="flex-end" >
                            <Grid item>
                                <Typography variant="h5" gutterBottom>                        
                                    Experiments History
                                </Typography>                           
                            </Grid>
                            <Grid item>                                
                                <CustomFab 
                                    tooltip="Create Experiment" 
                                    size="medium" 
                                    color="primary" 
                                    label="add"
                                    onClick={this.addExperiment}
                                    icon={<AddIcon />}
                                />
                            </Grid>
                        </Grid>
                        <br/>
                        <TableContainer component={Paper} sx={{ maxHeight: 500 }}>
                            <Table sx={{ minWidth: 300 }} aria-label="experiments table" stickyheader={"true"}>
                                <TableHead stickyheader={"true"}>
                                    <TableRow>
                                        {
                                            headCells.map((headCell) => (
                                                <TableCell
                                                    key={headCell.id}
                                                    align="center"                                                        
                                                    sortDirection={this.state.orderBy === headCell.id ? this.state.order : false}
                                                >
                                                    <TableSortLabel
                                                        active={this.state.orderBy === headCell.id}
                                                        direction={this.state.orderBy === headCell.id ? this.state.order : 'asc'}
                                                        onClick={() => this.sortHandler(headCell.id)}
                                                    >
                                                        {headCell.label}                                                            
                                                    </TableSortLabel>
                                                </TableCell>
                                            ))
                                        }                                                                                     
                                        <TableCell align="center" >Experiment's Page</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>                                        
                                    {this.stableSort(this.state.experiments, this.getComparator())
                                        .map((experiment) => (                                            
                                            <TableRow
                                                key={experiment._id}
                                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}                                                
                                            >                                                
                                                <TableCell align="left">{experiment.name}</TableCell>
                                                <TableCell align="center">{formatDate(experiment.startDate)}</TableCell>
                                                <TableCell align="center">{formatDate(experiment.endDate)}</TableCell>
                                                <TableCell align="center">{experiment.isPublished.toString()}</TableCell>
                                                <TableCell align="center">{experiment.tasksNumber}</TableCell>
                                                <TableCell align="center">{experiment.numOfParticipants}</TableCell>
                                                <TableCell align="center">{experiment.numOfCompletedParticipants}</TableCell>
                                                <TableCell align="center">
                                                    <CustomFab 
                                                        tooltip="Go to Experiment's Page" 
                                                        size="small" 
                                                        color="primary" 
                                                        onClick={() => {redirectToExperimentPage(experiment._id)}}
                                                        icon={<ArrowForwardIcon />}
                                                    />
                                                </TableCell>
                                            </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </CardContent>
                </Card>                
            </Container>            
        )
    }
};

export default ExperimentsTable;