import React from "react";
import axios from "axios";

import SaveIcon from "@mui/icons-material/Save";
import ScienceIcon from "@mui/icons-material/Science";
import { Card, CardContent, Container, Grid } from "@mui/material";

import { formatDate } from "../../Utils/Utils";
import CustomFab from "../General/CustomFab";
import CustomSnackbar from "../General/CustomSnackbar";
import CustomAlertDialog from "../General/CustomAlertDialog";
import DropdownTasks from "./DropdownTasks";
import ExperimentGeneralData from "./ExperimentGeneralData";
import ExperimentTasksAndData from "./ExperimentTasksAndData";
import SurveyComponent from "./SurveyComponent";
import data from "./survey.json";

import {
    ExperimentURL,
    httpStatusCodes,
    redirectToExperimentPage,
    PASS_ITEM,
    redirectToCorrectErrorPage,
    redirectToForbiddenAccess,
} from "../../Utils/Links";
import ScrollToTopButton from "../General/ScrollToTopButton";

class ResearcherGUI extends React.Component {
    /**
     * CTOR of ResearcherGUI
     * @param {*} props
     */
    constructor(props) {
        super(props);

        this.state = {
            experimentId: this.props.match.params.id,
            invalidExperimentId: false,
            name: "",
            experimentType: undefined,
            startDate: "",
            endDate: "",
            paymentCode:"",
            datesError: false,
            generalDataValidationFunctions: {},
            isPublished: false,
            participantsUrlId: "",
            participantList: [],
            tasks: [],
            surveyJson: data,
            passcode: "",
            clicks: 0,
            toggleValue: "tasks",
            snackBar: {
                isShown: false,
                severity: "info",
                content: "",
            },
            alertDialog: {
                isOpen: false,
                onAgree: undefined,
                title: "",
                content: "",
            },
        };
    }

    /**
     * This function sends a GET request using the id of the experiment,
     * and updates the state of the component
     */
    async componentDidMount() {
        if (!this.props.cookies.get(PASS_ITEM)) {        
            redirectToForbiddenAccess();
        }
        await axios
            .get(`${ExperimentURL}/${this.state.experimentId}`)
            .then((result) => {
                this.setState(
                    {
                        name: result.data.name,
                        experimentType: result.data.experimentType,
                        startDate: formatDate(result.data.startDate),
                        endDate: formatDate(result.data.endDate),
                        passcode: result.data.passcode,
                        isPublished: result.data.isPublished,
                        participantsUrlId: result.data.participantsUrlId,
                        tasks: result.data.tasks,
                        participantList: result.data.participants,
                        clicks: result.data.tasks.length,
                        paymentCode:result.data.paymentCode,
                    },
                    () => {
                        this.props.setAppbar(
                            true,
                            this.props.cookies.get(PASS_ITEM)                            
                        );
                    }
                );
            })
            .catch((err) => {
                this.setState(
                    {
                        invalidExperimentId: true,
                    },
                    () => {
                        redirectToCorrectErrorPage(err.response.status);
                    }
                );
            });
    }

    /**
     * Setter of SnackBar's state
     * @param {*} snackBarStatus
     * @param {*} snackBarSeverity
     * @param {*} snackBarContent
     */
    setSnackBarState = (snackBarStatus, snackBarSeverity, snackBarContent) => {
        this.setState({
            snackBar: {
                isShown: snackBarStatus,
                severity: snackBarSeverity,
                content: snackBarContent,
            },
        });
    };

    /**
     * This function hides the alert when the "X" button is pressed
     */
    closeSnackBar = () => {
        this.setSnackBarState(false, "info", "");
    };

    /**
     * This function changes the toggle
     * @param {*} event
     * @param {*} newValue
     */
    setToggleValue = (newValue) => {
        this.setState({
            toggleValue: newValue,
        });
    };

    /**
     * This function gets the new order of the tasks (from DraggableTasks)
     * and updates them in the state of the component.
     * @param {*} newTasksOrder
     */
    setTasks = (newTasksOrder) => {
        this.setState({
            tasks: newTasksOrder,
        });
    };

    setGeneralData = (generalData) => {
        this.setState(generalData);
    };

    /**
     * This function gets an array of tasks after a task was removed (from TaskCard Component),
     * updates the state of the component and updates the task array in the db.
     * @param {*} tasksWithoutRemoved
     */
    removeTaskFromResearcherGui = (tasksWithoutRemoved) => {
        this.setState(
            {
                tasks: tasksWithoutRemoved,
                clicks: this.state.clicks - 1,
            },
            () => {
                this.removeTask();
            }
        );
    };

    /**
     * This function sends a PUT request in order to update the array of tasks in the db,
     * and shows an alert of success / error.
     */
    removeTask = () => {
        axios
            .put(`${ExperimentURL}/${this.state.experimentId}`, this.state)
            .then((res) => {
                // if the request was successfull
                if (res.status === httpStatusCodes.OK) {
                    this.setSnackBarState(
                        true,
                        "success",
                        "Task removed successfully!"
                    );
                } else {
                    // if the request was not successfull
                    this.setSnackBarState(true, "error", `${res.statusText}`);
                }
            })
            // if there is an error with the request
            .catch((err) => {
                this.setSnackBarState(
                    true,
                    "error",
                    `${err.response.data["message"]}`
                );
            });
    };

    /**
     * This function adds a new task to the array of tasks.
     * @param {*} task
     */
    addTask = (task) => {
        this.setState((prevState) => ({
            ...prevState,
            tasks: [
                ...prevState.tasks,
                {
                    order: prevState.clicks,
                    type: task,
                },
            ],
            clicks: prevState.clicks + 1,
        }));
    };
    /**
     * The function gets all the fields validation functions and runs all of them 
     * @returns True if there is an error in the validations
     */
    runValidations = () => {
        let isError = this.state.datesError;

        // for each text-field in the general data section of the experiment
        for (const f in this.state.generalDataValidationFunctions) {
            if (this.state.generalDataValidationFunctions[f]()) {
                isError = true;
            }
        }
        // for each task
        this.state.tasks.forEach((task) => {
            // for each text-field in the general data section of the task
            for (const f in task.generalFieldsValidationFunctions) {
                if (task.generalFieldsValidationFunctions[f]()) {
                    isError = true;
                }
            }
            // for each text-field in the specific task section
            for (const f in task.fieldsValidationFunctions) {
                if (task.fieldsValidationFunctions[f]()) {
                    isError = true;
                }
            }
        });
        return isError;
    };
    /**
     * This function saves the current state of the experiment to the db.
     */
    tryToSave = () => {
        this.saveExperiment()
            .then((res) => {
            })
            .catch((err) => {
            });
    };

    /**
     * This function tries to save the experiment.
     * @returns Promise - resolve() if succeeded, reject() if not.
     */
    saveExperiment = () => {
        const result = this.runValidations();
        return new Promise((resolve, reject) => {
            // if there is an error in one of the tasks
            if (this.state.tasks === [] || result) {
                this.setSnackBarState(
                    true,
                    "error",
                    "Please solve error in all of the tasks"
                );
                reject();
            } else {
                axios
                    .put(
                        `${ExperimentURL}/${this.state.experimentId}`,
                        this.state
                    )
                    .then((res) => {
                        if (res.status === httpStatusCodes.OK) {
                            // if the request was successfull
                            this.setSnackBarState(
                                true,
                                "success",
                                "Experiment saved successfully!"
                            );
                            resolve();
                        } else {
                            // if the request was not successfull
                            this.setSnackBarState(
                                true,
                                "error",
                                `${res.statusText}`
                            );
                            reject();
                        }
                    })
                    .catch((err) => {
                        // if there is an error with the request
                        this.setSnackBarState(
                            true,
                            "error",
                            `${err.response.data["message"]}`
                        );
                        reject();
                    });
            }
        });
    };

    setAlertDialog = (isOpen, onAgree, title, content) => {
        this.setState({
            alertDialog: {
                isOpen: isOpen,
                onAgree: onAgree,
                title: title,
                content: content,
            },
        });
    };

    closeAlertDialog = () => {
        this.setAlertDialog(false, undefined, "", "");
    };

    setAlertDialogOfPublish = () => {
        this.setAlertDialog(
            true,
            this.agreeToPublish,
            "Are you sure you want to publish the experiment?",
            "After publishing the experiment you will not be able to edit any task"
        );
    };

    agreeToPublish = () => {
        this.setState(
            {
                alertDialog: {
                    isOpen: false,
                    onAgree: undefined,
                    title: "",
                    content: "",
                },
            },
            () => {
                this.publishExperiment();
            }
        );
    };

    /**
     * This function sends a PUT request in order to publish the experiment (generate the relevant fields
     * for each trial of each task).
     */
    publishExperiment = () => {
        this.saveExperiment().then((val) => {
            axios
                .put(`${ExperimentURL}/publish/${this.state.experimentId}`)
                .then((res) => {
                    if (res.status === httpStatusCodes.OK) {
                        // if the request was successfull
                        this.setState({
                            isPublished: true,
                            passcode: res.data.passcode,
                            participantsUrlId: res.data.participantsUrlId,
                        });
                        this.setSnackBarState(
                            true,
                            "success",
                            "Experiment published successfully!"
                        );
                    } else {
                        // if the request was not successfull
                        this.setSnackBarState(
                            true,
                            "error",
                            `${res.statusText}`
                        );
                    }
                })
                .catch((err) => {
                    // if there is an error with the request
                    this.setSnackBarState(
                        true,
                        "error",
                        err.response.data["message"]
                    );
                });
        });
    };

    uploadCustomizedSurveyJson = (event) => {
        var thisClass = this;
        event.stopPropagation();
        event.preventDefault();
        var file = event.target.files[0];
        var regex = /(\.json)$/i;
        if (!regex.exec(file.name)) {
            this.setSnackBarState(
                true,
                "error",
                "Please upload a valid JSON file"
            );
        } else {
            var fr = new FileReader();
            fr.onload = (function () {
                return function (e) {
                    thisClass.setState(
                        { surveyJson: JSON.parse(e.target.result) },
                        () => {
                            axios
                                .put(
                                    `${ExperimentURL}/${thisClass.state.experimentId}`,
                                    thisClass.state
                                )
                                .then((res) => {
                                    if (res.status === httpStatusCodes.OK) {
                                        // if the request was successfull
                                        thisClass.setSnackBarState(
                                            true,
                                            "success",
                                            "Survey JSON file was uploaded successfully!"
                                        );
                                    } else {
                                        // if the request was not successfull
                                        thisClass.setSnackBarState(
                                            true,
                                            "error",
                                            `${res.statusText}`
                                        );
                                    }
                                })
                                .catch((err) => {
                                    // if there is an error with the request
                                    thisClass.setSnackBarState(
                                        true,
                                        "error",
                                        `${err.response.data["message"]}`
                                    );
                                });
                        }
                    );
                };
            })(file);
            fr.readAsText(file);
        }
    };

    setAlertDialogOfClone = () => {
        this.setAlertDialog(
            true,
            this.agreeToClone,
            "Are you sure you want to clone the experiment?",
            "After cloning the experiment you will be redirected to the cloned experiment page"
        );
    };

    agreeToClone = () => {
        this.setState(
            {
                alertDialog: {
                    isOpen: false,
                    onAgree: undefined,
                    title: "",
                    content: "",
                },
            },
            () => {
                this.prepareToCloneExperiment();
            }
        );
    };

    prepareToCloneExperiment = () => {
        if (!this.state.isPublished) {
            this.saveExperiment().then((val) => {
                this.cloneExperiment();
            });
        } else {
            this.cloneExperiment();
        }
    };

    cloneExperiment = () => {
        let clonedData = {
            name: `${this.state.name}_cloned`,
            experimentType: this.state.experimentType,
            startDate: new Date(this.state.startDate),
            endDate: new Date(this.state.endDate),
            isPublished: false,
            tasks: this.state.tasks,
        };

        axios
            .post(`${ExperimentURL}/`, clonedData)
            .then((res) => {
                redirectToExperimentPage(res.data._id);
            })
            .catch((err) => {
                // if there is an error with the request
                this.setSnackBarState(
                    true,
                    "error",
                    "Experiment could not be cloned properly"
                );
            });
    };

    setAlertDialogOfDelete = () => {
        this.setAlertDialog(
            true,
            this.agreeToDelete,
            "Are you sure you want to delete the experiment?",
            "This action is irreversible, and all experiment's data will be lost!"
        );
    };

    agreeToDelete = () => {
        this.setState(
            {
                alertDialog: {
                    isOpen: false,
                    onAgree: undefined,
                    title: "",
                    content: "",
                },
            },
            () => {
                this.deleteExperiment();
            }
        );
    };

    deleteExperiment = () => {
        axios
            .delete(`${ExperimentURL}/${this.state.experimentId}`)
            .then((res) => {
                redirectToExperimentPage();
            })
            .catch((err) => {
                // if there is an error with the request
                this.setSnackBarState(
                    true,
                    "error",
                    "Experiment could not be deleted properly"
                );
            });
    };

    render() {        
        if (
            this.state.invalidExperimentId ||
            !this.props.cookies.get(PASS_ITEM)            
        ) {
            return <div></div>;
        }
        return (
            <div>
                <Container>
                    <Card style={{ margin: "10px" }}>
                        <CardContent>
                            <Grid container spacing={2}>
                                <Grid item xs={12}>
                                    <ExperimentGeneralData
                                        name={this.state.name}
                                        experimentType={
                                            this.state.experimentType
                                        }
                                        startDate={this.state.startDate}
                                        endDate={this.state.endDate}
                                        generalDataValidationFunctions={
                                            this.state
                                                .generalDataValidationFunctions
                                        }
                                        isPublished={this.state.isPublished}
                                        passcode={this.state.passcode}
                                        paymentCode={this.state.paymentCode}
                                        cloneExperiment={
                                            this.setAlertDialogOfClone
                                        }
                                        deleteExperiment={
                                            this.setAlertDialogOfDelete
                                        }
                                        participantsUrlId={
                                            this.state.participantsUrlId
                                        }
                                        setResearcherGuiGeneralData={
                                            this.setGeneralData
                                        }
                                        setCopyAlert={this.setSnackBarState}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <SurveyComponent
                                        isPublished={this.state.isPublished}
                                        uploadSurveyJson={
                                            this.uploadCustomizedSurveyJson
                                        }
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <ExperimentTasksAndData
                                        experimentId={this.state.experimentId}
                                        experimentName={this.state.name}
                                        tasks={this.state.tasks}
                                        isPublished={this.state.isPublished}
                                        setToggleValue={this.setToggleValue}
                                        setTasks={this.setTasks}
                                        participants={
                                            this.state.participantList
                                        }
                                        removeTaskFromResearcherGui={
                                            this.removeTaskFromResearcherGui
                                        }
                                        saveExperiment={this.tryToSave}
                                        publishExperiment={
                                            this.setAlertDialogOfPublish
                                        }
                                    />
                                </Grid>
                            </Grid>

                            <CustomSnackbar
                                open={this.state.snackBar.isShown}
                                severity={this.state.snackBar.severity}
                                content={this.state.snackBar.content}
                                onClose={this.closeSnackBar}
                            />
                            <CustomAlertDialog
                                isOpen={this.state.alertDialog.isOpen}
                                onAgree={this.state.alertDialog.onAgree}
                                onDisagree={this.closeAlertDialog}
                                title={this.state.alertDialog.title}
                                content={this.state.alertDialog.content}
                            />
                        </CardContent>
                    </Card>
                    {!this.state.isPublished &&
                    this.state.toggleValue === "tasks" ? (
                        <Grid
                            container
                            spacing={3}
                            sx={{ position: "sticky", bottom: 10 }}
                            justifyContent="center"
                        >
                            <Grid item>
                                <DropdownTasks
                                    onButtonClick={this.addTask}
                                    isPublished={this.state.isPublished}
                                />
                            </Grid>
                            <Grid item>
                                <CustomFab
                                    tooltip="Save Experiment"
                                    size="medium"
                                    color="primary"
                                    label="save"
                                    onClick={this.tryToSave}
                                    icon={<SaveIcon />}
                                />
                            </Grid>
                            <Grid item>
                                <CustomFab
                                    tooltip="Publish Experiment"
                                    size="medium"
                                    color="primary"
                                    label="publish"
                                    onClick={this.setAlertDialogOfPublish}
                                    icon={<ScienceIcon />}
                                />
                            </Grid>
                        </Grid>
                    ) : (
                        <></>
                    )}
                </Container>
                <ScrollToTopButton />
            </div>
        );
    }
}
export default ResearcherGUI;
