import React from "react";
import UploadIcon from "@mui/icons-material/Upload";
import { Card, CardContent, Grid, Link, Typography } from "@mui/material";

import CustomInputButton from "../General/CustomInputButton";

class SurveyComponent extends React.Component {
    render() {
        return (
            <Grid>
                <Grid
                    container
                    direction="row"
                    justifyContent="space-between"
                    alignItems="center"
                >
                    <Typography variant="h5" gutterBottom>
                        Survey Creation
                    </Typography>
                    <CustomInputButton
                        name="upload-json"
                        type="file"
                        tooltip="Upload Survey JSON"
                        size="medium"
                        color={this.props.isPublished ? "warning" : "secondary"}
                        label="upload"
                        variant="contained"
                        component="span"
                        icon={<UploadIcon />}
                        onChange={this.props.uploadSurveyJson}
                        disable={this.props.isPublished}
                    />
                </Grid>
                <Card style={{ margin: "10px" }}>
                    <CardContent>
                        <Grid>
                            <Typography
                                varient="h6"
                                color={
                                    this.props.isPublished
                                        ? "lightgray"
                                        : "black"
                                }
                            >
                                {" "}
                                - By default, the survey contains the following
                                questions:
                                <br />
                                consent, year of birth, gender, neurological
                                disorders and corrected vision.
                                <br />- If you wish to add a customized survey,
                                please create it{" "}
                                <Link
                                    href="https://surveyjs.io/create-survey"
                                    underline="always"
                                    target="_blank"
                                >
                                    here
                                </Link>
                                .<br></br>- When finished, go to "JSON Editor"
                                tab, copy the JSON file and save it locally.
                                <br></br>- Then, upload the JSON file to MOPP by
                                clicking on the Upload button on the right.
                            </Typography>
                        </Grid>
                    </CardContent>
                </Card>
            </Grid>
        );
    }
}

export default SurveyComponent;
