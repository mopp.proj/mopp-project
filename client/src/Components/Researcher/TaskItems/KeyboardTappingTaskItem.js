import React from "react";

import { Grid,Typography } from "@mui/material";

class KeyboardTappingTaskItem extends React.Component{
    render(){
        return(
            <Grid container spacing={3}>
                <Grid item xs={12}>
                <Typography variant="h6" gutterBottom>
                        NOTE: The keyboard tapping task includes set of 3 trials:
                        Both hands, Right hand, Left hand
                    </Typography>
                </Grid>
            </Grid>
        )
    }
}
export default KeyboardTappingTaskItem;