import React from "react";
import { Grid, Typography } from "@mui/material";

import CustomDropdown from "../../General/CustomDropdown";
import CustomTextField from "../../General/CustomTextField";

const Distributions = ["Bimodal", "Gaussian", "Random"];

class LineLengthTaskItem extends React.Component {
    constructor(props) {
        super(props);        
        this.state = {
            taskId: this.props.task.order,
            fields: this.props.task.fields || { distributionType: "" },
            validationFunctions: this.props.task.validationFunctions || {},
        };
    }

    /**
     * This function updates the state of the component as a result of various situations.
     * @param {*} prevProps
     */
    componentDidUpdate(prevProps) {        
        if (prevProps.task.order !== this.state.taskId) {            
            this.setState(
                {
                    fields: this.props.task.fields || { distributionType: "" },
                    validationFunctions: this.props.task.validationFunctions || {},
                },
                () => {
                    this.setTaskFields();
                }
            );
        }
    }

    setTaskFields = () => {
        this.props.setTaskItemFields(this.state);
    };

    setDistributionType = (value, func) => {
        let distributionFields;
        switch (value) {
            case "Bimodal":
                distributionFields = {
                    fields: {
                        distributionType: value,
                        mean1: 0,
                        std1: 0,
                        mean2: 0,
                        std2:0,
                    },
                    validationFunctions: {
                        distributionType: func,
                        mean1: "",
                        std1: "",
                        mean2: "",
                        std2:"",
                    },
                };
                break;
            case "Gaussian":
                distributionFields = {
                    fields: {
                        distributionType: value,
                        mean: 0,
                        std: 0,
                    },
                    validationFunctions: {
                        distributionType: func,
                        mean: "",
                        std: "",
                    },
                };
                break;
            case "Random":
                distributionFields = {
                    fields: {
                        distributionType: value,
                        min: 0,
                        max: 0,
                    },
                    validationFunctions: {
                        distributionType: func,
                        min: "",
                        max: "",
                    },
                };
                break;
            default:
                distributionFields = {
                    fields: { distributionType: value },
                    validationFunctions: { distributionType: func },
                };
        }
        this.setState(
            {
                fields: distributionFields.fields,
                validationFunctions: distributionFields.validationFunctions,
            },
            () => {
                this.setTaskFields();
            }
        );
    };

    setBimodalFirstMean = (value, func) => {
        this.setState(
            (prevState) => ({
                ...prevState,
                fields: {
                    ...prevState.fields,
                    mean1: value,
                },
                validationFunctions: {
                    ...prevState.validationFunctions,
                    mean1: func,
                },
            }),
            () => {
                this.setTaskFields();
            }
        );
    };

    setBimodalSecondMean = (value, func) => {
        this.setState(
            (prevState) => ({
                ...prevState,
                fields: {
                    ...prevState.fields,
                    mean2: value,
                },
                validationFunctions: {
                    ...prevState.validationFunctions,
                    mean2: func,
                },
            }),
            () => {
                this.setTaskFields();
            }
        );
    };

    setBimodalSTD1 = (value, func) => {
        this.setState(
            (prevState) => ({
                ...prevState,
                fields: {
                    ...prevState.fields,
                    std1: value,
                },
                validationFunctions: {
                    ...prevState.validationFunctions,
                    std1: func,
                },
            }),
            () => {
                this.setTaskFields();
            }
        );
    };

    setBimodalSTD2 = (value, func) => {
        this.setState(
            (prevState) => ({
                ...prevState,
                fields: {
                    ...prevState.fields,
                    std2: value,
                },
                validationFunctions: {
                    ...prevState.validationFunctions,
                    std2: func,
                },
            }),
            () => {
                this.setTaskFields();
            }
        );
    };

    setGaussianMean = (value, func) => {
        this.setState(
            (prevState) => ({
                ...prevState,
                fields: {
                    ...prevState.fields,
                    mean: value,
                },
                validationFunctions: {
                    ...prevState.validationFunctions,
                    mean: func,
                },
            }),
            () => {
                this.setTaskFields();
            }
        );
    };

    setGaussianSTD = (value, func) => {
        this.setState(
            (prevState) => ({
                ...prevState,
                fields: {
                    ...prevState.fields,
                    std: value,
                },
                validationFunctions: {
                    ...prevState.validationFunctions,
                    std: func,
                },
            }),
            () => {
                this.setTaskFields();
            }
        );
    };

    setRandomMin = (value, func) => {
        this.setState(
            (prevState) => ({
                ...prevState,
                fields: {
                    ...prevState.fields,
                    min: value,
                },
                validationFunctions: {
                    ...prevState.validationFunctions,
                    min: func,
                },
            }),
            () => {
                this.setTaskFields();
            }
        );
    };

    setRandomMax = (value, func) => {
        this.setState(
            (prevState) => ({
                ...prevState,
                fields: {
                    ...prevState.fields,
                    max: value,
                },
                validationFunctions: {
                    ...prevState.validationFunctions,
                    max: func,
                },
            }),
            () => {
                this.setTaskFields();
            }
        );
    };

    getDistributionFields = () => {
        let distributionFields;
        switch (this.state.fields.distributionType) {
            case "Bimodal":
                distributionFields = (
                    <Grid 
                        container
                        direction="row"
                        justifyContent="flex-start"
                        alignItems="center"
                        spacing={3}
                    >
                        <Grid item>
                            <CustomTextField
                                id="mean1"
                                label="First Mean"
                                validation="positiveFloat"
                                onBlur={this.setBimodalFirstMean}
                                value={this.state.fields.mean1}
                                disabled={this.props.isPublished}
                            />
                        </Grid>
                        <Grid item>
                            <CustomTextField
                                id="std1_bm"
                                label="First Standard Deviation"
                                validation="positiveFloat"
                                onBlur={this.setBimodalSTD1}
                                value={this.state.fields.std1}
                                disabled={this.props.isPublished}
                            />
                        </Grid>
                        <Grid item>
                            <CustomTextField
                                id="mean2"
                                label="Second Mean"
                                validation="positiveFloat"
                                onBlur={this.setBimodalSecondMean}
                                value={this.state.fields.mean2}
                                disabled={this.props.isPublished}
                            />
                        </Grid>
                        <Grid item>
                            <CustomTextField
                                id="std2_bm"
                                label="Second Standard Deviation"
                                validation="positiveFloat"
                                onBlur={this.setBimodalSTD2}
                                value={this.state.fields.std2}
                                disabled={this.props.isPublished}
                            />
                        </Grid>
                    </Grid>
                );
                break;
            case "Gaussian":
                distributionFields = (
                    <Grid 
                        container
                        direction="row"
                        justifyContent="flex-start"
                        alignItems="center"
                        spacing={3}
                    >
                        <Grid item>
                            <CustomTextField
                                id="mean"
                                label="Mean"
                                validation="positiveFloat"
                                onBlur={this.setGaussianMean}
                                value={this.state.fields.mean}
                                disabled={this.props.isPublished}
                            />
                        </Grid>
                        <Grid item>
                            <CustomTextField
                                id="std"
                                label="Standard Deviation"
                                validation="positiveFloat"
                                onBlur={this.setGaussianSTD}
                                value={this.state.fields.std}
                                disabled={this.props.isPublished}
                            />
                        </Grid>
                    </Grid>
                );
                break;
            case "Random":
                distributionFields = (
                    <Grid
                        container
                        direction="row"
                        justifyContent="flex-start"
                        alignItems="center"
                        spacing={3}
                    >
                        <Grid item>
                            <CustomTextField
                                id="min"
                                label="Min"
                                validation="positiveInteger"
                                onBlur={this.setRandomMin}
                                value={this.state.fields.min}
                                disabled={this.props.isPublished}
                            />
                        </Grid>
                        <Grid item>
                            <CustomTextField
                                id="max"
                                label="Max"
                                validation="positiveInteger"
                                onBlur={this.setRandomMax}
                                value={this.state.fields.max}
                                disabled={this.props.isPublished}
                            />
                        </Grid>
                    </Grid>
                );
                break;
            default:
                break;
        }
        return distributionFields;
    };

    render() {   
        return (
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <Typography variant="h6" gutterBottom>
                        Line-Length Task Fields
                    </Typography>
                </Grid>
                <Grid item>
                    <CustomDropdown
                        options={Distributions}
                        label="Distribution Type"
                        onChange={this.setDistributionType}
                        value={this.state.fields.distributionType}
                        disabled={this.props.isPublished}
                    />
                </Grid>
                <Grid item>{this.getDistributionFields()}</Grid>
            </Grid>
        );
    }
}
export default LineLengthTaskItem;
