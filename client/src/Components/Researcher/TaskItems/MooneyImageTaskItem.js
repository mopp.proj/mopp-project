import React from "react";
import { Grid, Typography } from "@mui/material";
import CustomDropdown from "../../General/CustomDropdown";

const Difficulties = ["Easy", "Medium", "Hard"];

class MooneyImageTaskItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            taskId: this.props.task.order,
            fields: this.props.task.fields || { difficulty: "" },
            validationFunctions: this.props.task.validationFunctions || {},
        };
    }

    componentDidUpdate(prevProps) {
        if (prevProps.task.order !== this.state.taskId) {
            this.setState(
                {
                    fields: this.props.task.fields || { difficulty: "" },
                },
                () => {
                    this.setTaskFields();
                }
            );
        }
    }

    setTaskFields = () => {
        this.props.setTaskItemFields(this.state);
    };

    setDifficulty = (value, func) => {
        this.setState(
            {
                fields: { difficulty: value },
                validationFunctions: { difficulty: func },
            },
            () => {
                this.setTaskFields();
            }
        );
    };

    render() {
        return (
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <Typography variant="h6" gutterBottom>
                        Mooney Image Task Fields
                    </Typography>
                </Grid>
                <Grid item>
                    <CustomDropdown
                        options={Difficulties}
                        label="Difficulty"
                        onChange={this.setDifficulty}
                        value={this.state.fields.difficulty}
                        disabled={this.props.isPublished}
                    />
                </Grid>
            </Grid>
        );
    }
}
export default MooneyImageTaskItem;
