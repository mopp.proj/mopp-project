import React from "react";
import { Grid, Typography } from "@mui/material";

import CustomDropdown from "../../General/CustomDropdown";
import CustomTextField from "../../General/CustomTextField";

const RdkTypes = [
    "1 - Same && Random Position", 
    "2 - Same && Random Walk", 
    "3 - Same && Random Direction",
    "4 - Different && Random Position",
    "5 - Different && Random Walk", 
    "6 - Different && Random Direction",
];

const ApertureTypes = [
    "1 - Circle",
    "2 - Ellipse",
    "3 - Square",
    "4 - Rectangle",
];

class RdkTaskItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            taskId: this.props.task.order,
            fields: this.props.task.fields || { rdkType: "", apertureType: "", numOfDots: "", coherence: "", },
            validationFunctions: this.props.task.validationFunctions || {},
        };
    }

    componentDidUpdate(prevProps) {
        if (prevProps.task.order !== this.state.taskId) {
            this.setState(
                {
                    fields: this.props.task.fields || { rdkType: "", apertureType: "", numOfDots: "", coherence: "", },
                },
                () => {
                    this.setTaskFields();
                }
            );
        }
    }

    setTaskFields = () => {
        this.props.setTaskItemFields(this.state);
    };

    setRdkType = (value, func) => {
        this.setState((prevState) =>
            {
                return {
                    fields: { 
                        ...prevState.fields,
                        rdkType: value 
                    },
                    validationFunctions: { 
                        ...prevState.validationFunctions,
                        rdkType: func 
                    },
                }
            },
            () => {
                this.setTaskFields();
            }
        );
    };

    setApertureType = (value, func) => {
        this.setState((prevState) =>
            {
                return {
                    fields: { 
                        ...prevState.fields,
                        apertureType: value
                    },
                    validationFunctions: { 
                        ...prevState.validationFunctions,
                        apertureType: func 
                    },
                }
            },
            () => {
                this.setTaskFields();
            }
        );
    };
    
    setNumOfDots = (value, func) => {
        this.setState((prevState) =>
            {
                return {
                    fields: { 
                        ...prevState.fields,
                        numOfDots: value 
                    },
                    validationFunctions: { 
                        ...prevState.validationFunctions,
                        numOfDots: func 
                    },
                }
            },
            () => {
                this.setTaskFields();
            }
        );
    };

    setCoherence = (value, func) => {
        this.setState((prevState) =>
            {
                return {
                    fields: { 
                        ...prevState.fields,
                        coherence: value 
                    },
                    validationFunctions: { 
                        ...prevState.validationFunctions,
                        coherence: func 
                    },
                }
            },
            () => {
                this.setTaskFields();
            }
        );
    };    

    render() {
        return (
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <Typography variant="h6" gutterBottom>
                        RDK Task Fields
                    </Typography>
                </Grid>
                <Grid item>
                    <CustomDropdown
                        options={RdkTypes}
                        label="RDK Type"
                        onChange={this.setRdkType}
                        value={this.state.fields.rdkType}
                        disabled={this.props.isPublished}
                    />                    
                </Grid>
                <Grid item>
                    <CustomDropdown
                        options={ApertureTypes}
                        label="Aperture Type"
                        onChange={this.setApertureType}
                        value={this.state.fields.apertureType}
                        disabled={this.props.isPublished}
                    />                    
                </Grid>
                <Grid item>
                    <CustomTextField
                        id="numOfDots"
                        label="Number of Dots"
                        validation="positiveInteger"
                        onBlur={this.setNumOfDots}
                        value={this.state.fields.numOfDots}
                        disabled={this.props.isPublished}
                    />
                </Grid>
                <Grid item>
                    <CustomTextField
                        id="coherence"
                        label="Coherence"
                        validation="floatRange0to1"
                        onBlur={this.setCoherence}
                        value={this.state.fields.coherence}
                        disabled={this.props.isPublished}
                    />
                </Grid>
            </Grid>
        );
    }
}
export default RdkTaskItem;
