import React from "react";

import DeleteIcon from '@mui/icons-material/Delete';
import { Accordion, AccordionDetails, AccordionSummary, Grid, Typography } from "@mui/material";
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

import TaskItem from "./TaskItem";
import CustomFab from "../../General/CustomFab";
import CustomAlertDialog from "../../General/CustomAlertDialog";


class TaskCard extends React.Component {
    /**
     * CTOR of TaskCard.
     * @param {*} props 
     */
    constructor(props) {
        super(props);
        this.state = {
            isOpen: this.props.expandTask,
            alertDialog: {
                isOpen: false,
                onAgree: undefined,
                title: '',
                content: '',
            }
        }
    }

    componentDidUpdate = (prevProps) => {                
        if (prevProps.expandTask !== this.props.expandTask && this.state.isOpen !== this.props.expandTask) {            
            this.setState({
                isOpen: this.props.expandTask,
            });
        }        
    }

    /**
     * This function gets the fields of the task from TaskItem Component, 
     * and passes it to DraggableTasks Component.
     * @param {*} taskFields 
     */
    setTaskCardFields = (taskFields) => {    
        this.props.setDraggableTaskFields(taskFields);
    }

    setAlertDialog = (isOpen, onAgree, title, content) => {
        this.setState({
            alertDialog: {
                isOpen: isOpen,
                onAgree: onAgree,
                title: title,
                content: content,
            }
        });
    }
    
    closeAlertDialog = () => {
        this.setAlertDialog(false, undefined, '', '');
    }

    setAlertDialogOfRemove = () => {                
        this.setAlertDialog(
            true, 
            this.agreeToRemove,
            'Are you sure you want to remove this task?',
            'Removing a task is an irreversible action',
        );      
    }

    agreeToRemove = () => {        
        this.setState({
            alertDialog: {
                isOpen: false,
                onAgree: undefined,
                title: '',
                content: '',
            }
        }, () => {
            this.removeTask();
        })
    }

    /**
     * This function sends to DraggableTasks Component the id (order) of the task
     * which should be removed from the array of tasks.
     */
    removeTask = () => {
        this.props.removeTaskFromDraggableTasks(this.props.task.order);
    }

    openCloseAccordion = () => {        
        this.setState({
            isOpen: !this.state.isOpen,
        });        
    }

    render() {
        const { provided, innerRef } = this.props;        
        return (
            <div {...provided.draggableProps} {...provided.dragHandleProps} ref={innerRef}>
                <Accordion style={{ margin: "10px" }} expanded={this.state.isOpen}>
                    <AccordionSummary
                        expandIcon={<ExpandMoreIcon />}
                        onClick={this.openCloseAccordion}
                    >
                        <Grid container direction="row" justifyContent="space-between" alignItems="flex-end">
                            <Grid item>
                                <Typography variant="h5" gutterBottom>                        
                                    {`${this.props.task.order + 1} - ${this.props.task.type}`}
                                </Typography>
                            </Grid>
                            {this.props.isPublished ? 
                                <></>
                                :
                                <Grid item alignItems="right" style={{marginRight:"10px"}}>
                                    <CustomFab 
                                        tooltip="Remove Task" 
                                        size="small" 
                                        color="secondary" 
                                        label="remove" 
                                        onClick={this.setAlertDialogOfRemove}
                                        icon={<DeleteIcon />}
                                    />                                
                                </Grid>
                            }
                        </Grid>
                    </AccordionSummary>
                    <AccordionDetails>
                        <TaskItem 
                            task={this.props.task}
                            isPublished={this.props.isPublished}
                            setTaskCardFields={this.setTaskCardFields} 
                        />
                    </AccordionDetails>
                </Accordion>                
                <CustomAlertDialog 
                    isOpen={this.state.alertDialog.isOpen}
                    onAgree={this.state.alertDialog.onAgree}
                    onDisagree={this.closeAlertDialog}
                    title={this.state.alertDialog.title}
                    content={this.state.alertDialog.content}
                />
            </div>
        );
    }
}
export default TaskCard;
