﻿import React from "react";
import {
    Grid,
    InputAdornment,
    Typography,
    Checkbox,
    FormControlLabel,
    FormGroup,
} from "@mui/material";

import NumerosityTaskItem from "./NumerosityTaskItem";
import CustomTextField from "../../General/CustomTextField";
import LineLengthTaskItem from "./LineLengthTaskItem";
import BiologicalMotionTaskItem from "./BiologicalMotionTaskItem";
import MooneyImageTaskItem from "./MooneyImageTaskItem";
import KeyboardTappingTaskItem from "./KeyboardTappingTaskItem";
import RdkTaskItem from "./RdkTaskItem";
import RecentPriorNormalTaskItem from "./RecentPriorNormalTaskItem";
import RecentPriorDistortedTaskItem from "./RecentPriorDistortedTaskItem";

const defaultValues = {
    Numerosity: {
        description:
            "Please estimate, without counting,\nthe number of object in the following pictures.",
        trialNum: "24",
        stimulusDuration: "0.25",
        responseDuration: "3",
        practiceTrial: false,
    },
    "Line Length": {
        description:
            "If this line is 1 unit: _ , without measuring (just by viewing),\nguess how long the following lines are: (how many units?)",
        trialNum: "24",
        stimulusDuration: "0.25",
        responseDuration: "3",
        practiceTrial: false,
    },
    "Biological Motion": {
        description:
            "In the following session,\ndetermine whether you see a person moving or scrambled moving dots.",
        trialNum: "24",
        stimulusDuration: "3",
        responseDuration: "0",
        practiceTrial: false,
    },
    "Mooney Image": {
        description:
            "In the following scene,\ndetermine whether you see a person (in part or in whole, select one option)",
        trialNum: "24",
        stimulusDuration: "0.8",
        responseDuration: "0",
        practiceTrial: false,
    },
    "Keyboard Tapping": {
        description:
            "Using the index finger of both your hands, please press the “s” and “k” keys for 30 seconds\n(using your left hand, your right hand or both hands, as will be instructed)",
        trialNum: "3",
        stimulusDuration: "5",
        responseDuration: "30",
        practiceTrial: false,
    },
    "RDK": {
        description:
            "In the following scene,\ndetermine whether the dots are moving left or right",
        trialNum: "5",
        stimulusDuration: "3",
        responseDuration: "5",
        practiceTrial: false,
    },
    "Recent Prior Normal": {
        description:
            "If the tower were to fall on its own,\nwill more blocks fall on the plain gray tiles\nor more on the tiles with design?\n(Select one option)",
        trialNum: "1",
        stimulusDuration: "5",
        responseDuration: "5",
        practiceTrial: false,
    },
    "Recent Prior Distorted": {
        description:
            "Do you see a person, in part or in whole, in this scene?\n(Select one option)",
        trialNum: "1",
        stimulusDuration: "5",
        responseDuration: "5",
        practiceTrial: false,
    },
};

class TaskItem extends React.Component {
    /**
     * CTOR of TaskItem.
     * @param {*} props
     */
    constructor(props) {
        super(props);
        this.state = {
            taskId: this.props.task.order,
            generalFields:
                this.props.task.generalFields ||
                defaultValues[this.props.task.type],
            fields: this.props.task.fields || {},
            generalFieldsValidationFunctions: this.props.task
                .generalFieldsValidationFunctions || {
                description: "",
                stimulusDuration: "",
                responseDuration: "",
                trialNum: "",
            },
            fieldsValidationFunctions:
                this.props.task.fieldsValidationFunctions || {},
        };
    }

    /**
     * This function updates the state of the component as a result of various situations.
     * @param {*} prevProps
     */
    componentDidUpdate(prevProps) {
        // if task's order was changed before saving
        if (prevProps.task.order !== this.state.taskId) {
            this.setState({
                generalFields:
                    this.props.task.generalFields ||
                    defaultValues[this.props.task.type],
                fields: this.props.task.fields || {},
                generalFieldsValidationFunctions: this.props.task
                    .generalFieldsValidationFunctions || {
                    description: "",
                    stimulusDuration: "",
                    responseDuration: "",
                    trialNum: "",
                },
                fieldsValidationFunctions:
                    this.props.task.fieldsValidationFunctions || {},
            });
        }
    }

    /**
     * This function gets the task fields from a TaskComponent,
     * updates the state of the current component and sends it to TaskCard Component.
     * @param {*} taskFields
     */
    setTaskItemFields = (taskFields) => {
        this.setState(
            (prevState) => ({
                ...prevState,
                fields: taskFields.fields,
                fieldsValidationFunctions: taskFields.validationFunctions,
            }),
            () => {
                this.setTaskCardFields();
            }
        );
    };

    /**
     * This function sends the task fields to TaskCard Component.
     */
    setTaskCardFields = () => {
        this.props.setTaskCardFields(this.state);
    };

    setDescription = (value, func) => {
        this.setState(
            (prevState) => ({
                generalFields: {
                    ...prevState.generalFields,
                    description: value,
                },
                generalFieldsValidationFunctions: {
                    ...prevState.generalFieldsValidationFunctions,
                    description: func,
                },
            }),
            () => {
                this.setTaskCardFields();
            }
        );
    };

    /**
     * Setter of stimulusDuration
     * @param {*} event
     */
    setStimulusDuration = (value, func) => {
        this.setState(
            (prevState) => ({
                generalFields: {
                    ...prevState.generalFields,
                    stimulusDuration: value,
                },
                generalFieldsValidationFunctions: {
                    ...prevState.generalFieldsValidationFunctions,
                    stimulusDuration: func,
                },
            }),
            () => {
                this.setTaskCardFields();
            }
        );
    };

    /**
     * Setter of responseDuration
     * @param {*} event
     */
    setResponseDuration = (value, func) => {
        this.setState(
            (prevState) => ({
                generalFields: {
                    ...prevState.generalFields,
                    responseDuration: value,
                },
                generalFieldsValidationFunctions: {
                    ...prevState.generalFieldsValidationFunctions,
                    responseDuration: func,
                },
            }),
            () => {
                this.setTaskCardFields();
            }
        );
    };

    /**
     * Setter of practiceTrial
     * @param {*} event
     */
    setPracticeTrial = (event) => {
        this.setState(
            (prevState) => ({
                generalFields: {
                    ...prevState.generalFields,
                    practiceTrial: event.target.checked,
                },
            }),
            () => {
                this.setTaskCardFields();
            }
        );
    };

    /**
     * Setter of trialNum
     * @param {*} event
     */
    setTrialNum = (value, func) => {
        this.setState(
            (prevState) => ({
                generalFields: {
                    ...prevState.generalFields,
                    trialNum: value,
                },
                generalFieldsValidationFunctions: {
                    ...prevState.generalFieldsValidationFunctions,
                    trialNum: func,
                },
            }),
            () => {
                this.setTaskCardFields();
            }
        );
    };

    /**
     * This function returns a TaskItem Component as a result of task type.
     * @param {*} task
     * @returns a TaskItem Component
     */
    getTaskComponent = (task) => {
        let taskComponent;
        switch (task.type) {
            case "Numerosity":
                taskComponent = (
                    <NumerosityTaskItem
                        setTaskItemFields={this.setTaskItemFields}
                        task={task}
                        isPublished={this.props.isPublished}
                    />
                );
                break;
            case "Line Length":
                taskComponent = (
                    <LineLengthTaskItem
                        setTaskItemFields={this.setTaskItemFields}
                        task={task}
                        isPublished={this.props.isPublished}
                    />
                );
                break;
            case "Biological Motion":
                taskComponent = (
                    <BiologicalMotionTaskItem
                        setTaskItemFields={this.setTaskItemFields}
                        task={task}
                        isPublished={this.props.isPublished}
                    />
                );
                break;
            case "Mooney Image":
                taskComponent = (
                    <MooneyImageTaskItem
                        setTaskItemFields={this.setTaskItemFields}
                        task={task}
                        isPublished={this.props.isPublished}
                    />
                );
                break;
            case "Keyboard Tapping":
                taskComponent = <KeyboardTappingTaskItem />;
                break;
            case "RDK":
                taskComponent = (
                    <RdkTaskItem
                        setTaskItemFields={this.setTaskItemFields}
                        task={task}
                        isPublished={this.props.isPublished}
                    />
                );
                break;
            case "Recent Prior Normal":
                taskComponent = (
                    <RecentPriorNormalTaskItem
                        setTaskItemFields={this.setTaskItemFields}
                        task={task}
                        isPublished={this.props.isPublished}
                    />
                );
                break;
            case "Recent Prior Distorted":
                taskComponent = (
                    <RecentPriorDistortedTaskItem
                        setTaskItemFields={this.setTaskItemFields}
                        task={task}
                        isPublished={this.props.isPublished}
                    />
                );
                break;
            default:
                break;
        }
        return taskComponent;
    };

    render() {
        return (
            <Grid
                container
                direction="row"
                justifyContent="flex-start"
                alignItems="center"
                spacing={3}
            >
                <Grid item xs={12}>
                    <Typography variant="h6" gutterBottom>
                        General Fields
                    </Typography>
                </Grid>
                <Grid item xs={12}>
                    <CustomTextField
                        id="description"
                        label="Description"
                        multiline
                        fullWidth
                        rows={4}
                        validation="nonEmptyString"
                        onBlur={this.setDescription}
                        value={this.state.generalFields.description}
                        disabled={this.props.isPublished}
                    />
                </Grid>
                <Grid item>
                    <CustomTextField
                        id="trial_num"
                        label="Number of Trials"
                        validation="positiveInteger"
                        onBlur={this.setTrialNum}
                        value={this.state.generalFields.trialNum}
                        disabled={this.props.isPublished || (this.props.task.type === "Keyboard Tapping")}
                    />
                </Grid>
                <Grid item>
                    <CustomTextField
                        id="stim_duration"
                        label="Stimulus Duration"
                        InputProps={{
                            endAdornment: (
                                <InputAdornment position="end">
                                    sec
                                </InputAdornment>
                            ),
                        }}
                        validation="positiveFloat"
                        onBlur={this.setStimulusDuration}
                        value={this.state.generalFields.stimulusDuration}
                        disabled={this.props.isPublished}
                    />
                </Grid>
                <Grid item>
                    <CustomTextField
                        id="resp_duration"
                        label="Response Duration"
                        InputProps={{
                            endAdornment: (
                                <InputAdornment position="end">
                                    sec
                                </InputAdornment>
                            ),
                        }}
                        validation="positiveFloat"
                        onBlur={this.setResponseDuration}
                        value={this.state.generalFields.responseDuration}
                        disabled={this.props.isPublished}
                    />
                </Grid>
                <Grid item>
                    <FormGroup>
                        <FormControlLabel
                            control={<Checkbox />}
                            label="Add Practice Trial"
                            onChange={this.setPracticeTrial}
                            checked={this.state.generalFields.practiceTrial}
                            disabled={this.props.isPublished}
                        />
                    </FormGroup>
                </Grid>

                <Grid item>{this.getTaskComponent(this.props.task)}</Grid>
            </Grid>
        );
    }
}
export default TaskItem;
