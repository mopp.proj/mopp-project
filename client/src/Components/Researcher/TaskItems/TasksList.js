import React from "react";

class TasksList extends React.Component {
    /**
     * A class which renders the list of tasks which is shown in ResercherGUI Component.
     * @returns 
     */
    render() {
        const { provided, innerRef, children } = this.props;
        return (
            <div {...provided.droppableProps} ref={innerRef}>
                {children}
            </div>
        );
    }
}
export default TasksList;
