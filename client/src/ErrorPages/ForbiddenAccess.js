import React from "react";

import { Button, Typography } from "@mui/material";
import { redirectToHomePage } from "../Utils/Links.js";

import "./Assets/ForbiddenAccess.css";

class ForbiddenAccess extends React.Component {
    componentDidMount() {
        this.props.setAppbar(true, false);        
    }
    
    render() {        
        return (
            <div>                                       
                <div className="container">
                    <h1>4
                        <div className="lock">
                            <div className="top" />                            
                            <div className="bottom" />                            
                        </div>
                        3
                    </h1>
                    <p>Access denied</p>
                </div>
                <Button
                    variant="contained"
                    onClick={redirectToHomePage}
                    style={{                        
                        position: "absolute",
                        left: "50%",
                        top: "70%",
                        transform: "translate(-50%,-73%)",
                    }}
                >
                    <Typography
                        variant="h6"
                        color="inherit" 
                        component="div"
                    >
                        Go to Home
                    </Typography>                    
                </Button>
            </div>
        );
    }
}

export default ForbiddenAccess;