import React from "react";

import { Button, Typography } from "@mui/material";
import { PASS_ITEM, redirectToHomePage } from "../Utils/Links";


import "./Assets/InternalServerError.css";

class InternalServerError extends React.Component {
    componentDidMount() {
        this.props.setAppbar(true, this.props.cookies.get(PASS_ITEM));
    }

    render() {        
        return (
            <div>            
            <div id="serverError">
                <div className="serverError">
                    <div className="serverError-500">
                        <h1>Oops!</h1>
                        <h2>500 - Our server is on a break</h2>
                    </div>                    
                </div>
            </div>
            <Button
            variant="contained"
            onClick={redirectToHomePage}
            style={{                        
                position: "absolute",
                left: "50%",
                top: "70%",
                transform: "translate(-50%,-75%)",
            }}
        >
            <Typography
                variant="h6"
                color="inherit" 
                component="div"
            >
                Go to Home
            </Typography>                    
        </Button>
        </div>
        );
    }
}

export default InternalServerError;