import React from "react";

import { Typography } from "@mui/material";

import moppLogo from "../Utils/Assets/logo.png";

class MobileErrorPage extends React.Component {
    render() {
        return (
            <div>                                       
                <Typography 
                    variant="h6" 
                    color="inherit" 
                    component="div" 
                    style={{
                        margin: "auto",
                        textAlign: "center",
                        paddingTop: "50%",
                    }}
                >
                    Sorry,
                    <br/>
                    MOPP does not support mobile..
                    <br/>
                    Please connect from tablet or computer
                    <br/>
                    <img
                        alt=""
                        src={moppLogo}
                        height="60"
                    />
                </Typography>                                
            </div>
        );
    }
}

export default MobileErrorPage;