import React from "react";

import { Button, Typography } from "@mui/material";
import { PASS_ITEM, redirectToHomePage } from "../Utils/Links";

import PageNotFoundImage from './Assets/404.jpg';

class PageNotFound extends React.Component {
    componentDidMount() {
        this.props.setAppbar(true, this.props.cookies.get(PASS_ITEM));        
    }
    
    render() {        
        return (
            <div>
                <img 
                    src={PageNotFoundImage} 
                    className="PageNotFound" 
                    alt={"Page-Not-Found"}
                    height={400}
                    width={400}
                    style={{                        
                        paddingTop: "5%",
                        margin: "auto",
                        display: "block",
                    }}
                />
                <Button 
                    variant="contained"
                    onClick={redirectToHomePage}
                    style={{
                        width: "170px",                        
                        margin: "auto",
                        display: "block",
                    }}
                >
                    <Typography
                        variant="h6"
                        color="inherit" 
                        component="div"
                    >
                        Go to Home
                    </Typography>                    
                </Button>
            </div>
        );
    }
}

export default PageNotFound;