import React from "react";
import axios from "axios";

import { Grid, InputAdornment, Typography } from "@mui/material";
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";

import CustomFab from "../src/Components/General/CustomFab";
import CustomSnackbar from "../src/Components/General/CustomSnackbar";
import CustomTextField from "../src/Components/General/CustomTextField";

import {
    ExperimentURL,    
    ParticipantURL,
    PASS_ITEM,
    redirectToAuthPage,
    redirectToExperimentPage,
} from "./Utils/Links";

class Homepage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            experimentID: "",
            password: "",
            notExists: false,
            error: "",            
        };
    }

    componentDidMount() {        
        this.props.setAppbar(true, this.props.cookies.get(PASS_ITEM));
    }

    setExperimentID = (value) => {
        this.setState({ experimentID: value });
    };

    setPassword = (value) => {
        this.setState({ password: value });
    };

    validateExperimentID = () => {
        axios
            .get(`${ParticipantURL}/${this.state.experimentID}`)
            .then((res) => {
                redirectToAuthPage(this.state.experimentID);                
            })
            .catch((err) => {
                this.setState({
                    notExists: true,
                    error: "Invalid Experiment ID",
                });
            });
    };

    validatePassword = () => {
        axios
            .get(`${ExperimentURL}/validatePassword`, {
                params: {
                    password: this.state.password,
                },
            })
            .then((res) => {                
                this.props.cookies.set(PASS_ITEM, true, { path: "/" });
                this.props.setAppbar(true, true);                
                redirectToExperimentPage();
            })
            .catch((err) => {                
                this.setState({
                    notExists: true,
                    error: "Invalid Password",
                });                
                this.props.cookies.remove(PASS_ITEM);                
                this.props.setAppbar(true, false);
            });
    };

    setThenValidatePassword = (value) => {
        this.setState({ password: value }, () => {
            this.validatePassword();
        });
    };

    setThenValidateExperimentID = (value) => {
        this.setState({ experimentID: value }, () => {
            this.validateExperimentID();
        });
    };

    /**
     * This function hides the alert when the "X" button is pressed
     */
    closeSnackBar = () => {
        this.setState({
            notExists: false,
            error: "",
        });
    };

    render() {
        return (
            <div>
                <Typography
                    variant="h3"
                    gutterBottom
                    style={{
                        margin: "auto",
                        textAlign: "center",
                        paddingTop: "5%",
                    }}
                >
                    Welcome to MOPP!
                </Typography>
                <Grid
                    container
                    spacing={2}
                    direction="row"
                    justifyContent="center"
                    alignItems="center"
                    style={{
                        paddingTop: "5%",
                    }}
                >
                    <Grid item xs={6}>
                        <Grid
                            container
                            spacing={2}
                            direction="column"
                            justifyContent="center"
                            alignItems="center"
                        >
                            <Typography
                                variant="h6"
                                gutterBottom
                                style={{
                                    margin: "auto",
                                    textAlign: "center",
                                }}
                            >
                                Participants Entrance
                            </Typography>
                            <CustomTextField
                                label="Experiment ID"
                                value={this.state.experimentID}
                                onBlur={this.setExperimentID}
                                onKeyDown={this.setThenValidateExperimentID}
                                sx={{ width: 300 }}
                                InputProps={{
                                    endAdornment: (
                                        <InputAdornment position="end">
                                            <CustomFab
                                                tooltip="Enter Experiment ID"
                                                size="small"
                                                color="primary"
                                                label="experimentID"
                                                onClick={
                                                    this.validateExperimentID
                                                }
                                                icon={<ArrowForwardIcon />}
                                            />
                                        </InputAdornment>
                                    ),
                                }}
                            />
                        </Grid>
                    </Grid>
                    <Grid item xs={6}>
                        <Grid
                            container
                            spacing={2}
                            direction="column"
                            justifyContent="center"
                            alignItems="center"
                        >
                            <Typography
                                variant="h6"
                                gutterBottom
                                style={{
                                    margin: "auto",
                                    textAlign: "center",
                                }}
                            >
                                Researchers Entrance
                            </Typography>
                            <CustomTextField
                                label="Password"
                                value={this.state.password}
                                onBlur={this.setPassword}
                                onKeyDown={this.setThenValidatePassword}
                                sx={{ width: 300 }}
                                InputProps={{
                                    endAdornment: (
                                        <InputAdornment position="end">
                                            <CustomFab
                                                tooltip="Enter Password"
                                                size="small"
                                                color="primary"
                                                label="password"
                                                onClick={this.validatePassword}
                                                icon={<ArrowForwardIcon />}
                                            />
                                        </InputAdornment>
                                    ),
                                }}
                            />
                        </Grid>
                    </Grid>
                </Grid>
                <CustomSnackbar
                    open={this.state.notExists}
                    severity={"error"}
                    content={this.state.error}
                    onClose={this.closeSnackBar}
                />
            </div>
        );
    }
}

export default Homepage;
