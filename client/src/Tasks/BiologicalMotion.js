import React from "react";
import { AssetsURL } from "../Utils/Links";

// A fixed size for all gifs, based on the person BML Gifs
const gifSize = {
    width: 675,
    height: 678,
};

class BiologicalMotion extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            scaleFactor: this.props.virtualChinrestData.scale_factor,
        };
    }
    render() {
        // The server's API that fetches the BML gifs
        let url =
            AssetsURL +
            "/BMLGif/" +
            this.props.trialData.difficulty +
            "/" +
            this.props.trialData.gifName;
        return (            
            <div
                style={{
                    position: "fixed",
                    left: "50%",
                    top: "50%",
                    WebkitTransform: "translate(-50%, -50%)",
                    transform: "translate(-50%, -50%)",
                }}
            >            
                <img
                    width={gifSize.width * this.state.scaleFactor}
                    height={gifSize.height * this.state.scaleFactor}
                    src={url}
                    alt={this.props.trialData.gifName}                
                />
            </div>
        );
    }
}
export default BiologicalMotion;
