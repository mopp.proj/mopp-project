import React from "react";
import { Typography, Grid } from "@mui/material";

class KeyboardTapping extends React.Component {
    render() {
        var hand = this.props.hand;
        hand = hand.toLowerCase();
        var beforeText = this.props.isPractice
            ? `Type sk exactly 3 times with`
            : "Press keys 's' and 'k' repeatedly with";
        if (hand !== "both") {
            beforeText += " your";
        }
        var afterText = hand !== "both" ? "hand" : "hands";
        return (
            <Grid item xs={12}>
                <Typography
                    variant="h4"
                    gutterBottom
                    style={{                        
                        paddingTop: "40%",
                        margin: "auto",
                        textAlign: "center",
                    }}
                >
                    {beforeText} <b>{hand}</b> {afterText}
                </Typography>
            </Grid>
        );
    }
}
export default KeyboardTapping;
