import { Typography } from "@mui/material";
import React from "react";

class LineLength extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            lineLength: undefined,
            scaleFactor: this.props.percentage || 1,
        };
    }

    componentDidMount() {
        this.setState({
            lineLength: "_".repeat(this.props.lineLength),
        });
    }

    componentDidUpdate(prevProps) {
        if (prevProps.idx !== this.props.idx) {
            this.setState({
                lineLength: "_".repeat(this.props.lineLength),
            });
        }
    }

    render() {
        return (
            <div
                style={{
                    position: "fixed",
                    left: "50%",
                    top: "50%",
                    WebkitTransform: "translate(-50%, -50%)",
                    transform: "translate(-50%, -50%)",
                    margin: "auto",
                    textAlign: "center",
                }}
            >
                <Typography
                    variant="h4"
                    gutterBottom                    
                >
                    1 unit: _
                </Typography>
                <Typography
                    variant="h4"
                    gutterBottom                    
                >
                    What is the size of the following line?
                </Typography>
                <Typography
                    variant="h4"
                    gutterBottom                    
                >
                    {this.state.lineLength}
                </Typography>
            </div>
        );
    }
}

export default LineLength;
