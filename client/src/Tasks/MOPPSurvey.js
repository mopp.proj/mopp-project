import React from "react";
import axios from "axios";

import { Grid } from "@mui/material";

import "survey-react/modern.min.css";
import { Survey, StylesManager, Model } from "survey-react";

import "jspsych/css/jspsych.css";
import {
    STAGES,    
    ParticipantURL,    
    STAGE_ITEM,
    redirectCorrectPage,
    redirectToCorrectErrorPage,
} from "../Utils/Links";
StylesManager.applyTheme("modern");

class MOPPSurvey extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            participantsUrlID: this.props.match.params.id,
            participantID: this.props.match.params.participantID,
            surveyResults: undefined,
            surveyJson: undefined,
            survey: undefined,
        };
    }

    componentDidMount() {        
        // check that the given participantID exists with given participantsUrlID
        axios
            .get(
                `${ParticipantURL}/${this.state.participantsUrlID}/${this.state.participantID}`
            )
            .then((res) => {                
                if (!res.data.name) {
                    redirectToCorrectErrorPage(res.status);
                } else {
                    this.setState({
                        surveyJson: res.data.surveyJson,
                        survey: res.data.surveyJson
                            ? new Model(res.data.surveyJson)
                            : undefined,
                    }, () => {
                        this.props.setAppbar(false, false);
                    });
                }
            })
            .catch((err) => {                
                redirectToCorrectErrorPage(err.response.status);
            });

        redirectCorrectPage(
            STAGES.SURVEY,
            this.state.participantsUrlID,
            this.state.participantID,
            this.props.cookies,
        );
    }

    componentDidUpdate() {
        if (this.state.survey !== undefined) {
            this.state.survey.onComplete.add((sender) => {
                this.setState({
                    surveyResults: sender.data,
                });
                this.setSurveyResults(sender.data);
            });
        }
    }

    setSurveyResults = (surveyResults) => {
        axios
            .put(
                `${ParticipantURL}/${this.state.participantsUrlID}/${
                    this.state.participantID                
                }/${this.props.cookies.get(STAGE_ITEM)}`,
                surveyResults
            )
            .then((res) => {
                // redirect to Virtual Chinrest page
                this.props.cookies.set(STAGE_ITEM, STAGES.VC_START, { path: "/" });                
                redirectCorrectPage(
                    STAGES.SURVEY,
                    this.state.participantsUrlID,
                    this.state.participantID,
                    this.props.cookies,
                );
            })
            .catch((err) => {                
                redirectToCorrectErrorPage(err.response.status);
            });
    };

    render() {
        return (
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    {this.state.survey !== undefined ? (
                        <Survey model={this.state.survey} />
                    ) : (
                        <></>
                    )}
                </Grid>
            </Grid>
        );
    }
}

export default MOPPSurvey;
