import React from "react";
import { ImagesURL } from "../Utils/Links";
import axios from "axios";
import { Grid } from "@mui/material";

class MooneyImage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            src: undefined,
            url: `${ImagesURL}/${props.trialData.imgType}/${props.trialData.imgNum}`,
            scaleFactor: `${
                this.props.virtualChinrestData.scale_factor * 100
            }%`,
        };
    }

    /**
     * The function performs an API call to the server to get the image
     */
    getImage = () => {
        axios
            .get(
                `${ImagesURL}/${this.props.trialData.imgType}/${this.props.trialData.imgNum}`
            )
            .then((res) => {
                this.setState({
                    src: `data:image/bmp;base64, ${res.data}`,
                });
            });
    };

    componentDidMount() {
        this.getImage();
    }

    render() {
        return (
            <Grid
                style={{
                    paddingTop: "150%",
                }}
            >
                <img
                    src={this.state.src}
                    alt={this.state.src}
                    height={this.state.scaleFactor}
                    width={this.state.scaleFactor}
                />
            </Grid>
        );
    }
}
export default MooneyImage;
