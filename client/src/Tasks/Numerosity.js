import React from "react";

// calculate the distance between 2 points
function dist(x1, y1, x2, y2) {
    return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
}

// checks if the circles are overlapping
function isOverlap(c1, c2) {
    // calculate the distance between their centers
    let d = dist(c1.x, c1.y, c2.x, c2.y);
    // if the distance is bigger the both of their radiuses
    if (d < c1.radius + c2.radius) {
        return true;
    }
    return false;
}

// generate circles on a given canvas
// c - The canvas
// circlesNum - The number of circles to generate
// radius - The circles' radius
function generateCenters(c, circlesNum, radius) {
    const circles = [];
    // populate circles array
    const width = c.width;
    const height = c.height;
    while (circles.length < circlesNum) {
        // generate circle with center inside the canvas' boundaries
        let circle = {
            x: Math.floor(Math.random() * (width - radius * 2) + radius),
            y: Math.floor(Math.random() * (height - radius * 2) + radius),
            radius: radius,
        };
        // push it to the array if it's not overlapping any other circle
        if (circles.every((element) => !isOverlap(element, circle, radius))) {
            circles.push(circle);
        }
    }
    return circles;
}

// The function draws circles on a canvas context
function drawCircles(ctx, circles) {
    let isWhite = true;
    circles.forEach((circle) => {
        if (isWhite) {
            ctx.fillStyle = "#ffffff";
        } else {            
            ctx.fillStyle = "#000000";
        }
        ctx.beginPath();
        ctx.arc(
            circle.x,
            circle.y,
            circle.radius,
            0,
            2 * Math.PI // draw as circle
        );
        ctx.fill();
        ctx.stroke();        
        isWhite = !isWhite;
    });
}
class Numerosity extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            circlesNum: undefined,
            scaleFactor: this.props.virtualChinrestData.scale_factor,
            // the 0.99 and 0.9 are normalization, in order that scale factor of 1 will fit the screen properly
            trueWidth: window.innerWidth * 0.99,
            trueHeight: window.innerHeight * 0.9,
        };
    }

    componentDidMount() {
        this.setState({ circlesNum: this.props.circlesNum }, () => {
            this.setPageSize();
            this.createCircles();
        });
    }

    componentDidUpdate(prevProps) {
        if (prevProps.idx !== this.props.idx) {
            this.setState({ circlesNum: this.props.circlesNum }, () => {
                this.createCircles();
            });
        }
    }

    setPageSize() {
        let d = document.getElementById("fullPage");
        d.style.width = this.state.trueWidth + "px";
        d.style.height = this.state.trueHeight + "px";
    }

    //The function generates and draws circles on a canvas named "myCanvas"
    createCircles() {
        const radius = 6 * this.state.scaleFactor;
        let c = document.getElementById("myCanvas");
        let ctx = c.getContext("2d");
        c.width = this.state.trueWidth * this.state.scaleFactor;
        c.height = this.state.trueHeight * this.state.scaleFactor;
        const circles = generateCenters(c, this.state.circlesNum, radius);
        ctx.clearRect(0, 0, c.width, c.height);
        drawCircles(ctx, circles, radius);
    }

    render() {
        return (
            <div 
                id="fullPage" 
                style={{ 
                    background: "gray",
                    display:"flex",
                    justifyContent:"center",
                    alignItems:"center",
                    position: "fixed",
                    left: "50%",
                    top: "50%",
                    WebkitTransform: "translate(-50%, -50%)",
                    transform: "translate(-50%, -50%)",
                }}
            >
                <canvas id="myCanvas" style={{background:"gray"}} />
            </div>
        );
    }
}

export default Numerosity;
