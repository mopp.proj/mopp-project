import React from "react";

import {initJsPsych} from "jspsych";
import jsPsychRdk from '@jspsych-contrib/plugin-rdk';
import 'jspsych/css/jspsych.css';
import SVG from './Assets/svg.min.js.txt';

class RDK extends React.Component {
    constructor(props) {
        super(props);
        this.state = {            
            rdkType: this.props.rdkType,
            apertureType: this.props.apertureType,
            numOfDots: this.props.numOfDots,
            coherence: this.props.coherence,
            trialDuration: this.props.trialDuration,
            coherentDirection: this.props.coherentDirection,
        };
    }

    componentDidMount() {        
        this.setState({
            rdkType: this.props.rdkType,
            apertureType: this.props.apertureType,
            numOfDots: this.props.numOfDots,
            coherence: this.props.coherence,
            trialDuration: this.props.trialDuration,
            coherentDirection: this.props.coherentDirection,
        });

        // jsPsych must have this script in order to work properly        
        const script = document.createElement("script");
        script.src = SVG;
        script.async = true;
        document.body.appendChild(script);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.idx !== this.props.idx) {            
            this.setState({
                rdkType: this.props.rdkType,
                apertureType: this.props.apertureType,
                numOfDots: this.props.numOfDots,
                coherence: this.props.coherence,
                trialDuration: this.props.trialDuration,
                coherentDirection: this.props.coherentDirection,
            });
        }
    }    

    /**
     * run RDK task
     */
     runTask = () => {                
        const jsPsych = initJsPsych({
            on_finish: function() {
            }
        });        
        const trial = {
            type: jsPsychRdk,
            RDK_type: this.state.rdkType,
            aperture_type: this.state.apertureType,
            number_of_dots: this.state.numOfDots,
            coherence: this.state.coherence,
            trial_duration: this.state.trialDuration,
            coherent_direction: this.state.coherentDirection,
            correct_choice: ["!"],
        };
        
        jsPsych.run([trial]);
    }

    render() {        
        return (
            <div> 
                {this.runTask()}
            </div>
        );
    }
}

export default RDK;
