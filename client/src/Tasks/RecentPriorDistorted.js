﻿import React from "react";
import { ImagesURL } from "../Utils/Links";
import axios from "axios";
import { Grid } from "@mui/material";

class RecentPriorDistorted extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            src: undefined,
            url: 'https://mopp-project.com:5443/api/assets/RecentPrior/distorted.png',
            scaleFactor: `${this.props.virtualChinrestData.scale_factor * 100}%`,
        };
    }

    /**
     * The function performs an API call to the server to get the image
     */
    getImage = () => {

        axios
            .get('https://mopp-project.com:5443/api/assets/RecentPrior/distorted.png')
            .then((res) => {
                this.setState({
                    src: res.data
                });
            });
    };

    componentDidMount() {
        //this.getImage();
    }

    render() {
        return (
            <Grid
                style={{
                    paddingTop: "50%",
                }}
            >
                <img
                    src='https://gitlab.com/mopp.proj/mopp-project/-/raw/master/server/data/RecentPrior/distorted.png?ref_type=heads'
                    alt={this.state.src}
                    height={this.state.scaleFactor}
                    width={this.state.scaleFactor}
                />
            </Grid>
        );
    }
}
export default RecentPriorDistorted;
