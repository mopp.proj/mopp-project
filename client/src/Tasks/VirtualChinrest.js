import React from "react";
import axios from "axios";

import { Button, Grid, Typography } from "@mui/material";
import {initJsPsych} from "jspsych";
import jsPsychVirtualChinrest from '@jspsych/plugin-virtual-chinrest';
import 'jspsych/css/jspsych.css';

import CardImage from './Assets/card.png';
import CardDescription from './Assets/cardDescription.png';
import FixationDescription from './Assets/fixationDescription.png';
import SVG from './Assets/svg.min.js.txt';

import {     
    ParticipantURL,
    STAGES,
    STAGE_ITEM,
    redirectCorrectPage, 
    redirectToCorrectErrorPage
} from "../Utils/Links";

class VirtualChinrest extends React.Component {
    constructor(props) {
        super(props);        
        this.state = {
            participantsUrlID: this.props.match.params.id,
            participantID: this.props.match.params.participantID,
            showDescriptionPage: true,
            continueFromDescriptionBeforeSecondVC: false,
        };
    }

    componentDidMount() {
        this.props.setAppbar(false, false);

        const participantsUrlID = this.state.participantsUrlID;
        const participantID = this.state.participantID;

        // check that the given participantID exists with given participantsUrlID
        axios.get(`${ParticipantURL}/${participantsUrlID}/${participantID}`)
            .then(res => {                
                if (!res.data.name) {
                    redirectToCorrectErrorPage(res.status);
                }
            })
            .catch(err => {
                redirectToCorrectErrorPage(err.response.status);
            })
        
            redirectCorrectPage(STAGES.VC, participantsUrlID, participantID, this.props.cookies);

        // jsPsychVirtualChinrest must have this script in order to work properly
        const script = document.createElement("script");
        script.src = SVG;
        script.async = true;
        document.body.appendChild(script);
    }
    
    /**
     * run Virtual Chinrest task
     */
    runTask = () => {        
        let cookies = this.props.cookies;
        const participantsUrlID = this.state.participantsUrlID;
        const participantID = this.state.participantID;
        const jsPsych = initJsPsych({
            on_finish: function() {
                let trialData = jsPsych.data.get()["trials"][0];                
                let data = {};
                for (const [key, value] of Object.entries(trialData)) {
                    data[key] = typeof(value) === "string" ? value.toString().replace("'", "\"") : value;
                }                
                
                axios.put(                    
                    `${ParticipantURL}/${participantsUrlID}/${participantID}/${cookies.get(STAGE_ITEM)}`, 
                    data
                )
                    .then(res => {                        
                        if (cookies.get(STAGE_ITEM) === STAGES.VC_START) {                        
                            cookies.set(STAGE_ITEM, STAGES.VA, { path: "/" });
                            redirectCorrectPage(STAGES.VC_START, participantsUrlID, participantID, cookies);                            
                        } else {                            
                            cookies.set(STAGE_ITEM, STAGES.FINISH, { path: "/" });
                            redirectCorrectPage(STAGES.VC_END, participantsUrlID, participantID, cookies);                                                        
                        }                        
                        
                    })
                    .catch(err => {                        
                        redirectToCorrectErrorPage(err.response.status);
                    })
            }
        });
        const trial = {
            type: jsPsychVirtualChinrest,
            item_path: CardImage,
            blindspot_reps: 3,
            resize_units: "cm",
            pixels_per_unit: 50
        };       

        jsPsych.run([trial]);
    }

    continueFromDescription = () => {
        this.setState({
            showDescriptionPage: false,
        })
    }

    continueFromDescriptionBeforeSecondVC = () => {
        this.setState({
            continueFromDescriptionBeforeSecondVC: true,
        })
    }
    
    render() {
        if (!STAGES.VC_STAGES.includes(this.props.cookies.get(STAGE_ITEM))) {
            return (<div></div>);
        }
        if (this.props.cookies.get(STAGE_ITEM) === STAGES.VC_END) {        
            if (!this.state.continueFromDescriptionBeforeSecondVC) {
                return (
                    <div
                        style={{
                            margin: "auto",
                            textAlign: "center",
                        }}
                    >
                        <Typography
                            variant="h2"
                            gutterBottom
                            style={{                        
                                paddingTop: "10%",
                            }}
                        >
                            You almost done!
                        </Typography>
                        <Typography
                            variant="h4"
                            gutterBottom
                            style={{                        
                                paddingTop: "2%",
                            }}
                        >
                        For the last task, you'll need to do again the credit card task
                        </Typography>
                        <Button
                            color="primary"
                            variant="contained"
                            onClick={this.continueFromDescriptionBeforeSecondVC}                    
                            style={{
                                display: "block",
                                margin: "auto",
                                marginTop: "3%",
                            }}
                        >
                            Continue
                        </Button>
                    </div>
                )
            }
        }
        // if description should be shown
        if (this.state.showDescriptionPage) {
            return (
            <Grid>
                <Typography
                    variant="h4"
                    color="inherit"
                    noWrap
                    component="div"
                    gutterBottom
                    style={{
                        margin: "auto",
                        textAlign: "center",
                        whiteSpace: "pre-wrap",
                        paddingTop: "3%",
                    }}
                >
                    The following task has 2 steps:                    
                </Typography>                
                <Grid container
                    direction="row"
                    justifyContent="space-between"
                    alignItems="center"
                    style={{
                        paddingTop: "3%",
                        paddingLeft: "1%",
                    }}
                >
                    <Grid
                        item 
                        xs={6}                        
                    >
                        <Typography
                            variant="h4"
                            color="inherit"
                            noWrap
                            component="div"
                            style={{
                                margin: "auto",
                                textAlign: "center",
                            }}
                        >
                            Step 1:
                        </Typography>
                        <Grid>
                            <img
                                height={150}
                                src={CardDescription}
                                alt={"CardDescription"}
                                style={{
                                    display: "block",
                                    margin: "auto",
                                }} 
                            />
                            <Typography
                                variant="h6"
                                color="inherit"
                                noWrap
                                component="div"
                                style={{
                                    margin: "auto",
                                    textAlign: "center",
                                    paddingBottom: "7%",
                                }}
                            >
                                {<br/>}
                                Place a credit card or a card of equal size,{<br/>}
                                and adjust the slider until the size of the image of the card {<br/>}
                                on the screen matches the real-world card.{<br/>}                                
                            </Typography>
                        </Grid>                        
                    </Grid>
                    <Grid 
                        item 
                        xs={6}
                        
                    >
                        <Typography
                            variant="h4"
                            color="inherit"
                            noWrap
                            component="div"
                            style={{
                                margin: "auto",
                                textAlign: "center",
                            }}
                        >
                            Step 2:
                        </Typography>
                        <Grid>
                            <img
                                height={150}
                                width={400}
                                src={FixationDescription}
                                alt={"FixationDescription"}
                                style={{
                                    display: "block",
                                    margin: "auto",
                                }}
                            />
                            <Typography
                                variant="h6"
                                color="inherit"
                                noWrap
                                component="div"
                                style={{
                                    margin: "auto",
                                    textAlign: "center",
                                }}
                            >
                                {<br/>}
                                Cover your right eye with your right hand,{<br/>}
                                place your left hand on the spacebar,{<br/>}
                                and fix your left eye on the black square.{<br/>}
                                The red dot will sweep from right to left,{<br/>}
                                and you should press the spacebar when the red dot disappears{<br/>}
                            </Typography>
                        </Grid>
                    </Grid>
                </Grid>
                <Button
                    color="primary"
                    variant="contained"
                    onClick={this.continueFromDescription}                    
                    style={{
                        display: "block",
                        margin: "auto",
                        marginTop: "3%",
                    }}
                >
                    Continue
                </Button>
            </Grid>
            )
        } 
        return (
            <div> 
                {this.runTask()}
            </div>
        );               
    }
}

export default VirtualChinrest;