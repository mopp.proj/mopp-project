import React from "react";
import axios from "axios";

import { Button, Typography } from "@mui/material";

import optotypeE000 from "./Assets/optotypeE000.png"; // right
import optotypeE090 from "./Assets/optotypeE090.png"; // up
import optotypeE180 from "./Assets/optotypeE180.png"; // left
import optotypeE270 from "./Assets/optotypeE270.png"; // down
import TumblingEsDescription from "./Assets/TumblingEsDescription.png";

import {
    STAGES,
    STAGE_ITEM,
    ParticipantURL,
    redirectToAuthPage,
    redirectCorrectPage,
    redirectToCorrectErrorPage,
} from "../Utils/Links";

const ALTERNATIVE_IMAGES = [
    {
        img: optotypeE000,
        response: "Right",
    },
    {
        img: optotypeE090,
        response: "Up",
    },
    {
        img: optotypeE180,
        response: "Left",
    },
    {
        img: optotypeE270,
        response: "Down",
    },
];

const nTrials = 26;
const nAlternatives = ALTERNATIVE_IMAGES.length;

const timeoutResponseSeconds = 30;

const KEY_UP = "keyup";
const POSSIBLE_ALTERNATIVES = [0, 1, 2, 3];

const AC = 0.416908; // Acuity Constant - calculated by tan(2.5'/60) where 2.5' is in min of arc (1/60 degree)
// const OBSERVER_DISANCE = 412.5 / 10; // 412.5 cm
const OBSERVER_DISANCE = 150 / 10; // 150 cm

const MAX_SIZE = 0.05;
const STEP_SIZE = 0.05;
const MIN_SIZE = 1;

const FIRST_TRUE_TRIALS = [0.1, 0.2, 0.4, 0.6, 0.8];

class VisualAcuity extends React.Component {
    /**
     * CTOR of VisualAcuity task.
     * @param {*} props
     */
    constructor(props) {
        super(props);
        this.onKeyUp = this.onKeyUp.bind(this);

        this.state = {
            participantsUrlID: this.props.match.params.id,
            participantID: this.props.match.params.participantID,
            trialStimulations: [],
            skip_first_true_trials: false,
            currStepSize: FIRST_TRUE_TRIALS[0],
            distance: OBSERVER_DISANCE,
            imgSize: undefined,
            iTrial: 0,
            timer: undefined,
            startTime: "",
            answers: [],
            acuityValues: [FIRST_TRUE_TRIALS[0]],
            showTaskDescription: true,
        };
    }

    /**
     * This function does several things after the component is mounted:
     * 1. Add an event listener for using the keyboard in order to make a response to the given stimulus.
     * 2. Checks if the participant is really in the correct page (and didn't redirected themselves manually).
     * 3. Configures the different trials.
     */
    componentDidMount() {
        document.addEventListener(KEY_UP, this.onKeyUp);

        const participantsUrlID = this.state.participantsUrlID;
        const participantID = this.state.participantID;        

        // check that the given participantID exists with given participantsUrlID
        axios
            .get(`${ParticipantURL}/${participantsUrlID}/${participantID}`)
            .then((res) => {
                if (!res.data.name) {
                    // redirect to authentication page
                    redirectToAuthPage(participantsUrlID);
                }

                redirectCorrectPage(STAGES.VA, participantsUrlID, participantID, this.props.cookies);

                const participantsDistance =
                    OBSERVER_DISANCE *
                    res.data.participants.virtualChinrestStart.scale_factor;

                this.setState(
                    {
                        distance: participantsDistance,
                        imgSize: this.getImageSize(
                            FIRST_TRUE_TRIALS[0],
                            participantsDistance
                        ),
                    },
                    () => {
                        this.props.setAppbar(false, false);
                        this.createStimulations();
                    }
                );
            })
            .catch((err) => {
                // redirect to correct page
                redirectCorrectPage(STAGES.VA, participantsUrlID, participantID, this.props.cookies);
            });
    }

    /**
     * This function configures the stimulus (E's position) in each trial,
     * and shows the task's description before starting the task.
     */
    createStimulations = () => {
        const stimArr = this.reapeatArray(
            POSSIBLE_ALTERNATIVES,
            Math.round(nTrials / nAlternatives)
        ).concat(POSSIBLE_ALTERNATIVES.slice(0, nTrials % nAlternatives));

        this.setState(
            {
                trialStimulations: this.randomiseArray(stimArr),
                freeStimulations: this.getFreeStimulatios(),
            },
            () => {
                this._showDescriptionDelayed();
            }
        );
    };

    /**
     * This function repeats the given array for the given amount of times.
     * @param {Number} arr - array which should be repeated.
     * @param {Number} repeats - number of repeats.
     * @returns Array[Number] - a repeated array.
     */
    reapeatArray = (arr, repeats) => {
        return Array.from({ length: repeats }, () => arr).flat();
    };

    /**
     * This function randomise the elements in the given array.
     * @param {Number} arr
     * @returns a randomized array.
     */
    randomiseArray = (arr) => {
        return arr
            .map((value) => ({ value, sort: Math.random() }))
            .sort((a, b) => a.sort - b.sort)
            .map(({ value }) => value);
    };

    /**
     * This function chooses 3 trials as "Free Trials", where the answer doesn't matter
     * and the stimulus will be bigger by 0.1VA.
     * The purpose of this trials is to let the participant feel better with the task.
     * @returns Array[Number] - array of 3 indexes.
     */
    getFreeStimulatios = () => {
        let freeStims = [];

        while (freeStims.length < 3) {
            let stim =
                Math.floor(
                    Math.random() * (nTrials - FIRST_TRUE_TRIALS.length)
                ) + FIRST_TRUE_TRIALS.length;
            if (freeStims.indexOf(stim) === -1) {
                freeStims.push(stim);
            }
        }
        return freeStims;
    };

    /**
     * This function sets a timer for reading the task's description
     * and after the time has passed it start the VisualAcuity task
     */
    _showDescriptionDelayed = () => {
        var timer = setInterval(() => {
            this.startTask();
        }, timeoutResponseSeconds * 1000);
        this.setState({ timer: timer });
    };

    /**
     * This function clears the previous timer (which was set for reading the task's description)
     * and showing the first stimulus
     */
    startTask = () => {
        this.setState(
            {
                showTaskDescription: false,
            },
            () => {
                if (this.state.timer) {
                    clearInterval(this.state.timer);
                }
                this._showStimulusDelayed();
            }
        );
    };

    /**
     * This function sets a timer for responsing the given stimulus
     * and after the times has passed it shows the next trial.
     */
    _showStimulusDelayed = () => {
        var timer = setInterval(() => {
            this.setNextTrial("None", "");
        }, timeoutResponseSeconds * 1000);
        this.setState({ timer: timer, startTime: performance.now() });
    };

    /**
     * This function sets the next trial as a result of the response for the current trial.
     * @param {String} answer - the answer of the participant.
     * @param {Number} rt - response time of the participant.
     */
    setNextTrial = (answer, rt) => {
        const rightAnswer =
            ALTERNATIVE_IMAGES[this.state.trialStimulations[this.state.iTrial]]
                .response;
        const wasCorrect = answer === rightAnswer;
        const currStepSize = this.checkNextStepSize(wasCorrect);        

        this.setState({
            iTrial: this.state.iTrial + 1,
            currStepSize: currStepSize,
            imgSize: this.getImageSize(currStepSize),
            answers: this.state.answers.concat([
                {
                    "vaValue": this.state.currStepSize,
                    "isFreeStimulation": this.state.freeStimulations.indexOf(this.state.iTrial) === -1 ? false : true,
                    "answer": answer, 
                    "rightAnswer": rightAnswer, 
                    "rt": rt
                }]),
            acuityValues: this.state.acuityValues.concat([currStepSize])
        }, () => {
            if (this.state.timer) {
                clearInterval(this.state.timer);
            }
            // if the next trial is not the last one
            if (this.state.iTrial < nTrials) {
                this._showStimulusDelayed();
            } else {                
                this.endTask();
            }
        });
    };

    /**
     * This function returns the step size for the next trial, corresponding to
     * correctness and the index of the trial
     * @param {Boolean} wasCorrect - true is the participant was correct, otherwise false.
     * @returns Number - the step size for the next trial.
     */
    checkNextStepSize = (wasCorrect) => {
        // if the current trial is one of the first 5 trials and the participant was wrong
        if (this.state.iTrial < FIRST_TRUE_TRIALS.length - 1 && !wasCorrect) {
            this.setState(
                {
                    skip_first_true_trials: true,
                },
                () => {
                    return this.getNextStepSize(wasCorrect);
                }
            );
        }
        return this.getNextStepSize(wasCorrect);
    };

    /**
     * This function returns the step size of the next trial given different cases.
     * @param {Boolean} wasCorrect - true is the participant was correct, otherwise false.
     * @returns Number - the step size for the next trial.
     */
    getNextStepSize = (wasCorrect) => {
        // if the next trial in one of the first true trials
        if (
            !this.state.skip_first_true_trials &&
            this.state.iTrial < FIRST_TRUE_TRIALS.length - 1 &&
            wasCorrect
        ) {
            return FIRST_TRUE_TRIALS[this.state.iTrial + 1];
        }

        // if the next trial is a "free stimulation trial" - return a bigger stimulation
        if (this.state.freeStimulations.indexOf(this.state.iTrial + 1) !== -1) {
            return Math.max(this.state.currStepSize - 2 * STEP_SIZE, MAX_SIZE);
        }

        // if the current trial was a "free stimulation trial" - continue with the relevant step from previous real trials
        if (this.state.freeStimulations.indexOf(this.state.iTrial) !== -1) {
            return this.getDiffFromLastTrueRealTrials()
                ? Math.min(this.state.currStepSize + 3 * STEP_SIZE, MIN_SIZE)
                : Math.min(this.state.currStepSize + STEP_SIZE, MIN_SIZE);
        }

        return wasCorrect
            ? // two-up
              this.getDiffFromLastTrueRealTrials()
                ? Math.min(this.state.currStepSize + STEP_SIZE, MIN_SIZE)
                : this.state.currStepSize
            : // one-down
              Math.max(this.state.currStepSize - STEP_SIZE, MAX_SIZE);
    };

    /**
     * This helper function finds the last 2 "non-free" trials
     * and returns true if the difference between is 0 or more.
     * @returns Boolean - true is the difference is 0 or more, otherwise false.
     */
    getDiffFromLastTrueRealTrials = () => {
        if (
            this.state.iTrial < Math.min(this.state.freeStimulations) ||
            this.state.iTrial - 1 > Math.max(this.state.freeStimulations)
        ) {
            return (
                this.state.acuityValues[this.state.iTrial] -
                    this.state.acuityValues[this.state.iTrial - 1] >=
                0
            );
        }

        let firstRealTrial = this.getLastRealTrial(this.state.iTrial);
        let secondRealTrial = this.getLastRealTrial(firstRealTrial.startFrom);
        return (
            this.state.acuityValues[firstRealTrial.trialIndex] -
                this.state.acuityValues[secondRealTrial.trialIndex] >=
            0
        );
    };

    /**
     * This helper function search for the last "non-free" trial given the last index to start from.
     * @param {Number} startFrom - the index to start looking for a "non-free" trial.
     * @returns Number - the index of the last "non-free" trial, given startFrom value.
     */
    getLastRealTrial = (startFrom) => {
        let trialIndex = undefined;
        while (trialIndex === undefined) {
            if (this.state.freeStimulations.indexOf(startFrom) === -1) {
                trialIndex = startFrom;
            }
            startFrom -= 1;
        }
        return {
            trialIndex: trialIndex,
            startFrom: startFrom,
        };
    };

    /**
     * This function returns the stimulus size given a step size and the distance of the participant from the screen.
     * @param {Number} currStepSize - current trial's step size.
     * @param {Number} givenDistance - the distance of the participant from the screen (undefined unless it's the first trial).
     * @returns Number - stimulus size of the next trial
     */
    getImageSize = (currStepSize, givenDistance) => {
        const distance = givenDistance ? givenDistance : this.state.distance;
        return (AC / currStepSize) * distance * window.devicePixelRatio;
    };

    /**
     * This function is called after the last trial was finished.
     * It sends the answers to the db, and redirect the participant for the experiment's tasks page.
     */
    endTask = () => {
        const data = {
            answers: this.state.answers,
        };

        axios
            .put(
                `${ParticipantURL}/${this.state.participantsUrlID}/${this.state.participantID}/${STAGES.VA}`,
                data
            )
            .then((res) => {
                // redirect to ParticipantGUI page, where the different trials will be shown to the participant                
                this.props.cookies.set(STAGE_ITEM, STAGES.TASKS, { path: "/" });
                redirectCorrectPage(
                    STAGES.VA,
                    this.state.participantsUrlID,
                    this.state.participantID,
                    this.props.cookies,
                );
            })
            .catch((err) => {
                redirectToCorrectErrorPage(err.response.status);
            });
    };

    /**
     * This function is called when the participant chooses "Up" button.
     * @param {Number} rt
     */
    sendAnswerUp = (rt) => {
        this.setNextTrial(
            "Up",
            typeof rt === "number"
                ? rt
                : performance.now() - this.state.startTime
        );
    };

    /**
     * This function is called when the participant chooses "Down" button.
     * @param {Number} rt
     */
    sendAnswerDown = (rt) => {
        this.setNextTrial(
            "Down",
            typeof rt === "number"
                ? rt
                : performance.now() - this.state.startTime
        );
    };

    /**
     * This function is called when the participant chooses "Left" button.
     * @param {Number} rt
     */
    sendAnswerLeft = (rt) => {
        this.setNextTrial(
            "Left",
            typeof rt === "number"
                ? rt
                : performance.now() - this.state.startTime
        );
    };

    /**
     * This function is called when the participant chooses "Right" button.
     * @param {Number} rt
     */
    sendAnswerRight = (rt) => {
        this.setNextTrial(
            "Right",
            typeof rt === "number"
                ? rt
                : performance.now() - this.state.startTime
        );
    };

    /**
     * This function is called when the participant uses the keyboard to respond.
     * @param {Event} event - the event which was captured.
     */
    onKeyUp = (event) => {
        const rt = performance.now() - this.state.startTime;

        switch (event.key) {
            case "ArrowUp":
                this.sendAnswerUp(rt);
                break;
            case "ArrowDown":
                this.sendAnswerDown(rt);
                break;
            case "ArrowLeft":
                this.sendAnswerLeft(rt);
                break;
            case "ArrowRight":
                this.sendAnswerRight(rt);
                break;
            default:
                break;
        }
    };

    /**
     * This function is called after the component is unmounted from the page,
     * and removes the event listener for using the keyboard.
     */
    componentWillUnmount() {
        document.removeEventListener(KEY_UP, this.onKeyUp);

        if (this.state.timer) {
            clearInterval(this.state.timer);
        }
    }

    /**
     * @returns HTML objects - the html tags that should be sent to the browser.
     */
    render() {
        // if the participant should be in a different step of the experiment        
            if (STAGES.VA !== this.props.cookies.get(STAGE_ITEM)) {
            return <div></div>;
        }
        // if the participant haven't read the task's description
        if (this.state.showTaskDescription) {
            return (
                <div
                    style={{
                        margin: "auto",
                        textAlign: "center",
                    }}
                >
                    <Typography
                        variant="h4"
                        gutterBottom
                        style={{
                            paddingTop: "10%",
                        }}
                    >
                        The letter “E“ will appear in different orientations.{<br />}
                        Please estimate its orientation using the cursor keys {<br />}
                        (one out of four options: up, down, left, right). {<br />}
                        It is important that you don't move during this task.{<br />}
                        {
                            <img
                                alt=""
                                src={TumblingEsDescription}
                                height="200"
                                width="200"
                            />
                        }
                        {<br />}
                        If you can't recognize the orientation, use your best
                        guess.
                    </Typography>
                    <Button variant="contained" onClick={this.startTask}>
                        <Typography
                            variant="h5"
                            color="inherit"
                            style={{
                                paddingTop: "2%",
                            }}
                        >
                            Let's Start!
                        </Typography>
                    </Button>
                </div>
            );
        }
        return (
            <div>
                <div
                    style={{
                        position: "fixed",
                        left: "50%",
                        top: "50%",
                        WebkitTransform: "translate(-50%, -50%)",
                        transform: "translate(-50%, -50%)",
                    }}
                >
                    {this.state.iTrial < nTrials &&
                    ALTERNATIVE_IMAGES[
                        this.state.trialStimulations[this.state.iTrial]
                    ] ? (
                        <img
                            alt=""
                            src={
                                ALTERNATIVE_IMAGES[
                                    this.state.trialStimulations[
                                        this.state.iTrial
                                    ]
                                ].img
                            }
                            height={this.state.imgSize}
                            width={this.state.imgSize}
                        />
                    ) : (
                        <></>
                    )}
                </div>
                {this.state.iTrial < nTrials ? (
                    <div>
                        <Button
                            variant="contained"
                            onClick={this.sendAnswerUp}
                            style={{
                                width: "120px",
                                position: "fixed",
                                left: "50%",
                                top: "2%",
                                WebkitTransform: "translate(-50%, 2%)",
                                transform: "translate(-50%, 2%)",
                            }}
                        >
                            <Typography variant="h5" color="inherit">
                                Up
                            </Typography>
                        </Button>
                        <Button
                            variant="contained"
                            onClick={this.sendAnswerDown}
                            style={{
                                width: "120px",
                                position: "fixed",
                                left: "50%",
                                bottom: "2%",
                                WebkitTransform: "translate(-50%, 2%)",
                                transform: "translate(-50%, 2%)",
                            }}
                        >
                            <Typography variant="h5" color="inherit">
                                Down
                            </Typography>
                        </Button>
                        <Button
                            variant="contained"
                            onClick={this.sendAnswerLeft}
                            style={{
                                width: "120px",
                                position: "fixed",
                                left: "2%",
                                top: "50%",
                                WebkitTransform: "translate(2%, -50%)",
                                transform: "translate(2%, -50%)",
                            }}
                        >
                            <Typography variant="h5" color="inherit">
                                Left
                            </Typography>
                        </Button>
                        <Button
                            variant="contained"
                            onClick={this.sendAnswerRight}
                            style={{
                                width: "120px",
                                position: "fixed",
                                right: "2%",
                                top: "50%",
                                WebkitTransform: "translate(2%, -50%)",
                                transform: "translate(2%, -50%)",
                            }}
                        >
                            <Typography variant="h5" color="inherit">
                                Right
                            </Typography>
                        </Button>
                    </div>
                ) : (
                    <></>
                )}
            </div>
        );
    }
}

export default VisualAcuity;
