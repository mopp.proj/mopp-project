
// For running MOPP local, use this baseClientURL and baseServerURL
// const baseClientURL = "https://localhost:8080";
// const baseServerURL = "https://localhost:5443/api";

// For production, replace this baseURL with your public DNS:
const BASE_URL = "https://mopp-project.com"

const baseClientURL = BASE_URL;
exports.HomepageURL = baseClientURL;
exports.AuthURL = `${baseClientURL}/auth`;
exports.ExperimentsTableURL = `${baseClientURL}/experiments`;

const baseServerURL = BASE_URL+":5443/api";
exports.ExperimentURL = `${baseServerURL}/experiments`;
exports.ParticipantURL = `${baseServerURL}/participants`;
exports.AssetsURL = `${baseServerURL}/assets`;
exports.ImagesURL = `${baseServerURL}/images`;

// app redirections
exports.STAGE_ITEM = "mopp_stage";
exports.PASS_ITEM = "mopp_pass";
exports.STIMULUS_DISPLAY_TIME = "stimulus_display_time";

exports.STAGES = {
    "AUTH": "auth",
    "SURVEY": "sr",
    "VC": "vc",
    "VC_START": "vcs",
    "VC_END": "vce",
    "VC_STAGES": ["vcs", "vce"],
    "VA": "va",
    "TASKS": "tasks",
    "FINISH": "finish",
}

exports.redirectToHomePage = () => {
    window.location = this.HomepageURL;
};

exports.redirectToExperimentPage = (experimentID) => {
    if (experimentID) {
        window.location = `${this.ExperimentsTableURL}/${experimentID}`;
    } else {
        window.location = `${this.ExperimentsTableURL}`;
    }
};

exports.redirectToAuthPage = (participantsUrlID) => {
    if (participantsUrlID) {
        window.location = `${this.HomepageURL}/${participantsUrlID}/${this.STAGES.AUTH}`;
    } else {
        this.redirectToHomePage();
    }
};

exports.redirectCorrectPage = (
    currentPage,
    participantsUrlID,
    participantID,
    cookies,
) => {    
    const stage = cookies.get(this.STAGE_ITEM);

    if (!stage || participantID === undefined) {
        window.location = `${this.HomepageURL}/${participantsUrlID}/${this.STAGES.AUTH}`;
    }

    switch (stage) {
        case this.STAGES.SURVEY:
            if (currentPage !== this.STAGES.SURVEY) {
                window.location = `${this.HomepageURL}/${participantsUrlID}/${participantID}/${this.STAGES.SURVEY}`;
            }
            break;
        case this.STAGES.VC_START:
            if (currentPage !== this.STAGES.VC) {
                window.location = `${this.HomepageURL}/${participantsUrlID}/${participantID}/${this.STAGES.VC}`;
            }
            break;

        case this.STAGES.VC_END:
            if (currentPage !== this.STAGES.VC) {
                window.location = `${this.HomepageURL}/${participantsUrlID}/${participantID}/${this.STAGES.VC}`;
            }
            break;

        case this.STAGES.VA:
            if (currentPage !== this.STAGES.VA) {
                window.location = `${this.HomepageURL}/${participantsUrlID}/${participantID}/${this.STAGES.VA}`;
            }
            break;

        case this.STAGES.TASKS:
            if (currentPage !== this.STAGES.TASKS) {
                this.redirectStimulusPage(participantsUrlID, participantID);
            }
            break;

        case this.STAGES.FINISH:
            if (currentPage !== this.STAGES.FINISH) {
                window.location = `${this.HomepageURL}/${participantsUrlID}/${participantID}/${this.STAGES.FINISH}`;
            }
            break;
        
        default:
            window.location = `${this.HomepageURL}/${participantsUrlID}/${this.STAGES.AUTH}`;
        break;
    }
};

exports.redirectStimulusPage = (participantsUrlID, participantID) => {
    window.location = `${this.HomepageURL}/${participantsUrlID}/${participantID}/stimulus`;
}

exports.redirectResponsePage = (participantsUrlID, participantID) => {
    window.location = `${this.HomepageURL}/${participantsUrlID}/${participantID}/response`;
}

exports.httpStatusCodes = {
    OK: 200,
    BadRequest: 400,
    Forbidden: 403,
    NotFound: 404,
    InternalServerError: 500,
};

exports.redirectToCorrectErrorPage = (status) => {
    switch (status) {
        case this.httpStatusCodes.Forbidden:
            this.redirectToForbiddenAccess();
            break;
        case this.httpStatusCodes.NotFound:
            this.redirectToPageNotFound();
            break;
        default:
            this.redirectToInternalServerError();
            break;
    }
};

exports.redirectToForbiddenAccess = () => {
    window.location = `${this.HomepageURL}/ForbiddenAccess`;
};

exports.redirectToPageNotFound = () => {
    window.location = `${this.HomepageURL}/PageNotFound`;
};

exports.redirectToInternalServerError = () => {
    window.location = `${this.HomepageURL}/InternalServerError`;
};
