/**
 * This function formats a given date into format of "yy-mm-dd", 
 * in order to show date field properly
 * @param {*} date 
 * @returns formatted date
 */
 exports.formatDate = (date) => {
    let newDate = new Date(date);
    
    let year = newDate.getFullYear();
    
    let month = '' + (newDate.getMonth() + 1);
    month = month.length < 2 ? '0' + month : month;
    
    let day = '' + newDate.getDate();
    day = day.length < 2 ? '0' + day : day;

    return [year, month, day].join('-')
}