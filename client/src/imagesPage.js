import { Button, ListItem, Typography } from "@mui/material";
import React from "react";
import { ImagesURL } from "./Utils/Links";

const BMP_FILE_REQUIREIEMTS = [
    "Starts with a single character: I (inverted), U (upright), S (scrambled)",
    "Contains only a number with leading 0's ",
    ".bmp file image",
];

class ImagePage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            url: `${ImagesURL}/images`,
            selectedFiles: undefined,
            marginFromLeft: window.innerWidth / 2 - 30,
        };
    }

    updateSelectedFiles = (event) => {
        this.setState({
            selectedFiles: event.target.files,
        })
    }
    render() {
        return (
            <div
                style={{
                    margin: "auto",
                    textAlign: "center",
                    paddingTop: "5%",
                }}
            >
                <form
                    action={this.state.url}
                    method="post"
                    encType="multipart/form-data"
                >
                    <div>                        
                        <Typography
                            variant="h4"
                            gutterBottom
                        >
                            <label htmlFor="image">Upload images for "Mooney Images" task</label>
                        </Typography>
                        <br/>
                        <input
                            type="file"
                            id="image"
                            name="uploaded_image"
                            defaultValue=""
                            required
                            multiple
                            onChange={this.updateSelectedFiles}
                        />                        
                        <div>                    
                            <br/>
                            <Button 
                                type="submit"
                                color="primary"
                                size="small"
                                variant="contained"
                                disabled={!this.state.selectedFiles}
                            >
                                <Typography
                                    variant="h6"                                    
                                >
                                    Submit
                                </Typography>                                
                            </Button>
                        </div>
                        <br/><br/>
                        <Typography
                            variant="h5"
                            gutterBottom
                        >
                            Make sure that each image fits all requirements:                            
                        </Typography>                        
                        {BMP_FILE_REQUIREIEMTS.map((requirement, id) => {                                   
                                return (
                                    <Typography                                            
                                        variant="h6"    
                                        gutterBottom
                                            key={id}
                                    >
                                        <ListItem
                                            sx={{
                                                display: 'list-item'
                                            }}
                                            style={{
                                                margin: "auto",
                                                textAlign: "center",                                                
                                            }}
                                            key={id}
                                        >
                                            
                                                {requirement}                                                                          
                                        </ListItem>
                                    </Typography>                                        
                                );                                
                            })}
                        <Typography                                            
                            variant="h6"    
                            gutterBottom                                
                        >
                        For example: I0004.bmp
                        </Typography>
                    </div>                    
                </form>
            </div>
        );
    }
}
export default ImagePage;
