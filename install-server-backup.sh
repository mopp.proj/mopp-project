#!/bin/bash

# Forces the script to quit on error
set -e
# Usage example of the script. If there is a problem with the scripts arguments, it prints this to the screen and exit the installation
usage()
{
  echo "Usage: $0 [-d <DNS name (no https://)>]"; exit 1;
}

# Clone the git project
cloneProject()
{
  echo "Cloning the project..."
  if ! git clone https://gitlab.com/mopp.proj/mopp-project.git
  then
    echo "WARNING - mopp-project folder already exists in this folder. Do you want to delete it? (y/n)"
    read isDelete
    if [ "$isDelete" == "y" ]
    then
      sudo rm -r mopp-project
      echo "Cloning the project..."
      git clone https://gitlab.com/mopp.proj/mopp-project.git
    else
      echo "Aborting clone..."
      exit 1
    fi
  fi
}

# Install npm dependencies for client and server
installDependencies()
{
  echo "Installing server dependencies..."
  cd "$BASE_SERVER_DIR"
  npm install
  cd "$BASE_PATH"
}

# Updating server variables with the given DNS
updateServer()
{
  echo "Updating server variables..."
  sed -i "s/{dns}/$http_dns/" "$BASE_SERVER_DIR/.env"

  echo "Please enter the password you wish to enter with: "
  read password
  sed -i "s/{password}/$password/" "$BASE_SERVER_DIR/.env"
}

# Create a CA Certificate
createCertificates()
{
  echo "Creating SSL Certificates..."
  sudo openssl genrsa -out custom.key
  sudo openssl req -new -key custom.key -out csr.pem
}

# Update the httpd with the given domain
updateHttpd()
{
  echo "Updating httpd server with your domain..."
  sudo sed -i "s/mopp.ddns.net/$dns/g" "/etc/httpd/conf/httpd.conf"
}

# Activate the certbot for renewing the keys
activateCertbot()
{
  sudo certbot
}

# Copy the keys from client to server
copyKeys()
{
  echo "Copying keys from client to server..."
  sudo mkdir -p "./mopp-project/server/ssl"
  sudo cp /etc/letsencrypt/live/$dns/fullchain.pem ./mopp-project/server/ssl
  sudo cp /etc/letsencrypt/live/$dns/privkey.pem ./mopp-project/server/ssl
}

BASE_SERVER_DIR="./mopp-project/server"
BASE_PATH=$PWD

# Parsing the arguments given by the user
while getopts 'd:' OPTION; do
  case "$OPTION" in
    d)
      dns="$OPTARG"
      if [[ "$dns" == *"https://"* ]]
        then
          echo "ERROR: DNS arguemnt cannot be added with 'https://'"
          usage
      fi
      echo "The DNS given is: $OPTARG"
      http_dns="https:\/\/$dns"
      ;;
    ?)
      usage
      ;;
  esac
done
shift "$(($OPTIND-1))"

if [ $OPTIND -eq 1 ]
    then
        echo "No arguments were give"
        usage
fi

cloneProject
# installDependencies
# updateServer
# createCertificates
# updateHttpd
# activateCertbot
# copyKeys

# echo "Server installation complete! Please reboot the EC2 machine"


