module.exports = {
	apps:[{
			name:"mopp-server",
			cwd:".././server",
			script:"./start-server.sh",
			watch:true,
			env:{
					"NODE_ENV":"development",
			},
			env_production:{
					"NODE_ENV":"production",
			}
	}]
	//{
	//      name:"mopp-client",
	//      cwd:".././client",
	//      script:"npm",
	//      args:"start",
	//      watch:true,
	//      env:{
	//              "NODE_ENV":"development",
	//      },
	//      env_production:{
	//              "NODE_ENV":"production"
	//      }
	//}]
}