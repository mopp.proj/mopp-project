const distributions = require("./distributions");
const fs = require("fs");
const path = require("path");

const dataPath = "../data/BML gifs/";
let globalDifficulty = "";
let randomFiles = [];
let difficultyFiles = [];

/**
 * The function chooses a gif by a given difficulty and type
 * @param {*} difficulty 
 * @param {*} type 
 * @returns The chosen gif
 */
exports.chooseGif = (difficulty, type) => {
    // get the difficulty data if it does not exist
    if (difficultyFiles.length === 0 || globalDifficulty !== difficulty) {
        let dirPath = path.resolve(__dirname, dataPath + difficulty);
        difficultyFiles = fs.readdirSync(dirPath);
        globalDifficulty = difficulty;
    }
    // get the random files
    if (randomFiles.length == 0) {
        let dirPath = path.resolve(__dirname, dataPath + "Random");
        randomFiles = fs.readdirSync(dirPath);
    }
    const distribution = {
        distributionType: "Random",
        min: 0,
    };
    // choose gif from random or from difficulty
    if (type == "BML") {
        distribution.max = difficultyFiles.length - 1;
        const gifNum = distributions.getNumberFromDistribution(distribution);
        return difficultyFiles[gifNum];
    } else {
        distribution.max = randomFiles.length - 1;
        const gifNum = distributions.getNumberFromDistribution(distribution);
        return randomFiles[gifNum];
    }
};
/**
 * The function chooses a gif type (random dots or BML)
 * @returns The chosen type
 */
exports.chooseType = () => {
    const distribution = {
        distributionType: "Random",
        min: 0,
        max: 1,
    };
    // choose gif from random or from difficulty
    const trialType = distributions.getNumberFromDistribution(distribution);
    if (trialType === 0) {
        return "BML";
    } else {
        return "Random";
    }
};
