const distributions = require("./distributions");

/**
 * The function chooses an image by type
 * @param {*} type
 * @returns The chosen image
 */
exports.chooseImage = (type) => {
    let max = 0;
    switch (type) {
        case "U":
        case "I":
            max = 504;
            break;
        case "S":
            max = 100;
        default:
            break;
    }
    const distribution = {
        distributionType: "Random",
        min: 0,
        max: max,
    };
    return distributions.getNumberFromDistribution(distribution);
};

/**
 * The function chooses a mooney image type (Upright (0), Inverted (1) or Scrambled (2)) based on the selected difficulty:
 * If "Medium" was selected, the types ratio will be around 1/3 U, 1/3 I and 1/3 S.
 * If "Easy" was selected, the types will be around 1/2 U and 1/2 S.
 * If "Hard" was selected, the types will be around 1/2 I and 1/2 S.
 * @returns The chosen type
 */
exports.chooseType = (difficulty) => {
    if (difficulty == "Medium") {
        const distribution = {
            distributionType: "Random",
            min: 0,
            max: 2,
        };
        const trialType = distributions.getNumberFromDistribution(distribution);
        switch (trialType) {
            case 0:
                return "U";
            case 1:
                return "I";
            case 2:
                return "S";
        }
    } else {
        const distribution = {
            distributionType: "Random",
            min: 0,
            max: 1,
        };
        const trialType = distributions.getNumberFromDistribution(distribution);
        switch (trialType) {
            case 0:
                return difficulty == "Easy" ? "U" : "I";
            case 1:
                return "S";
        }
    }
};
