/**
 * This function gets some arguments, and returns a number from the selected distribution type.
 * @param {Object} distributionArgs
 * @returns a number from a given distribution.
 */
exports.getNumberFromDistribution = (distributionArgs) => {
    let number = 0;
    switch (distributionArgs.distributionType) {
        case "Bimodal":
            number = getNumberFromBimodalDistribution(
                distributionArgs.mean1,
                distributionArgs.std1,
                distributionArgs.mean2,
                distributionArgs.std2
            );
            break;
        case "Gaussian":
            number = getNumberFromGaussianDistribution(
                distributionArgs.mean,
                distributionArgs.std
            );
            break;
        case "Random":
            number = getNumberFromRandomDistribution(
                distributionArgs.min,
                distributionArgs.max
            );
            break;
        default:
            throw new Error("Invalid Distribution type - " + distType);
    }
    return number;
};

/**
 * This function returns a number from a Bimodal Distribution.
 * @param {Number} mean1
 * @param {Number} std1
 * @param {Number} mean2
 * @returns a number from a Bimodal Distribution.
 */
function getNumberFromBimodalDistribution(mean1, std1, mean2, std2) {
    mean1 = Number(mean1);
    mean2 = Number(mean2);
    std1 = Number(std1);
    std2 = Number(std2);
    // make sure that all arguments are numbers
    if ("number" !== typeof mean1 || mean1 < 0) {
        throw new TypeError(
            "getNumberFromBimodalDistribution: mean1 must be a positive number."
        );
    }
    if ("number" !== typeof mean2 || mean2 < 0) {
        throw new TypeError(
            "getNumberFromBimodalDistribution: mean2 must be a positive number."
        );
    }
    if ("number" !== typeof std1 || std1 < 0) {
        throw new TypeError(
            "getNumberFromBimodalDistribution: std must be a positive number."
        );
    }
    if ("number" !== typeof std2 || std2 < 0) {
        throw new TypeError(
            "getNumberFromBimodalDistribution: std must be a positive number."
        );
    }
    // pick a random number between 2 given mean values
    let random = getNumberFromRandomDistribution(
        Math.min(mean1, mean2),
        Math.max(mean1, mean2)
    );
    // decide which mean is closer to the random number
    let mean =
        Math.abs(mean1 - random) < Math.abs(mean2 - random) ? mean1 : mean2;
    // choose the std based on the mean that was chosen
    let std = mean === mean1 ? std1 : std2;
    
    // pick a random number from gaussian distirubtion using the closer mean and given std
    return getNumberFromGaussianDistribution(mean, std);
}

/**
 * This function returns a number from a Gaussian Distribution.
 * @param {*} mean
 * @param {*} std
 * @returns a number from a Gaussian Distribution
 */
function getNumberFromGaussianDistribution(mean, std) {
    mean = Number(mean);
    std = Number(std);
    // make sure that all arguments are valid
    if ("number" !== typeof mean || mean < 0) {
        throw new TypeError(
            "getNumberFromGaussianDistribution: mean must be a positive number."
        );
    }
    if ("number" !== typeof std || std < 0) {
        throw new TypeError(
            "getNumberFromGaussianDistribution: std must be a positive number."
        );
    }

    let u1 = Math.random();
    let u2 = Math.random();

    let v1 = 2 * u1 - 1;
    let v2 = 2 * u2 - 1;
    let s = v1 * v1 + v2 * v2;

    if (s >= 1) {
        return getNumberFromGaussianDistribution(mean, std);
    }

    let multiplier = Math.sqrt((-2 * Math.log(s)) / s);
    let x1 = v1 * multiplier;

    return Math.max(Math.ceil(std * x1 + mean), 1);
}

/**
 * This function returns a number from a Random Distribution.
 * @param {*} min
 * @param {*} max
 * @returns a number from a Random Distribution.
 */
function getNumberFromRandomDistribution(min, max) {
    min = Number(min);
    max = Number(max);
    // make sure that all arguments are valid
    if ("number" !== typeof min || min < 0) {
        throw new TypeError(
            "getNumberFromRandomDistribution: min must be a positive number."
        );
    }
    if ("number" !== typeof max || max < 0) {
        throw new TypeError(
            "getNumberFromRandomDistribution: max must be a positive number."
        );
    }
    if (max < min) {
        throw new Error(
            "getNumberFromRandomDistribution: max must be greater than min."
        );
    }
    const randomNum = Math.random() * (max - min);
    if (randomNum > 0.5) {
        return Math.ceil(randomNum) + min;
    } else {
        return Math.floor(randomNum) + min;
    }
}
