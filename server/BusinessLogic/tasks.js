﻿const distributions = require("./distributions");
const biologicalMotionLogic = require("./BiologicalMotionLogic");
const mooneyImageLogic = require("./MooneyImageLogic");

/**
 * This function gets a task, and returns an array of trials - each trial with it's relevant fields and values.
 * @param {*} task
 * @returns an array of trials.
 */
exports.createTaskTrials = (task) => {
    let trials = [];
    let practiceTrial = task.generalFields.practiceTrial ? 1 : 0;
    let keyTappingIndex = 0;
    for (i = 0; i < task.generalFields.trialNum + practiceTrial; i++) {
        switch (task.type) {
            case "Numerosity":
                trials.push({
                    numberOfObjects: distributions.getNumberFromDistribution(
                        task.fields
                    ),
                    isPractice: i == 0 && practiceTrial == 1 ? true : false,
                });
                break;
            case "Line Length":
                trials.push({
                    LengthOfLine: distributions.getNumberFromDistribution(
                        task.fields
                    ),
                    isPractice: i == 0 && practiceTrial == 1 ? true : false,
                });
                break;
            case "Biological Motion":
                // if practice trial, insert easy BML and random BML
                if (i == 0 && practiceTrial == 1) {
                    trials.push({
                        gifName: biologicalMotionLogic.chooseGif("Easy", "BML"),
                        gifType: "BML",
                        difficulty: "Easy",
                        isPractice: true,
                    });
                    trials.push({
                        gifName: biologicalMotionLogic.chooseGif(
                            "Easy",
                            "Random"
                        ),
                        gifType: "Random",
                        difficulty: "Random",
                        isPractice: true,
                    });
                } else {
                    const type = biologicalMotionLogic.chooseType();
                    trials.push({
                        gifName: biologicalMotionLogic.chooseGif(
                            task.fields.difficulty,
                            type
                        ),
                        gifType: type,
                        difficulty:
                            type === "BML" ? task.fields.difficulty : "Random",
                        isPractice: false,
                    });
                }
                break;
            case "Keyboard Tapping":
                if (practiceTrial === 1 && i === 0) {
                    trials.push({
                        hand: "Left",
                        isPractice: true,
                    });
                } else {
                    let hand;
                    switch (keyTappingIndex % 3) {
                        case 0:
                            hand = "Both";
                            break;
                        case 1:
                            hand = "Right";
                            break;
                        case 2:
                            hand = "Left";
                            break;
                    }
                    trials.push({
                        hand: hand,
                        isPractice: false,
                    });
                    keyTappingIndex++;
                }
                break;
            case "Mooney Image":
                // if practice trial, insert easy Mooney Image
                if (i == 0 && practiceTrial == 1) {
                    trials.push({
                        imgNum: mooneyImageLogic.chooseImage("U"),
                        imgType: "U",
                        isPractice: true,
                    });
                } else {
                    const type = mooneyImageLogic.chooseType(
                        task.fields.difficulty
                    );
                    let num = mooneyImageLogic.chooseImage(type);
                    // handle the case of num = [80, 81, 82, 83] and type is "S", since there are no such imgNums
                    while (type == "S" && num < 84 && num > 79) {
                        num = mooneyImageLogic.chooseImage(type);
                    }
                    trials.push({
                        imgNum: num,
                        imgType: type,
                        isPractice: false,
                    });
                }
                break;
            case "RDK":
                trials.push({
                    direction: String(Math.round(Math.random()) * 180), // 0 is right, 180 is left
                    isPractice: i == 0 && practiceTrial == 1 ? true : false,
                });
                break;
            case "Recent Prior Normal":
                trials.push({ isPractice: false });
                break;
            case "Recent Prior Distorted":
                trials.push({ isPractice: false });
                break;
            default:
                throw new Error("Invalid Task type - " + task.type);
        }
    }
    return trials;
};
