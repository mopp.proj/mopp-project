const fs = require("fs");
const path = require("path");

const moduleName = "assets";
const { insertNewLog } = require('./log.controller');
const httpStatuses = require("./HttpStatusCodes.json");

/**
 * This function returns a .gif file by a given gifName. 
 * @param {*} req - an HTTP request.
 * @param {*} res - an HTTP response.
 * @returns String - the file path of the .gif.
 */
exports.getBMLGif = (req, res) => {
    const baseDataPath = "../data/BML gifs/";
    const difficulty = req.params.difficulty;
    const gifName = req.params.gifName;
    const gifPath = baseDataPath + difficulty+"/" + gifName;
    const serverGifPath = path.resolve(__dirname, gifPath);
    try {
        // if the file exists
        if (fs.existsSync(serverGifPath)) {
            return res                    
                    .sendFile(serverGifPath);
        } else {
            return insertNewLog(
                {
                    "module": moduleName,
                    "function": "getBMLGif",
                    "params": req.params,
                    "message": `couldn't find path ${gifPath}`,
                    "status": httpStatuses.NotFound,
                },
                res
            );
        }
    } catch (err) {
        return insertNewLog(
            {
                "module": moduleName,
                "function": "getBMLGif",
                "params": req.params,
                "message": err.message || `There was a problem finding path ${gifPath}`,
                "status": httpStatuses.InternalServerError,
            },
            res
        );
    }
};
