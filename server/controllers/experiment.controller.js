﻿const mongoose = require("mongoose");
const dotenv = require("dotenv");
dotenv.config("../.env");
const pass = process.env.PASS;

const db = require("../db");
const taskTrials = require("../BusinessLogic/tasks");
const { insertNewLog } = require("./log.controller");

const Experiment = db.experiment;
const moduleName = "experiment";
const httpStatuses = require("./HttpStatusCodes.json");

const stringify = require("csv-stringify/sync");

const VISUAL_ACUITY_TRIALS_NUM = 26;

/**
 * This function creates and saves a new Experiment to the db.
 * @param {*} req - an HTTP request.
 * @param {*} res - an HTTP response.
 * @returns Experiment - the created experiment object.
 */
exports.create = (req, res) => {
    // if some content is missing in the request
    if (!req.body.name) {
        return insertNewLog(
            {
                module: moduleName,
                function: "create",
                params: req.body,
                message: "Experiment data can't be empty",
                status: httpStatuses.BadRequest,
            },
            res
        );
    }

    const experiment = new Experiment({
        name: req.body.name,
        startDate: req.body.startDate || "",
        endDate: req.body.endDate || "",
        experimentType: req.body.experimentType || "",
        isPublished: req.body.isPublished ? req.body.isPublished : false,
        tasks: req.body.tasks,
        participants: req.body.participants,
    });

    experiment
        .save(experiment)
        .then((data) => {
            return res.send(data);
        })
        .catch((err) => {
            return insertNewLog(
                {
                    module: moduleName,
                    function: "create",
                    params: req.body,
                    message:
                        err.message ||
                        "Some error occured while creating the Experiment",
                    status: httpStatuses.InternalServerError,
                },
                res
            );
        });
};

/**
 * This function finds all experiments in db, and aggregates the number of tasks (used by ExperimentTable).
 * @param {*} req - an HTTP request.
 * @param {*} res - an HTTP response.
 * @returns List of Experiment objects.
 */
exports.findAll = (req, res) => {
    Experiment.aggregate([
        {
            $project: {
                name: "$name",
                startDate: "$startDate",
                endDate: "$endDate",
                isPublished: "$isPublished",
                participants: "$participants",
                tasksNumber: { $size: "$tasks" },
            },
        },
    ])
        .then((data) => {
            if (!data) {
                return insertNewLog(
                    {
                        module: moduleName,
                        function: "findAll",
                        params: {},
                        message: "Couldn't find experiments",
                        status: httpStatuses.BadRequest,
                    },
                    res
                );
            } else {
                // calculating the number of participants and the number of completed participants
                data.forEach((exp) => {
                    (exp.numOfParticipants = exp.participants.length),
                        (exp.numOfCompletedParticipants =
                            calcNumberOfCompletedParticipants(
                                exp.participants
                            ));
                    delete exp.participants;
                });

                return res.send(data);
            }
        })
        .catch((err) => {
            return insertNewLog(
                {
                    module: moduleName,
                    function: "findAll",
                    params: {},
                    message: err.message || "Error retrieving experiments",
                    status: httpStatuses.InternalServerError,
                },
                res
            );
        });
};

/**
 * This function validates the given password, in order to show experiments only to valid researcher
 * @param {*} req
 * @param {*} res
 * @returns boolean - true if the passowrd is valid.
 */
exports.validatePassword = (req, res) => {
    if (req.query.password === pass) {
        res.send({ valid: true });
    } else {
        return insertNewLog(
            {
                module: moduleName,
                function: "validatePassword",
                params: req.query,
                message: "Invalid Password",
                status: httpStatuses.BadRequest,
            },
            res
        );
    }
};

/**
 * This function finds a single Experiment by a given id.
 * @param {*} req - an HTTP request.
 * @param {*} res - an HTTP response.
 * @returns Experiment object.
 */
exports.findOne = (req, res) => {
    const id = req.params.id;

    Experiment.findById(id, {
        name: 1,
        startDate: 1,
        endDate: 1,
        experimentType: 1,
        passcode: 1,
        isPublished: 1,
        participantsUrlId: 1,
        tasks: {
            order: 1,
            type: 1,
            generalFields: 1,
            fields: 1,
        },
        participants: 1,
        paymentCode: 1
    })
        .lean()
        .then((data) => {
            if (!data) {
                return insertNewLog(
                    {
                        module: moduleName,
                        function: "findOne",
                        params: req.params,
                        message: "Experiment not found",
                        status: httpStatuses.NotFound,
                    },
                    res
                );
            } else {
                // calculating the number of participants and the number of completed participants
                data.participants = {
                    numOfParticipants: data.participants.length,
                    numOfCompletedParticipants:
                        calcNumberOfCompletedParticipants(data.participants),
                };

                return res.send(data);
            }
        })
        .catch((err) => {
            return insertNewLog(
                {
                    module: moduleName,
                    function: "findOne",
                    params: req.params,
                    message:
                        err.message ||
                        `Error retrieving experiment with id=${id}`,
                    status: httpStatuses.InternalServerError,
                },
                res
            );
        });
};

/**
 * This function gets an array of participants and returns the number of participants who completed the experiment.
 * @param {Participant} participants
 * @returns Number - the number of participants who completed the experiment.
 */
const calcNumberOfCompletedParticipants = function (participants) {
    let participantsWhoCompleted = 0;
    participants.forEach((participant) => {
        // if the participant has finished the last virtualChinerest task
        if (participant.virtualChinrestEnd) {
            participantsWhoCompleted += 1;
        }
    });
    return participantsWhoCompleted;
};

/**
 * This function updates a single Experiment by a given id, but only if the experiment is not already published.
 * @param {*} req - an HTTP request.
 * @param {*} res - an HTTP response.
 * @returns String - a message when the experiment was updated successfully.
 */
exports.update = (req, res) => {
    // if the update request is empty
    if (!req.body) {
        return insertNewLog(
            {
                module: moduleName,
                function: "update",
                params: {
                    params: req.params,
                    body: req.body,
                },
                message: "Could not update experiment with empty data",
                status: httpStatuses.BadRequest,
            },
            res
        );
    }

    // find the experiment by the given id
    const id = req.params.id;
    Experiment.findById(id)
        .then((data) => {
            if (!data) {
                return insertNewLog(
                    {
                        module: moduleName,
                        function: "update",
                        params: {
                            params: req.params,
                            body: req.body,
                        },
                        message: "Experiment not found",
                        status: httpStatuses.NotFound,
                    },
                    res
                );
            }
            // if the experiment is already published
            if (data.isPublished) {
                return insertNewLog(
                    {
                        module: moduleName,
                        function: "update",
                        params: {
                            params: req.params,
                            body: req.body,
                        },
                        message: "Experiment is already published",
                        status: httpStatuses.BadRequest,
                    },
                    res
                );
            }
            // update the experiment by the given request
            Experiment.findByIdAndUpdate(id, req.body, {
                useFindAndModify: false,
            })
                .then((data) => {
                    if (!data) {
                        return insertNewLog(
                            {
                                module: moduleName,
                                function: "update",
                                params: {
                                    params: req.params,
                                    body: req.body,
                                },
                                message: "Could not update experiment",
                                status: httpStatuses.BadRequest,
                            },
                            res
                        );
                    }

                    return res.send({
                        message: "Experiment upadted successfully",
                    });
                })
                .catch((err) => {
                    return insertNewLog(
                        {
                            module: moduleName,
                            function: "update",
                            params: {
                                params: req.params,
                                body: req.body,
                            },
                            message:
                                err.message ||
                                `Error updating experiment with id=${id}`,
                            status: httpStatuses.InternalServerError,
                        },
                        res
                    );
                });
        })
        .catch((err) => {
            return insertNewLog(
                {
                    module: moduleName,
                    function: "update",
                    params: {
                        params: req.params,
                        body: req.body,
                    },
                    message:
                        err.message ||
                        `Could not find experiment with id=${id}`,
                    status: httpStatuses.InternalServerError,
                },
                res
            );
        });
};

/**
 * This function publishes a single Experiment by a given id, but only if the experiment is not already published.
 * @param {*} req - an HTTP request.
 * @param {*} res - an HTTP response.
 * @returns String - a message when the experiment was published successfully.
 */
exports.publish = (req, res) => {
    // find the experiment by the given id
    const id = req.params.id;
    Experiment.findById(id)
        .then((data) => {
            if (!data) {
                return insertNewLog(
                    {
                        module: moduleName,
                        function: "publish",
                        params: req.params,
                        message: "Experiment not found",
                        status: httpStatuses.NotFound,
                    },
                    res
                );
            }
            // if the experiment is already published
            if (data.isPublished) {
                return insertNewLog(
                    {
                        module: moduleName,
                        function: "publish",
                        params: req.params,
                        message: "Experiment is already published",
                        status: httpStatuses.BadRequest,
                    },
                    res
                );
            }
            let tasks = data.tasks;
            // for each task of the experiment
            tasks.forEach((task) => {
                // create trials data
                let trials = taskTrials.createTaskTrials(task);
                // save the trials of the task to the db
                Experiment.updateOne(
                    {
                        _id: id,
                        "tasks._id": task._id,
                    },
                    { $set: { "tasks.$.trials": trials } }
                ).catch((err) => {
                    return insertNewLog(
                        {
                            module: moduleName,
                            function: "publish",
                            params: req.params,
                            message:
                                err.message ||
                                `Error updating trials of task with id=${task._id}`,
                            status: httpStatuses.InternalServerError,
                        },
                        res
                    );
                });
            });

            // update that the experiment is published
            const publishData = {
                isPublished: true,
                passcode: Math.random().toString(36).slice(-10),
                participantsUrlId: new mongoose.Types.ObjectId(),
            };
            // update the experiment with data of trials
            Experiment.findByIdAndUpdate(id, publishData, {
                useFindAndModify: false,
            })
                .then((data) => {
                    if (!data) {
                        return insertNewLog(
                            {
                                module: moduleName,
                                function: "publish",
                                params: req.params,
                                message: "Experiment could not be published",
                                status: httpStatuses.BadRequest,
                            },
                            res
                        );
                    }
                    return res.send({
                        participantsUrlId: publishData.participantsUrlId,
                        passcode: publishData.passcode,
                        message: "Experiment is published" + id,
                    });
                })
                .catch((err) => {
                    return insertNewLog(
                        {
                            module: moduleName,
                            function: "publish",
                            params: req.params,
                            message:
                                err.message ||
                                `Error publishing experiment with id=${id}`,
                            status: httpStatuses.InternalServerError,
                        },
                        res
                    );
                });
        })
        .catch((err) => {
            return insertNewLog(
                {
                    module: moduleName,
                    function: "publish",
                    params: req.params,
                    message:
                        err.message ||
                        `Error retrieving experiment with id=${id}`,
                    status: httpStatuses.InternalServerError,
                },
                res
            );
        });
};

/**
 * This function deletes a single Experiment by a given id.
 * @param {*} req - an HTTP request.
 * @param {*} res - an HTTP response.
 * @returns String - a message when the experiment was deleted successfully.
 */
exports.delete = (req, res) => {
    const id = req.params.id;

    Experiment.deleteOne({ _id: id })
        .then((data) => {
            if (!data) {
                return insertNewLog(
                    {
                        module: moduleName,
                        function: "delete",
                        params: req.params,
                        message: "Experiment not found",
                        status: httpStatuses.NotFound,
                    },
                    res
                );
            }
            return res.send({ message: "Experiment deleted successfully" });
        })
        .catch((err) => {
            return insertNewLog(
                {
                    module: moduleName,
                    function: "delete",
                    params: req.params,
                    message:
                        err.message ||
                        `Error retrieving experiment with id=${id}`,
                    status: httpStatuses.InternalServerError,
                },
                res
            );
        });
};

/**
 * This function returns the experiment data in a csv data, by a given experiment id.
 * @param {*} req - an HTTP request.
 * @param {*} res - an HTTP response.
 * @returns List of csv strings.
 */
exports.downloadOne = (req, res) => {
    // find the experiemnt
    const id = req.params.id;
    Experiment.findById(id)
        .then((data) => {
            // specify the columns of the experiment
            let columns = [];
            columns.push("id");
            // get the columns of the survey
            let questions = getSurveyQuestions(data);
            questions.forEach((question) => {
                columns.push(`${question.pageID}.${question.questionName}`);
            });
            // get the columns of the virtualChinrest tasks
            let vcColumns = getVirtualChinrestColumns();
            vcColumns.forEach((col) => {
                columns.push(`${col.task}.${col.colName}`);
            });
            // get the columns of the visualAcuity task
            let vaColumns = getVisualAcuityColumns();
            // get the columns of the different trials
            let trials = getExperimentTrials(data);
            let answers = [columns.concat(vaColumns).concat(trials)];

            let id = 1;
            // for each participant who started the experiment
            data.participants.forEach((participant) => {
                // if the participant has finished the experiment
                if (participant.virtualChinrestEnd) {
                    let answer = [];
                    answer.push(id);
                    // get answers for the survey
                    questions.forEach((question) => {
                        answer.push(
                            question.questionName in participant.surveyResults
                                ? participant.surveyResults[
                                    question.questionName
                                ].toString()
                                : ""
                        );
                    });

                    // get answers for the VirtualChinrest tasks
                    vcColumns.forEach((vc) => {
                        answer.push(participant[vc.task][vc.colName]);
                    });

                    // get answers for the visualAcuity task
                    participant.visualAcuity.forEach((va, idx) => {
                        if (idx < VISUAL_ACUITY_TRIALS_NUM) {
                            answer.push(
                                va["vaValue"],
                                va["isFreeStimulation"].toString(),
                                va["rightAnswer"],
                                va["answer"],
                                va["rt"]
                            );
                        }
                    });

                    // get answers for the different trials
                    participant.experimentAnswers.forEach((ans) => {
                        // if the trial is not a practice trial
                        if (!ans.isPractice) {
                            let rightAnswer = getTrialRightAnswer(
                                data.tasks[ans.taskOrder].trials[ans.trialNum],
                                ans.taskType
                            );
                            answer.push(
                                rightAnswer,
                                new Date(ans.stimulusStartDisplayTime).toISOString(),
                                new Date(ans.responseStartDisplayTime).toISOString(),
                                ans.answer,
                                ans.responseTime
                            );
                        }
                    });

                    answers.push(answer);
                    id++;
                }
            });

            return res.send(stringify.stringify(answers));
        })
        .catch((err) => {
            return insertNewLog(
                {
                    module: moduleName,
                    function: "downloadOne",
                    params: req.params,
                    message:
                        err.message ||
                        `Error retrieving experiment with id=${id}`,
                    status: httpStatuses.InternalServerError,
                },
                res
            );
        });
};

/**
 * This function returns the columns of the survey's questions by a given Experiment object.
 * @param {Experiment} experiment - an Experiment object.
 * @returns List of objects.
 */
getSurveyQuestions = (experiment) => {
    let questions = [];
    // for each page in the survey
    experiment.surveyJson.pages.forEach((page, id) => {
        // for each question in the page
        page.elements.forEach((element) => {
            questions.push({
                pageID: id,
                questionName: element.name,
            });
        });
    });

    return questions;
};

/**
 * This function returns the columns of the experiment's trials by a given Experiment object.
 * @param {Experiment} experiment - an Experiment object.
 * @returns List of csv strings.
 */
getExperimentTrials = (experiment) => {
    let trials = [];
    // for each task in the experiment
    experiment.tasks.forEach((task) => {
        // for each trial of the task
        for (let i = 0; i < task.generalFields.trialNum; i++) {
            let prefix = `${task.order}.${task.type}.${i}`;
            let answerType =
                task.type === "Keyboard Tapping" ? "hand" : "rightAnswer";
            trials.push(
                `${prefix}.${answerType}`,
                `${prefix}.stimulusStartDisplayTime`,
                `${prefix}.responseStartDisplayTime`,
                `${prefix}.answer`,
                `${prefix}.rt`
            );
        }
    });

    return trials;
};

/**
 * This function returns the columns of virtualChinrest tasks.
 * @returns List of objects.
 */
getVirtualChinrestColumns = () => {
    const tasks = ["virtualChinrestStart", "virtualChinrestEnd"];
    const cols = ["view_dist_mm", "scale_factor"];

    let vcCols = [];
    tasks.forEach((task) => {
        cols.forEach((col) => {
            vcCols.push({
                task: task,
                colName: col,
            });
        });
    });

    return vcCols;
};

/**
 * This function returns the columns of visualAcuity task.
 * @returns List of objects.
 */
getVisualAcuityColumns = () => {
    let trials = [];
    for (let i = 0; i < VISUAL_ACUITY_TRIALS_NUM; i++) {
        trials.push(`visualAcuity.${i}.vaValue`);
        trials.push(`visualAcuity.${i}.isFreeStimulation`);
        trials.push(`visualAcuity.${i}.rightAnswer`);
        trials.push(`visualAcuity.${i}.answer`);
        trials.push(`visualAcuity.${i}.rt`);
    }
    return trials;
};

/**
 * This function returns the real trial answer.
 * @param trialData - the data of the trial.
 * @param taskType - type of task.
 * @returns String - the real trial answer.
 */
getTrialRightAnswer = (trialData, taskType) => {
    let rightAnswer = "";
    switch (taskType) {
        case "Biological Motion":
            rightAnswer = trialData["gifType"];
            break;
        case "Keyboard Tapping":
            rightAnswer = trialData["hand"];
            break;
        case "Line Length":
            rightAnswer = trialData["LengthOfLine"];
            break;
        case "Mooney Image":
            rightAnswer = trialData["imgType"];
            break;
        case "Numerosity":
            rightAnswer = trialData["numberOfObjects"];
            break;
        case "RDK":
            rightAnswer = trialData["direction"];
            break;
        case "Recent Prior Normal":
            rightAnswer = trialData["imgType"];
            break;
        case "Recent Prior Distorted":
            rightAnswer = trialData["imgType"];
            break;
        default:
            break;
    }
    return rightAnswer;
};
