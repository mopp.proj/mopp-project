const fs = require("fs");
const path = require("path");

const dotenv = require("dotenv");
dotenv.config("../.env");

const db = require("../db");
const Image = db.image;
const links = require("../../client/src/Utils/Links");
const imagesURL = links.ImagesURL;

const moduleName = "image";
const httpStatuses = require("./HttpStatusCodes.json");
const { insertNewLog } = require("./log.controller");

let parentDir = __dirname.split(path.sep)
parentDir = parentDir.slice(0, parentDir.length - 1).join(path.sep);
const UPLOADS_DIR = [parentDir, 'data', 'Mooney Images', 'uploads'].join(path.sep);

/**
 * This function uploads images from uploads dir to DB.
 * @param {*} req - an HTTP request.
 * @param {*} res - an HTTP response.
 * @returns String - a message when the files were uploaded successfully.
 */
exports.uploadImages = (req, res) => {
    // for each file which should be uploaded
    req.files.forEach((file) => {        
        let img = {
            // parse image type & num from the filename (e.g.: I0120.bmp -> num: 120 type: I)
            type: file.originalname[0],
            num: file.originalname.replace(/(^[A-Z]{1}0{1,})|(\.bmp)/g, ""),
            data: fs.readFileSync([UPLOADS_DIR, file.originalname].join(path.sep)),
            contentType: "image/bmp",
            url: `${imagesURL}/${
                file.originalname[0]
            }/${file.originalname.replace(/(^[A-Z]{1}0{1,})|(\.bmp)/g, "")}`,
        };
        
        Image.findOneAndUpdate(
            {
                "type": img.type,
                "num": img.num,
            },
            img,
            {
                "upsert": true,
                "new": true,
                "setDefaultsOnInsert": true,
            }
        )
            .then((data) => {                
                return res.send("file was uploaded successfully");
            })
            .catch((err) => {
                return insertNewLog(
                    {
                        "module": moduleName,
                        "function": "uploadImages",
                        "params": req.files,
                        "message": err.message || "Some error occured while uploading the images.",
                        "status": httpStatuses.InternalServerError,
                    },
                    res
                );
            });
    });
};

/**
 * This function uploads list of image files from disk
 * @param {[String]} files - list of file's path
 */
exports.uploadImagesFromDisk = (files) => {
    files.forEach((file) => {
        let filePath = [UPLOADS_DIR, file.originalname].join(path.sep);
        
        try {
            let img = {
                // parse image type & num from the filename (e.g.: I0120.bmp -> num: 120 type: I)
                type: file.originalname[0],
                num: file.originalname.replace(/(^[A-Z]{1}0{1,})|(\.bmp)/g, ""),
                data: fs.readFileSync(filePath),
                contentType: "image/bmp",
                url: `${imagesURL}/${
                    file.originalname[0]
                }/${file.originalname.replace(/(^[A-Z]{1}0{1,})|(\.bmp)/g, "")}`,
            };
            // upload the image file to the db
            Image.findOneAndUpdate(
                {
                    "type": img.type,
                    "num": img.num,
                },
                img,
                {
                    "upsert": true,
                    "new": true,
                    "setDefaultsOnInsert": true,
                }
            )
                .then(
                    // delete the file from disk
                    fs.unlink(filePath, (err) => {
                        if (err) {
                            insertNewLog(
                                {
                                    "module": moduleName,
                                    "function": "uploadImagesFromDisk.unlink",
                                    "params": {
                                        "filePath": filePath,
                                    },
                                    "message": err || "Some error occured while unlinking the image.",
                                    "status": "",
                                },                    
                            );
                        }
                    })
                )
                .catch((err) => {
                    insertNewLog(
                        {
                            "module": moduleName,
                            "function": "uploadImagesFromDisk",
                            "params": req.files,
                            "message": err.message || "Some error occured while uploading the images.",
                            "status": httpStatuses.InternalServerError,
                        },                    
                    );
                });
        } catch (error) {
            insertNewLog(
                {
                    "module": moduleName,
                    "function": "uploadImagesFromDisk.readFileSync",
                    "params": {
                        "filePath": filePath,
                    },
                    "message": error || "Some error occured while reading the images from disk",
                    "status": "",
                },                    
            );
        }
        
        
    });
}

/**
 * This function returns an image by a given parameters.
 * @param {*} req - an HTTP request.
 * @param {*} res - an HTTP response.
 * @returns base64 image.
 */
exports.getImage = (req, res) => {
    Image.findOne(
        { type: req.params.imgType, num: req.params.imgNum },
        (err, item) => {
            if (err) {
                return insertNewLog(
                    {
                        "module": moduleName,
                        "function": "getImage",
                        "params": req.params,
                        "message": err.message,
                        "status": httpStatuses.InternalServerError,
                    },
                    res
                );
            } else {
                return res.send(item.data.toString("base64"));
            }
        }
    );
};
