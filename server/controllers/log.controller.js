const db = require("../db");
const Log = db.log;

/**
 * This function inserts a new log entry to the db.
 * @param {*} req - an HTTP request.
 * @param {*} res - an HTTP response.
 * @returns empty object - {}.
 */
exports.insertNewLog = (req, res) => {
    if (!req) {
        console.log("log cannot be empty!");
    }    
    
    const log = new Log({
        module: req.module,
        function: req.function,
        params: req.params,
        message: req.message,
        status: req.status,
        statusCode: req.statusCode,
    });    
    
    log
        .save(log)        
        // .then((data) => {
        //     return res.send(data);
        // })
        .catch((err) => {
            console.error(err.message || "Some error occured while creating the log.");
        });
        if (res) {
            return res
                .status(req.status)
                .send({ message: req.message });
        }
        return {};
}