const mongoose = require("mongoose");

const db = require("../db");
const Experiment = db.experiment;

const moduleName = "participant";
const { insertNewLog } = require("./log.controller");
const httpStatuses = require("./HttpStatusCodes.json");

/**
 * This function checks if a given participantsUrlID exists.
 * @param {*} req - an HTTP request.
 * @param {*} res - an HTTP response.
 * @returns Boolean - true is exists, false is not.
 */
exports.exists = (req, res) => {
    const participantsUrlID = req.params.id;
    // try to find an experiment with the given participantsUrlID
    Experiment.find({
        participantsUrlId: participantsUrlID,
    })
        .then((data) => {
            if (data.length === 0) {
                return insertNewLog(
                    {
                        module: moduleName,
                        function: "exists",
                        params: req.params,
                        message: "ParticipantUrlID was not found",
                        status: httpStatuses.NotFound,
                    },
                    res
                );
            } else {
                return res.send({
                    exists: true,
                    paymentCode:data[0].paymentCode
                });
            }
        })
        .catch((err) => {
            return insertNewLog(
                {
                    module: moduleName,
                    function: "exists",
                    params: req.params,
                    message:
                        err.message ||
                        `Error retrieving ParticipantUrlID with id=${participantsUrlID}`,
                    status: httpStatuses.InternalServerError,
                },
                res
            );
        });
};

/**
 * This function returns a participant object by a given participandID and participantsUrlID.
 * @param {*} req - an HTTP request.
 * @param {*} res - an HTTP response.
 * @returns Participant object.
 */
exports.findOne = (req, res) => {
    const participantsUrlID = req.params.id;
    const participantID = req.params.participantID;

    // try to find a participant with the given participantID
    // in the experiment with the given participantsUrlID
    Experiment.aggregate([
        {
            $match: {
                participantsUrlId: mongoose.Types.ObjectId(participantsUrlID),
                participants: {
                    $elemMatch: {
                        _id: mongoose.Types.ObjectId(participantID),
                    },
                },
            },
        },
        { $unwind: "$participants" },
        {
            $match: {
                "participants._id": mongoose.Types.ObjectId(participantID),
            },
        },
    ])
        .then((data) => {
            // if there is not participans with the given participandID in the experiment
            // with the given participantsUrlID
            if (data.length === 0) {
                return insertNewLog(
                    {
                        module: moduleName,
                        function: "findOne",
                        params: req.params,
                        message:
                            "ParticipantUrlID or ParticipantID was not found",
                        status: httpStatuses.NotFound,
                    },
                    res
                );
            } else {
                return res.send(data[0]);
            }
        })
        .catch((err) => {
            return insertNewLog(
                {
                    module: moduleName,
                    function: "findOne",
                    params: req.params,
                    message:
                        err.message ||
                        `Error retrieving ParticipantUrlID with id=${participantsUrlID} or ParticipantID with id=${participantID}`,
                    status: httpStatuses.InternalServerError,
                },
                res
            );
        });
};

/**
 * This function returns a participant object by a given participandID and participantsUrlID.
 * @param {*} req - an HTTP request.
 * @param {*} res - an HTTP response.
 * @returns Participant object.
 */
 exports.findNextTrial = (req, res) => {
    const participantsUrlID = req.params.id;
    const participantID = req.params.participantID;    

    // try to find a participant with the given participantID
    // in the experiment with the given participantsUrlID
    Experiment.aggregate([
        {
            $match: {
                participantsUrlId: mongoose.Types.ObjectId(participantsUrlID),
                participants: {
                    $elemMatch: {
                        _id: mongoose.Types.ObjectId(participantID),
                    },
                },
            },
        },
        { $unwind: "$participants" },
        {
            $match: {
                "participants._id": mongoose.Types.ObjectId(participantID),
            },
        },
    ])
        .then((data) => {
            // if there is not participans with the given participandID in the experiment
            // with the given participantsUrlID
            if (data.length === 0) {
                return insertNewLog(
                    {
                        module: moduleName,
                        function: "findOne",
                        params: req.params,
                        message:
                            "ParticipantUrlID or ParticipantID was not found",
                        status: httpStatuses.NotFound,
                    },
                    res
                );
            } else {
                let trialNum = data[0]["participants"]["experimentAnswers"].length;                
                
                const tasks = data[0]["tasks"];                
                let trial = {};
                tasks.every((task, taskIdx) => {                    
                    // if trialNum is bigger than num of trials in current task
                    if (trialNum >= task["trials"].length) {
                        // decrease trialNum
                        trialNum -= task["trials"].length;
                        return true;
                    } else {
                        trial = {
                            "taskOrder": taskIdx,
                            "taskType": task["type"],
                            "trialNum": trialNum,
                            "numOfTasks": tasks.length,
                            "numOfTrials": task["trials"].length,
                            "showDescription": trialNum == 0 ? true : false,
                            "isLast": (taskIdx + 1 == tasks.length && trialNum + 1 == task["trials"].length) ? true : false,
                            "generalFields": task["generalFields"],                            
                            "taskFields": task["fields"],
                            "virtualChinrest": data[0]["participants"]["virtualChinrestStart"],
                            "trialData": task["trials"][trialNum],
                            
                        }
                        return false;
                    }
                });

                return res.send(trial);
            }
        })
        .catch((err) => {
            return insertNewLog(
                {
                    module: moduleName,
                    function: "findOne",
                    params: req.params,
                    message:
                        err.message ||
                        `Error retrieving ParticipantUrlID with id=${participantsUrlID} or ParticipantID with id=${participantID}`,
                    status: httpStatuses.InternalServerError,
                },
                res
            );
        });
};

/**
 * This function returns the experiment type by of an experiment by a given participantsUrlID.
 * @param {*} req - an HTTP request.
 * @param {*} res - an HTTP response.
 * @returns String - type of the experiment.
 */
exports.getExperimentType = (req, res) => {
    const participantsUrlID = req.params.id;

    Experiment.find(
        {
            participantsUrlId: participantsUrlID,
        },
        {
            experimentType: 1,
        }
    )
        .then((data) => {
            // if there is no experiment with the given participantsUrlID
            if (!data) {
                return insertNewLog(
                    {
                        module: moduleName,
                        function: "getExperimentType",
                        params: req.params,
                        message: "ParticipantUrlID was not found",
                        status: httpStatuses.NotFound,
                    },
                    res
                );
            } else {
                return res.send(data);
            }
        })
        .catch((err) => {
            return insertNewLog(
                {
                    module: moduleName,
                    function: "getExperimentType",
                    params: req.params,
                    message:
                        err.message ||
                        `Error retrieving ParticipantUrlID with id=${participantsUrlID}`,
                    status: httpStatuses.InternalServerError,
                },
                res
            );
        });
};

/**
 * This function validates the credentials of the participant, and checks if they didn't already participated
 * in the experiment with the given participantsUrlID
 * @param {*} req - an HTTP request.
 * @param {*} res - an HTTP response.
 * @returns request for adding a new participant - only when the participant is valid.
 */
exports.validateAndInsertNewParticipant = (req, res) => {
    const participantsUrlID = req.params.id;

    // if it's a supervised experiment and the participant authenticate using a passcode
    if (
        req.body.experimentType === "Supervised" &&
        req.body.connectionType === "Passcode"
    ) {
        Experiment.findOne({
            participantsUrlId: participantsUrlID,
            passcode: req.body.authID,
        })
            .then((data) => {
                // if the passcode is not valid
                if (data === null) {
                    return insertNewLog(
                        {
                            module: moduleName,
                            function: "validateAndInsertNewParticipant",
                            params: {
                                params: req.params,
                                body: req.body,
                            },
                            message: `The given passcode ${req.body.authID} is not valid for experiment 
                            with participantsUrlId=${participantsUrlID}`,
                            status: httpStatuses.BadRequest,
                        },
                        res
                    );
                } else {
                    return addNewParticipant(req, res);
                }
            })
            .catch((err) => {
                return insertNewLog(
                    {
                        module: moduleName,
                        function: "validateAndInsertNewParticipant",
                        params: {
                            params: req.params,
                            body: req.body,
                        },
                        message:
                            err.message ||
                            `Error retrieving ParticipantUrlID with id=${participantsUrlID}`,
                        status: httpStatuses.InternalServerError,
                    },
                    res
                );
            });
    } else {
        // if the participant was authenticated using one of the platforms
        // check if the authentication is unique,
        // and that no one participated from a computer with the given clientIP
        Experiment.findOne({
            participantsUrlId: participantsUrlID,
            $or: [
                {
                    "participants.connectionType": req.body.connectionType,
                    "participants.authID": req.body.authID,
                },
                {
                    "participants.clientIP": req.body.clientIP,
                },
            ],
        })
            .then((data) => {
                // if the participant is already participated in the experiment
                if (data) {
                    return insertNewLog(
                        {
                            module: moduleName,
                            function: "validateAndInsertNewParticipant",
                            params: {
                                params: req.params,
                                body: req.body,
                            },
                            message: `Participant with authID=${req.body.authID} already connected using ${req.body.connectionType}, 
                            or with clientIP ${req.body.clientIP} in order to answer participantsUrlID=${participantsUrlID}`,
                            status: httpStatuses.Forbidden,
                        },
                        res
                    );
                } else {
                    return addNewParticipant(req, res);
                }
            })
            .catch((err) => {
                return insertNewLog(
                    {
                        module: moduleName,
                        function: "validateAndInsertNewParticipant",
                        params: {
                            params: req.params,
                            body: req.body,
                        },
                        message:
                            err.message ||
                            `Error retrieving ParticipantUrlID with id=${participantsUrlID}`,
                        status: httpStatuses.InternalServerError,
                    },
                    res
                );
            });
    }
};

/**
 * This function adds a new participant record to the list of participants in the experiment
 * with the given participantsUrlID.
 * @param {*} req - an HTTP request.
 * @param {*} res - an HTTP response.
 * @returns ObjectID - the id of the created participant.
 */
addNewParticipant = (req, res) => {
    // create a new participant for the current experiment
    const participantsUrlID = req.params.id;
    const participantID = mongoose.Types.ObjectId();
    Experiment.findOneAndUpdate(
        { participantsUrlId: participantsUrlID },
        {
            $push: {
                participants: {
                    _id: participantID,
                    connectionType: req.body.connectionType,
                    authID: req.body.authID,
                    clientIP: req.body.clientIP,
                    experimentAnswers: [],
                },
            },
        }
    )
        .then((data) => {
            if (!data) {
                return insertNewLog(
                    {
                        module: moduleName,
                        function: "addNewParticipant",
                        params: {
                            params: req.params,
                            body: req.body,
                        },
                        message: "ParticipantUrlID not found",
                        status: httpStatuses.NotFound,
                    },
                    res
                );
            } else {
                return res.send({
                    participantID: participantID,
                });
            }
        })
        .catch((err) => {
            return insertNewLog(
                {
                    module: moduleName,
                    function: "addNewParticipant",
                    params: {
                        params: req.params,
                        body: req.body,
                    },
                    message:
                        err.message ||
                        `Error retrieving ParticipantUrlID with id=${participantsUrlID}`,
                    status: httpStatuses.InternalServerError,
                },
                res
            );
        });
};

/**
 * This function adds a new answer for the participant record in the experiment with
 * the given participantID and participantsUrlID.
 * @param {*} req - an HTTP request.
 * @param {*} res - an HTTP response.
 * @returns String - a message when the update command was successful.
 */
exports.insertParticipantAnswer = (req, res) => {
    const participantsUrlID = req.params.id;
    const participantID = req.params.participantID;

    Experiment.findOneAndUpdate(
        {
            participantsUrlId: participantsUrlID,
            "participants._id": participantID,
        },
        {
            $push: {
                "participants.$.experimentAnswers": {
                    taskOrder: req.body.taskOrder,
                    taskType: req.body.taskType,
                    trialNum: req.body.trialNum,
                    answer: req.body.answer,
                    responseTime: req.body.responseTime,
                    responseStartDisplayTime: req.body.responseStartDisplayTime,
                    stimulusStartDisplayTime: req.body.stimulusStartDisplayTime,
                    isPractice: req.body.isPractice,
                },
            },
        }
    )
        .then((data) => {
            if (!data) {
                return insertNewLog(
                    {
                        module: moduleName,
                        function: "insertParticipantAnswer",
                        params: {
                            params: req.params,
                            body: req.body,
                        },
                        message: "ParticipantUrlID or participantID not found",
                        status: httpStatuses.NotFound,
                    },
                    res
                );
            } else {
                return res.send({
                    message: `Pushed new answer for participant.id=${participantID} in participantUrlID=${participantsUrlID}`,
                });
            }
        })
        .catch((err) => {
            return insertNewLog(
                {
                    module: moduleName,
                    function: "insertParticipantAnswer",
                    params: {
                        params: req.params,
                        body: req.body,
                    },
                    message:
                        err.message ||
                        `Error retrieving ParticipantUrlID with id=${participantsUrlID} or participant.id=${participantID}`,
                    status: httpStatuses.InternalServerError,
                },
                res
            );
        });
};

/**
 * This function updates the visualAcutiy task's answer for a participant,
 * by a given participantID and participantsUrlID.
 * @param {*} req - an HTTP request.
 * @param {*} res - an HTTP response.
 * @returns String - a message when the update command was successful.
 */
exports.setParticipantVisualAcuity = (req, res) => {
    const participantsUrlID = req.params.id;
    const participantID = req.params.participantID;

    Experiment.findOneAndUpdate(
        {
            participantsUrlId: participantsUrlID,
            "participants._id": participantID,
        },
        {
            $set: {
                "participants.$.visualAcuity": req.body.answers,
            },
        }
    )
        .then((data) => {
            if (!data) {
                return insertNewLog(
                    {
                        module: moduleName,
                        function: "setParticipantVisualAcuity",
                        params: {
                            params: req.params,
                            body: req.body,
                        },
                        message: "ParticipantUrlID or participantID not found",
                        status: httpStatuses.NotFound,
                    },
                    res
                );
            } else {
                return res.send({
                    message: `Updated visualAcuity data for participant.id=${participantID} 
                        in participantUrlID=${participantsUrlID}`,
                });
            }
        })
        .catch((err) => {
            return insertNewLog(
                {
                    module: moduleName,
                    function: "setParticipantVisualAcuity",
                    params: {
                        params: req.params,
                        body: req.body,
                    },
                    message:
                        err.message ||
                        `Error retrieving ParticipantUrlID with id=${participantsUrlID} or participant.id=${participantID}`,
                    status: httpStatuses.InternalServerError,
                },
                res
            );
        });
};

/**
 * This function updates the data of the first virtualChinrest task for a participant,
 * by a given participantID and participantsUrlID.
 * @param {*} req - an HTTP request.
 * @param {*} res - an HTTP response.
 * @returns String - a message when the update command was successful.
 */
exports.setParticipantVirtualChinrestStart = (req, res) => {
    const participantsUrlID = req.params.id;
    const participantID = req.params.participantID;

    Experiment.findOneAndUpdate(
        {
            participantsUrlId: participantsUrlID,
            "participants._id": participantID,
        },
        {
            $set: {
                "participants.$.virtualChinrestStart": req.body,
            },
        }
    )
        .then((data) => {
            if (!data) {
                return insertNewLog(
                    {
                        module: moduleName,
                        function: "setParticipantVirtualChinrestStart",
                        params: {
                            params: req.params,
                            body: req.body,
                        },
                        message: "ParticipantUrlID or participantID not found",
                        status: httpStatuses.NotFound,
                    },
                    res
                );
            } else {
                return res.send({
                    message: `Updated virtualChinrestStart data for participant.id=${participantID} 
                        in participantUrlID=${participantsUrlID}`,
                });
            }
        })
        .catch((err) => {
            return insertNewLog(
                {
                    module: moduleName,
                    function: "setParticipantVirtualChinrestStart",
                    params: {
                        params: req.params,
                        body: req.body,
                    },
                    message:
                        err.message ||
                        `Error retrieving ParticipantUrlID with id=${participantsUrlID} or participant.id=${participantID}`,
                    status: httpStatuses.InternalServerError,
                },
                res
            );
        });
};

/**
 * This function updates the data of the second virtualChinrest task for a participant,
 * by a given participantID and participantsUrlID.
 * @param {*} req - an HTTP request.
 * @param {*} res - an HTTP response.
 * @returns String - a message when the update command was successful.
 */
exports.setParticipantVirtualChinrestEnd = (req, res) => {
    const participantsUrlID = req.params.id;
    const participantID = req.params.participantID;

    Experiment.findOneAndUpdate(
        {
            participantsUrlId: participantsUrlID,
            "participants._id": participantID,
        },
        {
            $set: {
                "participants.$.virtualChinrestEnd": req.body,
            },
        }
    )
        .then((data) => {
            if (!data) {
                return insertNewLog(
                    {
                        module: moduleName,
                        function: "setParticipantVirtualChinrestEnd",
                        params: {
                            params: req.params,
                            body: req.body,
                        },
                        message: "ParticipantUrlID or participantID not found",
                        status: httpStatuses.NotFound,
                    },
                    res
                );
            } else {
                return res.send({
                    message: `Updated virtualChinrestEnd data for participant.id=${participantID} 
                        in participantUrlID=${participantsUrlID}`,
                });
            }
        })
        .catch((err) => {
            return insertNewLog(
                {
                    module: moduleName,
                    function: "setParticipantVirtualChinrestEnd",
                    params: {
                        params: req.params,
                        body: req.body,
                    },
                    message:
                        err.message ||
                        `Error retrieving ParticipantUrlID with id=${participantsUrlID} or participant.id=${participantID}`,
                    status: httpStatuses.InternalServerError,
                },
                res
            );
        });
};

/**
 * This function updates the answers of the survey for a participant,
 * by a given participantID and participantsUrlID.
 * @param {*} req - an HTTP request.
 * @param {*} res - an HTTP response.
 * @returns String - a message when the update command was successful.
 */
exports.setParticipantSurveyResults = (req, res) => {
    const participantsUrlID = req.params.id;
    const participantID = req.params.participantID;

    Experiment.findOneAndUpdate(
        {
            participantsUrlId: participantsUrlID,
            "participants._id": participantID,
        },
        {
            $set: {
                "participants.$.surveyResults": req.body,
            },
        }
    )
        .then((data) => {
            if (!data) {
                return insertNewLog(
                    {
                        module: moduleName,
                        function: "setParticipantSurveyResults",
                        params: {
                            params: req.params,
                            body: req.body,
                        },
                        message: "ParticipantUrlID or participantID not found",
                        status: httpStatuses.NotFound,
                    },
                    res
                );
            } else {
                return res.send({
                    message: `Updated surveyResults data for participant.id=${participantID} 
                        in participantUrlID=${participantsUrlID}`,
                });
            }
        })
        .catch((err) => {
            return insertNewLog(
                {
                    module: moduleName,
                    function: "setParticipantSurveyResults",
                    params: {
                        params: req.params,
                        body: req.body,
                    },
                    message:
                        err.message ||
                        `Error retrieving ParticipantUrlID with id=${participantsUrlID} or participant.id=${participantID}`,
                    status: httpStatuses.InternalServerError,
                },
                res
            );
        });
};
