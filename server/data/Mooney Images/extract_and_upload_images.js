const fs = require("fs");
const path = require("path");
const AdmZip = require("adm-zip");
const { uploadImagesFromDisk } = require("../../controllers/image.controller");
const { insertNewLog } = require("../../controllers/log.controller");

const zipFiles = ["scrambled_bmp", "stimuli_bmp"];


const getBmpFiles = function(dirPath, filesArr) {
    let files = fs.readdirSync(dirPath);

    let bmpFiles = filesArr;

    files.forEach(function(file) {
        let filePath = [dirPath, file].join(path.sep)
        if (fs.statSync(filePath).isDirectory()) {
            getBmpFiles(filePath);
            if (fs.existsSync(filePath)) {
                fs.rmdirSync(filePath, {recursive: true});
            }
        } else {            
            if (path.extname(filePath) !== '.bmp') {
                // remove file from File System                                
                fs.unlink(filePath, (err) => {
                    if (err) {
                        console.error("Having some issues on removing non .bmp file from uploads directory..");
                        console.error(err);

                        insertNewLog(
                            {
                                "module": "extract_and_uploads_images",
                                "function": "extractAndUploadImages.unlink",
                                "params": {
                                    "filePath": filePath,
                                },
                                "message": err,
                                "status": "",
                            },                    
                        );
                    }                    
                });
            } else {
                // if the file is in a sub-directory
                if (path.dirname(filePath) !== 'uploads') {                    
                    // move the file into uploads directory
                    fs.rename(filePath, [__dirname, 'uploads', path.basename(filePath)].join(path.sep), (err) => {
                        if (err) {
                            console.error("Having some issues on moving .bmp file from subdirectory to uploads directory..");
                            console.error(err);

                            insertNewLog(
                                {
                                    "module": "extract_and_uploads_images",
                                    "function": "extractAndUploadImages.rename",
                                    "params": {
                                        "filePath": filePath,
                                    },
                                    "message": err,
                                    "status": "",
                                },                    
                            );
                        }                        
                    })
                }
                
                if (bmpFiles) {
                    bmpFiles.push({
                        "originalname": path.basename(filePath),
                    })
                }
            }
        }
    })

    return bmpFiles;
}

exports.extractAndUploadImages =() => {
    const uploadsDir = [__dirname, 'uploads'].join(path.sep)
    
    try {
        zipFiles.forEach(function(zipFile) {
            let filesDir = [__dirname, zipFile].join(path.sep);
            // extracing images from zip to uploads folder
            let zip = new AdmZip(`${filesDir}.zip`);
        
            console.log(`unzipping ${zipFile} to uploads directory`);
        
            zip.extractAllTo(uploadsDir, true);
        })
        
        console.log('Searching for .bmp images');
        let bmpFiles = getBmpFiles(uploadsDir, new Array());
        
        console.log('Uploading .bmp images to db');
        const chunkSize = 50;
        for (let i = 0; i < bmpFiles.length; i += chunkSize) {
            const chunk = bmpFiles.slice(i, i + chunkSize);
            uploadImagesFromDisk(chunk);
        }
    } catch (error) {
        console.error("Having some issues on uploading Mooney images to db..");
        console.error(error);

        insertNewLog(
            {
                "module": "extract_and_uploads_images",
                "function": "extractAndUploadImages",
                "params": {
                    "zip_files": zipFiles,
                },
                "message": error,
                "status": "",
            },                    
        );
    }
}