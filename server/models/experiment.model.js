module.exports = (mongoose) => {
    let generalFieldSchema = mongoose.Schema({
        description: String,
        stimulusDuration: Number,
        responseDuration: Number,
        practiceTrial: Boolean,
        trialNum: Number,
    });

    let taskSchema = mongoose.Schema({
        order: Number,
        type: String,
        generalFields: generalFieldSchema,
        fields: Object,
        trials: [Object],
    });

    let participantAnswerSchema = mongoose.Schema({
        taskOrder: Number,
        taskType: String,
        trialNum: Number,
        answer: String,
        stimulusStartDisplayTime: Date,
        responseStartDisplayTime: Date,
        responseTime: Number,
        isPractice: Boolean,
    });

    let visualAcuitySchema = mongoose.Schema({
        vaValue: Number,
        isFreeStimulation: Boolean,
        answer: String,
        rightAnswer: String,
        rt: Number,
    });

    let participantSchema = mongoose.Schema({
        connectionType: String,
        authID: String,
        clientIP: String,
        visualAcuity: [visualAcuitySchema],
        virtualChinrestStart: Object,
        virtualChinrestEnd: Object,
        surveyResults: Object,
        experimentAnswers: [participantAnswerSchema],
    });

    let schema = mongoose.Schema(
        {
            name: String,
            startDate: Date,
            endDate: Date,
            experimentType: String,
            isPublished: Boolean,
            surveyJson: Object,
            passcode: String,
            paymentCode:String,
            participantsUrlId: mongoose.Types.ObjectId,
            tasks: [taskSchema],
            participants: [participantSchema],
        },
        {
            collection: "Experiments",
            timestamps: true,
        }
    );

    const mopp = mongoose.model("MOPP", schema);
    return mopp;
};
