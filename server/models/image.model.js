module.exports = (mongoose) => {
    let imageSchema = mongoose.Schema(
        {
            type: String,
            num: String,
            data: Buffer,
            contentType: String,
            url: String,
        },
        { collection: "Images" }
    );

    const images = mongoose.model("IMAGES", imageSchema);
    return images;
};
