module.exports = (mongoose) => {
    let logSchema = mongoose.Schema(
        {
            module: String,
            function: String,
            params: Object,
            message: String,
            status: Number,
        },
        { collection: "Logs" }
    );

    const images = mongoose.model("LOGS", logSchema);
    return images;
};
