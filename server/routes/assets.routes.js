module.exports = (app) => {
    const router = require("express").Router();
    const participant = require("../controllers/assets.controller");    
    
    router.get("/BMLGif/:difficulty/:gifName",participant.getBMLGif);

    app.use("/api/assets", router);
};
