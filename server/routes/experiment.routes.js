module.exports = app => {
    const router = require("express").Router();
    const experiment = require("../controllers/experiment.controller");

    router.get("/", experiment.findAll);
    router.get("/validatePassword", experiment.validatePassword);
    router.get("/:id", experiment.findOne);
    router.get("/:id/download", experiment.downloadOne);
    
    router.post("/", experiment.create);
    router.put("/:id", experiment.update);
    router.put("/publish/:id", experiment.publish);

    router.delete("/:id", experiment.delete);

    app.use("/api/experiments", router);
}