const multer = require("multer");
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, "./data/Mooney Images/uploads")            
    },
    filename: (req, file, cb) => {
        cb(null, file.originalname);
    },
});
const upload = multer({ storage: storage });
module.exports = (app) => {
    const router = require("express").Router();
    const image = require("../controllers/image.controller");

    router.get(`/:imgType/:imgNum`, image.getImage);
    router.post("/images", upload.array("uploaded_image"), (req, res, next) => {
        image.uploadImages(req, res)
    });

    app.use("/api/images", router);
};
