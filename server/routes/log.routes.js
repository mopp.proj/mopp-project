module.exports = (app) => {
    const router = require("express").Router();
    const log = require("../controllers/log.controller");

    router.post("/", log.insertNewLog);

    app.use("/api/logs", router);
};
