module.exports = (app) => {
    const router = require("express").Router();
    const participant = require("../controllers/participant.controller");    
    
    router.get("/:id", participant.exists)
    router.get("/:id/:participantID", participant.findOne);
    router.get("/:id/:participantID/get/trial", participant.findNextTrial);
    router.get("/:id/get/experimentType", participant.getExperimentType);

    router.put("/:id", participant.validateAndInsertNewParticipant);
    router.put("/:id/:participantID", participant.insertParticipantAnswer);
    router.put(
        "/:id/:participantID/va",
        participant.setParticipantVisualAcuity
    );
    router.put(
        "/:id/:participantID/vcs",
        participant.setParticipantVirtualChinrestStart
    );
    router.put(
        "/:id/:participantID/vce",
        participant.setParticipantVirtualChinrestEnd
    );
    router.put(
        "/:id/:participantID/sr",
        participant.setParticipantSurveyResults
    );

    app.use("/api/participants", router);
};
