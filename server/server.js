const fs = require("fs");
const cors = require("cors");
const express = require("express");
const https = require("https");
const bodyParser = require("body-parser");
const dotenv = require("dotenv");
const { insertNewLog } = require("./controllers/log.controller");
const httpStatuses = require("./controllers/HttpStatusCodes.json");
const {
    extractAndUploadImages,
} = require("./data/Mooney Images/extract_and_upload_images");
dotenv.config();

const PORT = process.env.PORT || 5443;

const privateKey = fs.readFileSync(process.env.SSL_KEY_FILE, "utf8");
const certificate = fs.readFileSync(process.env.SSL_CRT_FILE, "utf8");
const creds = {
    key: privateKey,
    cert: certificate,
};

const app = express();

// For local run, use this baseClientURL
// const baseClientURL = "https://localhost:8080";
const baseClientURL = process.env.BASE_URL;

const corsOptions = {
    origin: baseClientURL,
    credentials: true,
    optionSuccessStatus: 200,
};

app.use(cors(corsOptions));
// parse requests of content-type - application/json
app.use(bodyParser.json());
// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

require("./routes/image.routes")(app);
require("./routes/experiment.routes")(app);
require("./routes/participant.routes")(app);
require("./routes/assets.routes")(app);
require("./routes/log.routes")(app);

process.on('uncaughtException', function (err) {
    console.log(err)
    return insertNewLog(
        {
            "module": "main",
            "function": "general",
            "params": "none",
            "message": err.message || "Some unknown error",
            "status": httpStatuses.InternalServerError,
        })
  });

const httpsServer = https.createServer(creds, app);
httpsServer.listen(PORT, () => {
    console.log(`Node https server started running in port: ${PORT}`);

    // NOTE: uncomment if you'd like to insert Mooney Images to the db
    // extractAndUploadImages();
});
